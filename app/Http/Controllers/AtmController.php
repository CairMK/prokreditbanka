<?php

namespace App\Http\Controllers;

use App\Models\Atm;
use App\Models\User as User;
use App\Models\Workflow as Workflow;
use DB;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AtmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $atms = Atm::orderBy('created_at', 'asc')->get();
        $data = ['atms' => $atms];
        return view('admin.atm.atm')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::get();
        $atms = Atm::all();
        $workflows = Workflow::orderBy('id', 'desc')->get();
        $data = ['users' => $users, 'workflows' => $workflows, 'atms' => $atms];
        return view('admin.atm.createatm')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',

        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $request['title'] = strip_tags($request['title']);
        $request['slug'] = str_slug($request['title']);

        $slug = Atm::where('title', $request['title'])->get();

        (int)$count = count($slug);

        if ($count > 0) {
            $request['slug'] = $request['slug'] . '-' . $count;
        }

        $input = $request->all();

//        if ($request->hasFile('image')) {
//
//            $image = $request->file('image');
//            $path = public_path() . '/assets/img/atm';
//            $pathThumb = public_path() . '/assets/img/atm/thumbnails/';
//            $pathMedium = public_path() . '/assets/img/atm/medium/';
//            $ext = $image->getClientOriginalExtension();
//
//            if ($count > 0) {
//                $imageName = str_slug($input['title']) . '-' . $count . '.' . $ext;
//            } else {
//                $imageName = str_slug($input['title']) . '.' . $ext;
//            }
//
//            $image->move($path, $imageName);
//
//            $findimage = public_path() . '/assets/img/atm/' . $imageName;
//            $imagethumb = Image::make($findimage)->resize(200, null, function ($constraint) {
//                $constraint->aspectRatio();
//            });
//
//            $imagemedium = Image::make($findimage)->resize(600, null, function ($constraint) {
//                $constraint->aspectRatio();
//            });
//            $imagethumb->save($pathThumb . $imageName);
//            $imagemedium->save($pathMedium . $imageName);
//
//            $image = $request->imagethumb = $imageName;
//            $imagethumb = $request->image = $imageName;
//            $imagemedium = $request->image = $imageName;
//
//        }
//
//        $input['image'] = $image;
//        $input['imagemedium'] = $imagemedium;
//        $input['imagethumb'] = $imagethumb;

        $atms = Atm::create($input);

        Session::flash('flash_message', 'Product successfully created!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $atms = Atm::FindOrFail($id);
        $users = User::get();
        $workflows = Workflow::orderBy('id', 'desc')->get();
        $data = ['atms' => $atms, 'users' => $users, 'workflows' => $workflows];
        return view('admin.atm.editatm')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $errors = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $request['title'] = strip_tags($request['title']);

        $slug = DB::table('atm')->select('slug')->where('id', '=', $id)->get();

        $slugname = $slug[0]->slug;

        $input = $request->all();
        $atm = Atm::FindOrFail($id);

        $atm->fill($input)->save();

//        if ($request->hasFile('image')) {
//
//            $image = $request->file('image');
//            $path = public_path() . '/assets/img/atn';
//            $pathThumb = public_path() . '/assets/atm/products/thumbnails/';
//            $pathMedium = public_path() . '/assets/atm/products/medium/';
//            $ext = $image->getClientOriginalExtension();
//
//            $imageName = $slugname . '.' . $ext;
//
//            $image->move($path, $imageName);
//
//            $findimage = public_path() . '/assets/img/atm/' . $imageName;
//            $imagethumb = Image::make($findimage)->resize(200, null, function ($constraint) {
//                $constraint->aspectRatio();
//            });
//
//            $imagemedium = Image::make($findimage)->resize(600, null, function ($constraint) {
//                $constraint->aspectRatio();
//            });
//
//            $imagethumb->save($pathThumb . $imageName);
//            $imagemedium->save($pathMedium . $imageName);
//
//            $image = $request->imagethumb = $imageName;
//            $imagethumb = $request->image = $imageName;
//            $imagemedium = $request->image = $imageName;
//
//            $input['image'] = $image;
//            $input['imagemedium'] = $imagemedium;
//            $input['imagethumb'] = $imagethumb;
//
//        }

        $atm->fill($input)->save();

        Session::flash('flash_message', 'Product successfully edited!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $atm = Atm::FindOrFail($id);

        // Delete blog images
//        $image = public_path() . '/assets/img/atm/' . $product->image;
//        $imagemedium = public_path() . '/assets/img/atm/medium/' . $product->image;
//        $imagethumb = public_path() . '/assets/img/atm/thumbnails/' . $product->image;
//
//        unlink($image);
//        unlink($imagemedium);
//        unlink($imagethumb);

        $atm->delete();
        return redirect('/admin/atm');
    }
}
