<?php

namespace App\Http\Controllers;

use App\Models\Category as Category;
use App\Models\Cities as Cities;
use App\Models\Bankomat as Bankomat;
use App\Models\User as User;
use App\Models\Workflow as Workflow;
use DB;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class BankomatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bankomat = Bankomat::orderBy('id', 'desc')->paginate(10);
        $data = ['bankomat' => $bankomat];
        return view('admin.bankomat.bankomat')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::get();
        $cities = Cities::get();
        $users = User::get();
        $workflows = Workflow::orderBy('id', 'desc')->get();
        $data = ['cities' => $cities, 'categories' => $categories, 'users' => $users, 'workflows' => $workflows];
        return view('admin.bankomat.createbankomat')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = Validator::make($request->all(), [
            'title' => 'required|max:255',
//            'description' => 'required',
            'image' => 'required',

        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $request['title'] = strip_tags($request['title']);
        $request['slug'] = str_slug($request['title']);

        $slug = Bankomat::where('title', $request['title'])->get();

        (int)$count = count($slug);

        if ($count > 0) {
            $request['slug'] = $request['slug'] . '-' . $count;
        }

        $input = $request->all();

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $path = public_path() . '/assets/img/bankomat';
            $pathThumb = public_path() . '/assets/img/bankomat/thumbnails/';
            $pathMedium = public_path() . '/assets/img/bankomat/medium/';
            $ext = $image->getClientOriginalExtension();

            if ($count > 0) {
                $imageName = str_slug($input['title']) . '-' . $count . '.' . $ext;
            } else {
                $imageName = str_slug($input['title']) . '.' . $ext;
            }

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/bankomat/' . $imageName;
            $imagethumb = Image::make($findimage)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagemedium = Image::make($findimage)->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $imagethumb->save($pathThumb . $imageName);
            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;
            $imagethumb = $request->image = $imageName;
            $imagemedium = $request->image = $imageName;

        }

        $input['image'] = $image;
        $input['imagemedium'] = $imagemedium;
        $input['imagethumb'] = $imagethumb;

        $bankomat = Bankomat::create($input);

        Session::flash('flash_message', 'Bankomat successfully created!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bankomat = Bankomat::FindOrFail($id);
        $categories = Category::get();
        $cities = Cities::get();
        $users = User::get();
        $workflows = Workflow::orderBy('id', 'desc')->get();
        $data = ['bankomat' => $bankomat, 'cities' => $cities, 'categories' => $categories, 'users' => $users, 'workflows' => $workflows];
        return view('admin.bankomat.editbankomat')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $errors = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $request['title'] = strip_tags($request['title']);

        $slug = DB::table('bankomat')->select('slug')->where('id', '=', $id)->get();

        $slugname = $slug[0]->slug;

        $input = $request->all();
        $bankomat = Bankomat::FindOrFail($id);

        $bankomat->fill($input)->save();

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $path = public_path() . '/assets/img/bankomat';
            $pathThumb = public_path() . '/assets/img/bankomat/thumbnails/';
            $pathMedium = public_path() . '/assets/img/bankomat/medium/';
            $ext = $image->getClientOriginalExtension();

            $imageName = $slugname . '.' . $ext;

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/bankomat/' . $imageName;
            $imagethumb = Image::make($findimage)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagemedium = Image::make($findimage)->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagethumb->save($pathThumb . $imageName);
            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;
            $imagethumb = $request->image = $imageName;
            $imagemedium = $request->image = $imageName;

            $input['image'] = $image;
            $input['imagemedium'] = $imagemedium;
            $input['imagethumb'] = $imagethumb;

        }

        $bankomat->fill($input)->save();

        Session::flash('flash_message', 'Bankomat successfully edited!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bankomat = Bankomat::FindOrFail($id);

        // Delete blog images
        $image = public_path() . '/assets/img/bankomat/' . $bankomat->image;
        $imagemedium = public_path() . '/assets/img/bankomat/medium/' . $bankomat->image;
        $imagethumb = public_path() . '/assets/img/bankomat/thumbnails/' . $bankomat->image;

        unlink($image);
        unlink($imagemedium);
        unlink($imagethumb);

        $bankomat->delete();
        return redirect('/admin/bankomat');
    }
}
