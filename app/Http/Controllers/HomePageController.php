<?php

namespace App\Http\Controllers;

use App\Models\Category as Category;
use App\Models\Bankomat as Bankomat;
use App\Models\Kurs as Kurs;
use App\Models\Product as Product;
use App\Models\Referral as Refferal;
use App\Models\Services as Services;
use App\Models\Settings as Settings;
use App\Models\News as News;
use App\Models\Atm as Atm;
use App\Models\Slider;
use App\Models\Sliders;
use App\Models\StaticPage as StaticPage;
use DB;
use Illuminate\Support\Facades\Session;

class HomePageController extends Controller
{

    public function index()
    {
        $lang = Session::get('locale');

//        dd($lang);
        $settings = Settings::firstOrFail();
        $newshome = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
        $slides = Slider::all();
        $kurs = Kurs::all();
//        $categories = Category::roots()->get();
        $slider = Slider::where('workflow_id', '=', '1')->where('lang', '=', $lang)->get();
//        $tree = Category::getTreeHP($categories);
        $data = ["newshome" => $newshome, "settings" => $settings, "slides" => $slides, "status" => "success", "slider" => $slider, "kurs" => $kurs];
//        "categories" => $categories, "tree" => $tree,
        return view('main.homepage')->with($data);
    }

    public function staticpages($slug)
    {
        $lang = Session::get('locale');
        $newshome = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
        $kurs = Kurs::all();

        if ($slug == "contact") {
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "products" => $products, "kurs" => $kurs, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.contact')->with($data);
        } elseif
        ($slug == "eco") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "products" => $products, "kurs" => $kurs, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.eco')->with($data);
        } elseif
        ($slug == "ebank-f") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "products" => $products, "kurs" => $kurs, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.ebank-f')->with($data);
        } elseif
        ($slug == "deposit") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "products" => $products, "kurs" => $kurs, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.deposit')->with($data);
        } elseif
        ($slug == "flex") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "products" => $products, "kurs" => $kurs, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.flex')->with($data);
        } elseif
        ($slug == "invest") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "products" => $products, "kurs" => $kurs, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.invest')->with($data);
        } elseif
        ($slug == "ebank") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "products" => $products, "kurs" => $kurs, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.ebank')->with($data);
        } elseif
        ($slug == "stamben") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "products" => $products, "kurs" => $kurs, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.stamben')->with($data);
        } elseif
        ($slug == "ecobusinesses") {
            $products = Product::all();
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "products" => $products, "kurs" => $kurs, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.ecobusinesses')->with($data);
        } elseif
        ($slug == "corporate-information-according-to-nbrm") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "products" => $products, "kurs" => $kurs, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.bank')->with($data);
        } elseif
        ($slug == "terms-and-conditions") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->where('lang', '=', $lang)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "products" => $products, "kurs" => $kurs, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.bank')->with($data);
        } elseif
        ($slug == "network") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $bankomat = Bankomat::where('lang', '=', $lang)->get();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "bankomat" => $bankomat, "kurs" => $kurs, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.network')->with($data);
        } elseif
        ($slug == "calculator") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $bankomat = Bankomat::where('lang', '=', $lang)->get();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "bankomat" => $bankomat, "kurs" => $kurs, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.calculator')->with($data);
        } elseif
        ($slug == "calculator1") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $bankomat = Bankomat::where('lang', '=', $lang)->get();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "bankomat" => $bankomat, "kurs" => $kurs, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.calculator1')->with($data);
        } elseif
        ($slug == "risk-management") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "kurs" => $kurs, "products" => $products, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.bank')->with($data);
        } elseif
        ($slug == "annual-report") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "kurs" => $kurs, "products" => $products, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.bank')->with($data);
        } elseif
        ($slug == "abou-us") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->where('lang', '=', $lang)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "kurs" => $kurs, "products" => $products, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.staticpage')->with($data);
        } elseif
        ($slug == "the-banks-capital") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->where('lang', '=', $lang)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "kurs" => $kurs, "products" => $products, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.bank')->with($data);
        } elseif
        ($slug == "faq") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $atm = Atm::where("slug", "=", $slug)->where('lang', '=', $lang)->get();
//            dd($atm);
            $settings = Settings::firstOrFail();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "kurs" => $kurs, "settings" => $settings, "tree" => $tree, "atm" => $atm];
            return view('main.faq')->with($data);
        } elseif ($slug == "ecohouseholds") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "kurs" => $kurs, "products" => $products, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.ecohouseholds')->with($data);
        } elseif ($slug == "tenderi") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "kurs" => $kurs, "products" => $products, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.tenderi')->with($data);
        } elseif ($slug == "prodazba") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $products = Product::all();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "kurs" => $kurs, "products" => $products, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.prodazba')->with($data);
        } elseif ($slug == "contactinfo") {
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
            $staticpage = StaticPage::where("slug", "=", $slug)->first();
            $staticpages = StaticPage::all();
            $products = Product::all();
            $settings = Settings::firstOrFail();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $data = ["newshome" => $newshome, "news" => $news, "kurs" => $kurs, "products" => $products, "settings" => $settings, "staticpage" => $staticpage, "staticpages" => $staticpages, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.contactinfo')->with($data);
        } else
            $news = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();
        $settings = Settings::firstOrFail();
        $services = Services::all();
        $staticpage = StaticPage::where("slug", "=", $slug)->where('lang', '=', $lang)->first();
        $staticpages = StaticPage::all();
        $categories = Category::roots()->get();
        $products = Product::all();
        $tree = Category::getTreeHP($categories);
        $allcategories = Category::get();
        $data = ["newshome" => $newshome, "news" => $news, "kurs" => $kurs, "settings" => $settings, "services" => $services, "staticpage" => $staticpage, "staticpages" => $staticpages, "allcategories" => $allcategories, "status" => "success", "products" => $products, "categories" => $categories, "tree" => $tree];
        return view('main.staticpage')->with($data);

    }

    public
    function services($slug)
    {
        $settings = Settings::firstOrFail();
        $services = Services::all();
        $service = Services::where('slug', '=', $slug)->first();
        $allcategories = Category::get();
        $categories = Category::roots()->get();
        $tree = Category::getTreeHP($categories);
        $staticpages = StaticPage::all();
        $data = ["service" => $service, "services" => $services, "staticpages" => $staticpages, "settings" => $settings, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
        return view('main.services')->with($data);
    }

    public
    function product($slug)
    {
        $settings = Settings::firstOrFail();
        $services = Services::all();
        $product = Product::where('slug', '=', $slug)->first();
        $sliders = Sliders::where('product_id', '=', $product->id)->get();
        $allcategories = Category::get();
        $categories = Category::roots()->get();
        $tree = Category::getTreeHP($categories);
        $staticpages = StaticPage::all();
        $data = ["sliders" => $sliders, "product" => $product, "services" => $services, "staticpages" => $staticpages, "settings" => $settings, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
        return view('main.product')->with($data);
    }

    public
    function referents($slug)
    {
        $settings = Settings::firstOrFail();
        $services = Services::all();
        $referral = Refferal::all();
        if ($slug == "all") {
            $products = Product::all();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $staticpages = StaticPage::all();
            $data = ["referral" => $referral, "products" => $products, "services" => $services, "staticpages" => $staticpages, "settings" => $settings, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.allreferral')->with($data);
        } else {
            $category = Category::where('slug', '=', $slug)->first();
            $referral = Refferal::where('slug', '=', $slug)->first();

        }

        $allcategories = Category::get();
        $categories = Category::roots()->get();
        $tree = Category::getTreeHP($categories);
        $staticpages = StaticPage::all();
        $data = ["referral" => $referral, "category" => $category, "services" => $services, "staticpages" => $staticpages, "settings" => $settings, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
        return view('main.referral')->with($data);

    }

    public
    function categories($slug)
    {

        $lang = Session::get('locale');
        $kurs = Kurs::all();
        $settings = Settings::firstOrFail();
        $services = Services::all();
        $newshome = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();

        if ($slug == "all") {
            $products = Product::all();
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $staticpages = StaticPage::all();
            $data = ["products" => $products, "services" => $services, "kurs" => $kurs, "newshome" => $newshome, "staticpages" => $staticpages, "settings" => $settings, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.allcategories')->with($data);
        } else {
            $category = Category::where('slug', '=', $slug)->first();
            $products = Product::where('category', '=', $category->id)->get();
        }

        $sliders = Sliders::where('category_id', '=', $category->id)->get();
        $allcategories = Category::get();
        $categories = Category::roots()->get();
        $tree = Category::getTreeHP($categories);
        $staticpages = StaticPage::all();
        $data = ["category" => $category, "sliders" => $sliders, "kurs" => $kurs, "newshome" => $newshome, "products" => $products, "services" => $services, "staticpages" => $staticpages, "settings" => $settings, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
        return view('main.categories')->with($data);
    }

    public function news($slug)
    {
        $lang = Session::get('locale');
        $settings = Settings::firstOrFail();
        $services = Services::all();
        $news = News::orderBy('id', 'desc')->where('lang', '=', $lang)->paginate(10);
        $newshome = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();

        if ($slug == "all") {
            $allcategories = Category::get();
            $categories = Category::roots()->get();
            $tree = Category::getTreeHP($categories);
            $staticpages = StaticPage::all();
            $data = ["newshome" => $newshome, "news" => $news, "services" => $services, "staticpages" => $staticpages, "settings" => $settings, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
            return view('main.allnews')->with($data);
        } else {
            $category = Category::where('slug', '=', $slug)->first();
            $news = News::where('slug', '=', $slug)->where('lang', '=', $lang)->first();
            $newshome = News::where('workflow_id', '=', '1')->where('lang', '=', $lang)->take(2)->get();


        }

        $allcategories = Category::get();
        $categories = Category::roots()->get();
        $tree = Category::getTreeHP($categories);
        $staticpages = StaticPage::all();
        $data = ["newshome" => $newshome, "news" => $news, "category" => $category, "services" => $services, "staticpages" => $staticpages, "settings" => $settings, "tree" => $tree, "categories" => $categories, "allcategories" => $allcategories];
        return view('main.news')->with($data);

    }

}
