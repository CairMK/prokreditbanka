<?php

namespace App\Http\Controllers;

use App\Models\Kurs as Kurs;
use App\Models\User as User;
use App\Models\Workflow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class KursController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $kurs = Kurs::first();
        $workflows = Workflow::all();
        $users = User::all();

        $data = ['users' => $users, "kurs" => $kurs, 'workflows' => $workflows];
        if (empty($kurs)) {

            return view('admin.kurs.kurs')->with($data);
        } else {
            $kurs = Kurs::first()->get();
            $data = ['users' => $users, "kurs" => $kurs, 'workflows' => $workflows];
            return view('admin.kurs.kurs')->with($data);
        }
    }

    public function store(Request $request)
    {

        $errors = Validator::make($request->all(), [
            'usa' => 'required',
            'funta' => 'required',
            'euro' => 'required',
            'frank' => 'required',
            'frank1' => 'required',
            'euro1' => 'required',
            'funta1' => 'required',
            'usa1' => 'required',

        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $input = $request->all();

        Settings::create($input);

        Session::flash('flash_message', 'Settings successfully created!');

        return redirect()->back();
    }

    public function edit($id)
    {
        $kurs = Kurs::FindOrFail($id);
        $workflows = Workflow::all();
        $users = User::all();
        $data = ['users' => $users, "kurs" => $kurs, 'workflows' => $workflows];
        return view('admin.kurs.editkurs')->with($data);
    }

    public function update(Request $request)
    {

        $errors = Validator::make($request->all(), [
            'usa' => 'required',
            'funta' => 'required',
            'euro' => 'required',
            'frank' => 'required',
            'frank1' => 'required',
            'euro1' => 'required',
            'funta1' => 'required',
            'usa1' => 'required',
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $input = $request->all();
        $kurs = Kurs::FindOrFail($request->id);

        $kurs->fill($input)->save();


        $kurs->fill($input)->save();

        Session::flash('flash_message', 'Settings successfully edited!');

        return redirect()->back();
    }

    public function all()
    {
        return view('admin.kurs');
    }
}
