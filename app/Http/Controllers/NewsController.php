<?php

namespace App\Http\Controllers;

use App\Models\Country as Country;
use App\Models\News as News;
use App\Models\User as User;
use App\Models\Workflow;
use DB;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $news = News::paginate(10);
        $data = ["news" => $news];
        return view('admin.news.news')->with($data);
    }

    public function create()
    {
        $countries = Country::orderBy('name', 'asc')->get();
        $users = User::get();
        $workflows = Workflow::get();
        $data = ["countries" => $countries, "users" => $users, "workflows" => $workflows];
        return view('admin.news.createnews')->with($data);
    }

    public function store(Request $request)
    {
        $errors = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $request['title'] = strip_tags($request['title']);
        $request['slug'] = str_slug($request['title']);

        $slug = News::where('title', $request['title'])->get();

        (int)$count = count($slug);

        if ($count > 0) {
            $request['slug'] = $request['slug'] . '-' . $count;
        }

        $input = $request->all();

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $path = public_path() . '/assets/img/news';
            $pathThumb = public_path() . '/assets/img/news/thumbnails/';
            $pathMedium = public_path() . '/assets/img/news/medium/';
            $ext = $image->getClientOriginalExtension();

            if ($count > 0) {
                $imageName = str_slug($input['title']) . '-' . $count . '.' . $ext;
            } else {
                $imageName = str_slug($input['title']) . '.' . $ext;
            }

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/news/' . $imageName;
            $imagethumb = Image::make($findimage)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagemedium = Image::make($findimage)->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagethumb->save($pathThumb . $imageName);
            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;
            $imagethumb = $request->image = $imageName;
            $imagemedium = $request->image = $imageName;
            $input['image'] = $image;
            $input['imagemedium'] = $imagemedium;
            $input['imagethumb'] = $imagethumb;
        }

        $news = News::create($input);

        Session::flash('flash_message', 'News post successfully created!');

        return redirect()->back();
    }

    public function show($id)
    {
        $news = News::find($id);
        $data = ['news' => $news];
        return view('admin.news.views')->with($data);
    }

    public function edit($id)
    {
        $news = News::FindOrFail($id);
        $users = User::get();
        $workflows = Workflow::orderBy('id', 'desc')->get();
        $data = ['news' => $news, 'users' => $users, 'workflows' => $workflows];
        return view('admin.news.editnews')->with($data);
    }

    public function update(Request $request, $id)
    {
        $errors = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $request['title'] = strip_tags($request['title']);

        $slug = DB::table('news')->select('slug')->where('id', '=', $id)->get();

        $slugname = $slug[0]->slug;

        $input = $request->all();
        $news = News::FindOrFail($id);

        $news->fill($input)->save();

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $path = public_path() . '/assets/img/news';
            $pathThumb = public_path() . '/assets/img/news/thumbnails/';
            $pathMedium = public_path() . '/assets/img/news/medium/';
            $ext = $image->getClientOriginalExtension();

            $imageName = $slugname . '.' . $ext;

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/news/' . $imageName;
            $imagethumb = Image::make($findimage)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagemedium = Image::make($findimage)->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagethumb->save($pathThumb . $imageName);
            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;
            $imagethumb = $request->image = $imageName;
            $imagemedium = $request->image = $imageName;

            $input['image'] = $image;
            $input['imagemedium'] = $imagemedium;
            $input['imagethumb'] = $imagethumb;

        }

        $news->fill($input)->save();

        Session::flash('flash_message', 'News post successfully edited!');

        return redirect()->back();
    }

    public function destroy($id)
    {
        $news = News::FindOrFail($id);

        if ($news->image) {
            // Delete staticpage images
            $image = public_path() . '/assets/img/news/' . $news->image;
            $imagemedium = public_path() . '/assets/img/news/medium/' . $news->image;
            $imagethumb = public_path() . '/assets/img/news/thumbnails/' . $news->image;

            unlink($image);
            unlink($imagemedium);
            unlink($imagethumb);
        }

        $news->delete();
        return redirect('/admin/news');
    }
}
