<?php

namespace App\Http\Controllers;


use App\Models\PopUp as PopUp;
use App\Models\User as User;
use App\Models\Workflow as Workflow;
use DB;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

;

class PopUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $popup = PopUp::orderBy('id', 'desc')->get();
        $data = ['popup' => $popup];
        return view('admin.popup.popup')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::get();
        $workflows = Workflow::orderBy('id', 'desc')->get();
        $data = ['users' => $users, 'workflows' => $workflows];
        return view('admin.popup.createpopup')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
            'image' => 'required',

        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $request['title'] = strip_tags($request['title']);
        $request['slug'] = str_slug($request['title']);

        $slug = PopUp::where('title', $request['title'])->get();

        (int)$count = count($slug);

        if ($count > 0) {
            $request['slug'] = $request['slug'] . '-' . $count;
        }

        $input = $request->all();

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $path = public_path() . '/assets/img/popup';
            $pathThumb = public_path() . '/assets/img/popup/thumbnails/';
            $pathMedium = public_path() . '/assets/img/popup/medium/';
            $ext = $image->getClientOriginalExtension();

            if ($count > 0) {
                $imageName = str_slug($input['title']) . '-' . $count . '.' . $ext;
            } else {
                $imageName = str_slug($input['title']) . '.' . $ext;
            }

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/popup/' . $imageName;
            $imagethumb = Image::make($findimage)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagemedium = Image::make($findimage)->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $imagethumb->save($pathThumb . $imageName);
            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;
            $imagethumb = $request->image = $imageName;
            $imagemedium = $request->image = $imageName;

        }

        $input['image'] = $image;
        $input['imagemedium'] = $imagemedium;
        $input['imagethumb'] = $imagethumb;

        $popup = PopUp::create($input);

        Session::flash('flash_message', 'Product successfully created!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $popup = PopUp::FindOrFail($id);
        $users = User::get();
        $workflows = Workflow::orderBy('id', 'desc')->get();
        $data = ['popup' => $popup, 'users' => $users, 'workflows' => $workflows];
        return view('admin.popup.editpopup')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $errors = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $request['title'] = strip_tags($request['title']);

        $slug = DB::table('popup')->select('slug')->where('id', '=', $id)->get();

        $slugname = $slug[0]->slug;

        $input = $request->all();
        $popup = PopUp::FindOrFail($id);

        $popup->fill($input)->save();

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $path = public_path() . '/assets/img/popup';
            $pathThumb = public_path() . '/assets/img/popup/thumbnails/';
            $pathMedium = public_path() . '/assets/img/popup/medium/';
            $ext = $image->getClientOriginalExtension();

            $imageName = $slugname . '.' . $ext;

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/popup/' . $imageName;
            $imagethumb = Image::make($findimage)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagemedium = Image::make($findimage)->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagethumb->save($pathThumb . $imageName);
            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;
            $imagethumb = $request->image = $imageName;
            $imagemedium = $request->image = $imageName;

            $input['image'] = $image;
            $input['imagemedium'] = $imagemedium;
            $input['imagethumb'] = $imagethumb;

        }

        $popup->fill($input)->save();

        Session::flash('flash_message', 'Product successfully edited!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $popup = PopUp::FindOrFail($id);

        // Delete blog images
        $image = public_path() . '/assets/img/popup/' . $popup->image;
        $imagemedium = public_path() . '/assets/img/popup/medium/' . $popupt->image;
        $imagethumb = public_path() . '/assets/img/popup/thumbnails/' . $popupt->image;

        unlink($image);
        unlink($imagemedium);
        unlink($imagethumb);

        $product->delete();
        return redirect('/admin/popup');
    }
}
