<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Atm extends Model
{

    protected $table = 'atm';
    protected $fillable = ['title', 'lang', 'address', 'description', 'user_id', 'slug', 'workflow_id', 'created_at', 'updated_at', 'lat', 'lng', 'image', 'imagemedium', 'imagethumb'];


    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function workflow()
    {
        return $this->belongsTo('App\Models\Workflow', 'user_id');
    }

    public function images()
    {
        return $this->belongsTo('App\Sliders', 'id', 'product_id');
    }
}
