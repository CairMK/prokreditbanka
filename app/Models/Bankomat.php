<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bankomat extends Model
{
    protected $table = 'bankomat';
    protected $fillable = ['title', 'slug', 'image', 'lang', 'imagemedium', 'imagethumb', 'cities', 'keywords', 'description', 'category', 'user_id', 'workflow_id', 'featured_id', 'created_at', 'updated_at', 'lat', 'lng'];

    public function cat()
    {
        return $this->belongsTo('App\Models\Category', 'category');
    }


}
