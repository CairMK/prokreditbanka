<?php

namespace App\Models;

class Category extends \Baum\Node
{

    protected $fillable = array('name', 'parent_id', 'slug', 'file', 'filename', 'image', 'description');
    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'category');
    }

    public static function getTree($categories)
    {

        $lists = '<ul>';
        foreach ($categories as $category)
            $lists .= self::renderNode($category);
        $lists .= '</ul>';
        return $lists;
    }

    public static function renderNode($node)
    {
        $list = '<li><a href="/admin/categories/' . $node->id . '/edit">' . $node->name;
        if ($node->children()->count() > 0) {
            $list .= '<ul>';
            foreach ($node->children as $child)
                $list .= self::renderNode($child);
            $list .= "</ul>";
        }

        $list .= "</a></li>";
        return $list;
    }

    public static function getTreeHP($categories)
    {
        $lists = '<li class="nav-item dropdown d-none>';
        foreach ($categories as $category)
            $lists .= self::renderNodeHP($category);
        $lists .= '</li>';
        return $lists;
    }

    public static function renderNodeHP($node)
    {
        $list = '<div><a href="/categories/' . $node->slug . '">' . $node->name . '</a>';
        if ($node->children()->count() > 0) {
            $list .= '<div class="dropdown-menu" aria-labelledby="businessClient-dropdown">';
            foreach ($node->children as $child)
                $list .= self::renderNodeHP($child);
            $list .= "</div>";
        }

        $list .= "</div>";
        return $list;
    }
}

//'
//                            <a class="nav-link dropdown-toggle" href="#" id="businessClient-dropdown"
//                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
//    Business Clients
//</a>
//                            <div class="dropdown-menu" aria-labelledby="businessClient-dropdown">
//                                <a class="dropdown-item" href="#">Accounts</a>
//                                <a class="dropdown-item" href="#">Documentary operation</a>
//                                <a class="dropdown-item" href="#">Deposits and state bonds</a>
//                                <a class="dropdown-item" href="#">Services for non-resident business</a>
//                            </div>
//                        </li>'
