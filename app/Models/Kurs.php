<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kurs extends Model
{
    protected $table = 'kurs';
    protected $fillable = ['usa', 'usa1', 'euro', 'euro1', 'frank', 'frank1', 'funta', 'funta1', 'user_id', 'workflow_id', 'created_at', 'updated_at'];


    public function users()
    {
        return $this->belongsTo('App\Models\Users', 'user_id');
    }


}
