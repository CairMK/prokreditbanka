<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PopUp extends Model
{
    protected $table = 'popup';
    protected $fillable = ['title', 'slug', 'image', 'lang', 'imagemedium', 'imagethumb', 'description', 'user_id', 'workflow_id', 'created_at', 'updated_at'];

    public function cat()
    {
        return $this->belongsTo('App\Models\Category', 'category');
    }

    public function images()
    {
        return $this->belongsTo('App\Models\Sliders', 'id', 'product_id');
    }

}
