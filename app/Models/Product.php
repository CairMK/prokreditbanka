<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $fillable = ['title', 'title1', 'title2', 'title3', 'title4', 'title5', 'title6', 'slug', 'image', 'lang', 'imagemedium', 'imagethumb', 'countries', 'cities', 'neighborhood', 'keywords', 'description', 'description1', 'description2', 'description3', 'description4', 'description5', 'description6', 'category', 'user_id', 'workflow_id', 'featured_id', 'created_at', 'updated_at'];

    public function cat()
    {
        return $this->belongsTo('App\Models\Category', 'category');
    }

    public function images()
    {
        return $this->belongsTo('App\Models\Sliders', 'id', 'product_id');
    }

}
