<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $table = 'services';
    protected $fillable = ['title', 'slug', 'image', 'imagemedium', 'imagethumb', 'description', 'user_id', 'workflow_id', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function createdby()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function workflow()
    {
        return $this->belongsTo('App\Models\Workflow', 'user_id');
    }

}
