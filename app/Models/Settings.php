<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{

    protected $table = 'settigs';
    protected $fillable = ['title', 'mainurl', 'email', 'link', 'address', 'mkd1', 'mkd2', 'mkd3', 'mkd4', 'euro1', 'euro2', 'euro3', 'euro4',
        'logo', 'logomedium', 'logothumb', 'description', 'user_id', 'workflow_id', 'created_at', 'updated_at',
        'phone', 'twitter', 'facebook', 'linkedin', 'gplus', 'youtube', 'flickr', 'pinterest', 'lat', 'lng'
    ];


    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function createdby()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function workflow()
    {
        return $this->belongsTo('App\Models\Workflow', 'user_id');
    }
}
