<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaticPage extends Model
{
    protected $table = 'staticpage';
    protected $fillable = ['title', 'title1', 'title2', 'title3', 'title4', 'title5', 'title6', 'title7', 'title8', 'title9', 'slug', 'image', 'lang', 'imagemedium', 'imagethumb', 'description', 'description1', 'description2', 'description3', 'description4', 'description5', 'description6', 'description7', 'description8', 'description9', 'user_id', 'workflow_id', 'created_at', 'updated_at'];


    public function users()
    {
        return $this->belongsTo('App\Models\Users', 'user_id');
    }


}
