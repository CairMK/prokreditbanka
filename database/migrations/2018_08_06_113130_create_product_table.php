<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->string('slug');
			$table->string('image');
			$table->string('imagemedium');
			$table->string('imagethumb');
			$table->integer('category')->unsigned();
			$table->text('description', 65535);
			$table->integer('user_id')->unsigned();
			$table->integer('workflow_id');
			$table->timestamps();
			$table->integer('cities')->unsigned();
			$table->integer('neighborhood')->unsigned();
			$table->integer('countries')->unsigned();
			$table->string('keywords');
			$table->string('lang');
			$table->integer('featured_id');
			$table->string('title1');
			$table->text('description1', 65535);
			$table->string('title2');
			$table->text('description2', 65535);
			$table->string('title3');
			$table->text('description3', 65535);
			$table->string('title4');
			$table->text('description4', 65535);
			$table->string('title5');
			$table->text('description5', 65535);
			$table->string('title6');
			$table->text('description6', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product');
	}

}
