<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaticpageTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staticpage', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('image');
            $table->string('imagemedium');
            $table->string('imagethumb');
            $table->text('description', 65535);
            $table->integer('user_id')->unsigned()->index('staticpage_user_id_foreign');
            $table->integer('workflow_id');
            $table->timestamps();
            $table->string('title1')->nullable();
            $table->text('description1', 65535)->nullable();
            $table->string('title2')->nullable();
            $table->text('description2', 65535)->nullable();
            $table->string('title3')->nullable();
            $table->text('description3', 65535)->nullable();
            $table->string('title4')->nullable();
            $table->text('description4', 65535)->nullable();
            $table->string('title5')->nullable();
            $table->text('description5', 65535)->nullable();
            $table->string('title6')->nullable();
            $table->text('description6', 65535)->nullable();
            $table->string('lang')->nullable();
            $table->string('title7')->nullable();
            $table->text('description7', 65535)->nullable();
            $table->string('title8')->nullable();
            $table->text('description8', 65535)->nullable();
            $table->string('title9')->nullable();
            $table->text('description9', 65535)->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('staticpage');
    }

}
