<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKamatiToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settigs', function (Blueprint $table) {
            $table->string('mkd1');
            $table->string('mkd2');
            $table->string('mkd3');
            $table->string('mkd4');
            $table->string('euro1');
            $table->string('euro2');
            $table->string('euro3');
            $table->string('euro4');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settigs', function (Blueprint $table) {
            $table->string('mkd1');
            $table->string('mkd2');
            $table->string('mkd3');
            $table->string('mkd4');
            $table->string('euro1');
            $table->string('euro2');
            $table->string('euro3');
            $table->string('euro4');
        });
    }
}