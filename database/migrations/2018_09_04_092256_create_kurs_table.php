<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kurs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('usa');
            $table->string('usa1');
            $table->string('euro');
            $table->string('euro1');
            $table->string('funta');
            $table->string('funta1');
            $table->string('frank');
            $table->string('frank1');
            $table->integer('user_id')->unsigned()->index('staticpage_user_id_foreign');
            $table->integer('workflow_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kurs');
    }
}
