<?php

use Illuminate\Database\Seeder;

class BankomatTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('bankomat')->delete();
        
        \DB::table('bankomat')->insert(array (
            0 => 
            array (
                'id' => '4',
                'title' => 'ЕКСПОЗИТУРА ТЕТОВО 1',
                'slug' => 'ekspozitura-tetovo-1',
                'image' => 'ekspozitura-tetovo-1.jpg',
                'imagemedium' => 'ekspozitura-tetovo-1.jpg',
                'imagethumb' => 'ekspozitura-tetovo-1.jpg',
                'category' => '87',
                'cities' => '29',
                'lat' => '42.0104998908',
                'lng' => '20.9714330226',
                'description' => '<table border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td>&nbsp;Адреса:</td>
<td>&nbsp; &nbsp;Методија Андонов Ченто, 1</td>
</tr>
<tr>
<td>&nbsp;Телефон:</td>
<td>&nbsp; &nbsp;++ 389 2 244 60 00</td>
</tr>
<tr>
<td>Е-пошта:&nbsp;</td>
<td>&nbsp; &nbsp;<a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">info@procreditbank.com.mk</a><a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">&nbsp;</a></td>
</tr>
<tr>
<td>&nbsp;Работно време:</td>
<td>&nbsp; &nbsp;пон-пет 09:00 - 17:00</td>
</tr>
<tr>
<td>&nbsp;Работи во сабота:</td>
<td>&nbsp; &nbsp;09:00 - 13:00</td>
</tr>
<tr>
<td>Паркинг:</td>
<td>&nbsp; &nbsp;не</td>
</tr>
<tr>
<td>&nbsp;Зона 24/7:</td>
<td>&nbsp;&nbsp;&nbsp;да</td>
</tr>
<tr>
<td>
<p>&nbsp;Пристап за</p>

<p>инвалидска&nbsp;количка:</p>
</td>
<td>&nbsp; &nbsp;да</td>
</tr>
</tbody>
</table>
',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 09:23:28',
                'updated_at' => '2018-07-27 09:23:28',
            ),
            1 => 
            array (
                'id' => '5',
                'title' => 'DEGA TETOVË 1',
                'slug' => 'dega-tetove-1',
                'image' => 'dega-tetove-1.jpg',
                'imagemedium' => 'dega-tetove-1.jpg',
                'imagethumb' => 'dega-tetove-1.jpg',
                'category' => '87',
                'cities' => '29',
                'lat' => '42.0104998908',
                'lng' => '20.9714330226',
                'description' => '<table border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td>Adresa:</td>
<td>&nbsp; &nbsp;Metodija Andonov Cento, 1</td>
</tr>
<tr>
<td>&nbsp;Telefoni:</td>
<td>&nbsp; &nbsp;++ 389 2 244 60 00&nbsp;</td>
</tr>
<tr>
<td>Posta elektronike:&nbsp;</td>
<td>&nbsp; &nbsp;<a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">info@procreditbank.com.mk</a><a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">&nbsp;</a></td>
</tr>
<tr>
<td>&nbsp;Koha e pun&euml;s:</td>
<td>&nbsp; &nbsp;E h&euml;n&euml; - e premte 09:00 - 17:00</td>
</tr>
<tr>
<td>&nbsp;Punon t&euml; shtun&euml;n:</td>
<td>&nbsp; &nbsp;09:00 - 13:00</td>
</tr>
<tr>
<td>&nbsp;Parking:</td>
<td>&nbsp; &nbsp;jo</td>
</tr>
<tr>
<td>&nbsp;Zon&euml; 24/7:</td>
<td>&nbsp;&nbsp;&nbsp;po</td>
</tr>
<tr>
<td>&nbsp;Qasje p&euml;r karoc&euml;:&nbsp;</td>
<td>&nbsp;&nbsp;&nbsp;po</td>
</tr>
</tbody>
</table>
',
                'lang' => 'sq',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 09:26:26',
                'updated_at' => '2018-07-27 09:26:26',
            ),
            2 => 
            array (
                'id' => '6',
                'title' => 'BRANCH TETOVO 1',
                'slug' => 'branch-tetovo-1',
                'image' => 'branch-tetovo-1.jpg',
                'imagemedium' => 'branch-tetovo-1.jpg',
                'imagethumb' => 'branch-tetovo-1.jpg',
                'category' => '87',
                'cities' => '29',
                'lat' => '42.0105008876',
                'lng' => '20.9714343638',
                'description' => '<table border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td>Address:</td>
<td>&nbsp; &nbsp;Metodija Andonov Cento, 1</td>
</tr>
<tr>
<td>&nbsp;Telephone:</td>
<td>&nbsp; &nbsp;++ 389 2 244 60 00&nbsp;</td>
</tr>
<tr>
<td>Mail:&nbsp;</td>
<td>&nbsp; &nbsp;<a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">info@procreditbank.com.mk</a><a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">&nbsp;</a></td>
</tr>
<tr>
<td>&nbsp;Working Hours:</td>
<td>&nbsp; &nbsp;mon-fri 09:00 - 17:00</td>
</tr>
<tr>
<td>&nbsp;Saturdays:</td>
<td>&nbsp; &nbsp;09:00 - 13:00</td>
</tr>
<tr>
<td>&nbsp;Car park:</td>
<td>&nbsp; &nbsp;yes</td>
</tr>
<tr>
<td>&nbsp;Zone 24/7:</td>
<td>&nbsp;&nbsp;&nbsp;yes</td>
</tr>
<tr>
<td>
<p>&nbsp;Access</p>

<p>for&nbsp;wheelchair:</p>
</td>
<td>&nbsp; &nbsp;yes</td>
</tr>
</tbody>
</table>
',
                'lang' => 'en',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 09:28:07',
                'updated_at' => '2018-07-27 09:28:07',
            ),
            3 => 
            array (
                'id' => '7',
                'title' => 'ЦЕНТРАЛНА УПРАВА',
                'slug' => 'tsentralna-uprava',
                'image' => 'tsentralna-uprava.jpg',
                'imagemedium' => 'tsentralna-uprava.jpg',
                'imagethumb' => 'tsentralna-uprava.jpg',
                'category' => '87',
                'cities' => '33',
                'lat' => '42.0053530000',
                'lng' => '21.3866380000',
                'description' => '<table border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td>Адреса:</td>
<td>&nbsp; &nbsp;Манапо бр.7</td>
</tr>
<tr>
<td>&nbsp;Телефон:</td>
<td>&nbsp; &nbsp;++ 389 2 244 60 00&nbsp;</td>
</tr>
<tr>
<td>Е-пошта:&nbsp;</td>
<td>&nbsp; &nbsp;<a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">info@procreditbank.com.mk</a><a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">&nbsp;</a></td>
</tr>
<tr>
<td>&nbsp;Работно време:</td>
<td>&nbsp; &nbsp;пон-пет 08:30 - 16:30</td>
</tr>
<tr>
<td>&nbsp;Работи во сабота:</td>
<td>&nbsp;&nbsp; не</td>
</tr>
<tr>
<td>&nbsp;Паркинг:</td>
<td>&nbsp; &nbsp;да</td>
</tr>
<tr>
<td>&nbsp;Зона 24/7:</td>
<td>&nbsp; &nbsp;да</td>
</tr>
<tr>
<td>
<p>&nbsp;Пристап за</p>

<p>инвалидска&nbsp;количка:</p>
</td>
<td>&nbsp; &nbsp;да</td>
</tr>
</tbody>
</table>
',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 09:35:46',
                'updated_at' => '2018-07-27 09:35:46',
            ),
            4 => 
            array (
                'id' => '8',
                'title' => 'ЕКСПОЗИТУРА КАРПОШ',
                'slug' => 'ekspozitura-karposh',
                'image' => 'ekspozitura-karposh.jpg',
                'imagemedium' => 'ekspozitura-karposh.jpg',
                'imagethumb' => 'ekspozitura-karposh.jpg',
                'category' => '87',
                'cities' => '33',
                'lat' => '42.0053530000',
                'lng' => '21.3866380000',
                'description' => '<table border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td>&nbsp;Адреса:</td>
<td>&nbsp;&nbsp;&nbsp;Манапо бр.7</td>
</tr>
<tr>
<td>&nbsp;Телефон:</td>
<td>&nbsp; &nbsp;+ 389 2 244 60 00</td>
</tr>
<tr>
<td>Е-пошта:&nbsp;</td>
<td>&nbsp; &nbsp;<a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">info@procreditbank.com.mk</a><a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">&nbsp;</a></td>
</tr>
<tr>
<td>&nbsp;Работно време:</td>
<td>&nbsp; &nbsp;пон-пет 08:00 - 18:00</td>
</tr>
<tr>
<td>&nbsp;Работи во сабота:</td>
<td>&nbsp; &nbsp;да , 09:00 - 13:00</td>
</tr>
<tr>
<td>Паркинг:</td>
<td>&nbsp; &nbsp;да</td>
</tr>
<tr>
<td>Пристап за
<p>инвалидска&nbsp;количка:</p>
</td>
<td>&nbsp;&nbsp;&nbsp;да</td>
</tr>
<tr>
<td>
<p>Зона 24/7:&nbsp;</p>
</td>
<td>&nbsp;&nbsp;&nbsp;да</td>
</tr>
</tbody>
</table>
',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 09:36:32',
                'updated_at' => '2018-07-27 09:36:32',
            ),
            5 => 
            array (
                'id' => '9',
                'title' => 'ЕКСПОЗИТУРА КАПИШТЕЦ ',
                'slug' => 'ekspozitura-kapishtets',
                'image' => 'ekspozitura-kapishtets.png',
                'imagemedium' => 'ekspozitura-kapishtets.png',
                'imagethumb' => 'ekspozitura-kapishtets.png',
                'category' => '87',
                'cities' => '33',
                'lat' => '41.9946687275',
                'lng' => '21.4101047908',
                'description' => '<table border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td>&nbsp;Адреса:</td>
<td>&nbsp; &nbsp;Јуриј Гагарин, 17</td>
</tr>
<tr>
<td>&nbsp;Телефон:</td>
<td>&nbsp; &nbsp;++ 389 2 244 60 00</td>
</tr>
<tr>
<td>Е-пошта:&nbsp;</td>
<td>&nbsp; &nbsp;<a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">info@procreditbank.com.mk</a><a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">&nbsp;</a></td>
</tr>
<tr>
<td>&nbsp;Работно време:</td>
<td>&nbsp; &nbsp;пон-пет 09:00 - 17:00</td>
</tr>
<tr>
<td>&nbsp;Работи во сабота:</td>
<td>&nbsp; &nbsp;не</td>
</tr>
<tr>
<td>Паркинг:</td>
<td>&nbsp; &nbsp;да</td>
</tr>
<tr>
<td>&nbsp;Пристап за
<p>инвалидска&nbsp;количка:</p>

<p></p>
</td>
<td>&nbsp;&nbsp;&nbsp;да</td>
</tr>
<tr>
<td>
<p>Зона 24/7&nbsp;:</p>
</td>
<td>&nbsp;&nbsp;да</td>
</tr>
</tbody>
</table>
',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 09:38:02',
                'updated_at' => '2018-07-27 09:38:02',
            ),
            6 => 
            array (
                'id' => '10',
                'title' => 'HEAD OFFICE',
                'slug' => 'head-office',
                'image' => 'head-office.jpg',
                'imagemedium' => 'head-office.jpg',
                'imagethumb' => 'head-office.jpg',
                'category' => '87',
                'cities' => '33',
                'lat' => '42.0053530000',
                'lng' => '21.3866380000',
                'description' => '<table border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td>&nbsp;Address:</td>
<td>&nbsp; &nbsp;Manapo, no.7</td>
</tr>
<tr>
<td>&nbsp;Telephone:</td>
<td>&nbsp; &nbsp;++ 389 2 244 60 00&nbsp;</td>
</tr>
<tr>
<td>Mail:&nbsp;</td>
<td>&nbsp; &nbsp;<a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">info@procreditbank.com.mk</a><a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">&nbsp;</a></td>
</tr>
<tr>
<td>&nbsp;Working Hours:</td>
<td>&nbsp; &nbsp;mon-fri 08:30 - 16:30</td>
</tr>
<tr>
<td>&nbsp;Saturdays:</td>
<td>&nbsp; &nbsp;no</td>
</tr>
<tr>
<td>&nbsp;Car park:</td>
<td>&nbsp;&nbsp;&nbsp;yes</td>
</tr>
<tr>
<td>&nbsp;Zone 24/7:</td>
<td>&nbsp; &nbsp;yes</td>
</tr>
<tr>
<td>
<p>&nbsp;Access</p>

<p>for&nbsp;wheelchair:</p>
</td>
<td>&nbsp; &nbsp;yes</td>
</tr>
</tbody>
</table>
',
                'lang' => 'en',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 09:45:15',
                'updated_at' => '2018-07-27 09:45:15',
            ),
            7 => 
            array (
                'id' => '11',
                'title' => 'BRANCH KARPOS',
                'slug' => 'branch-karpos',
                'image' => 'branch-karpos.jpg',
                'imagemedium' => 'branch-karpos.jpg',
                'imagethumb' => 'branch-karpos.jpg',
                'category' => '87',
                'cities' => '33',
                'lat' => '42.0053530000',
                'lng' => '21.3866380000',
                'description' => '<table border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td>&nbsp;Address:</td>
<td>&nbsp;&nbsp;&nbsp;Manapo no.7</td>
</tr>
<tr>
<td>&nbsp;Telephone:</td>
<td>&nbsp; &nbsp;++ 389 2 244 60 00&nbsp;</td>
</tr>
<tr>
<td>Mail:&nbsp;</td>
<td>&nbsp; &nbsp;<a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">info@procreditbank.com.mk</a><a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">&nbsp;</a></td>
</tr>
<tr>
<td>&nbsp;Working Hours:</td>
<td>&nbsp; &nbsp;mon-fri 08:00 - 18:00</td>
</tr>
<tr>
<td>&nbsp;Saturdays:</td>
<td>&nbsp;&nbsp;&nbsp;09:00 - 13:00</td>
</tr>
<tr>
<td>&nbsp;Car park:</td>
<td>&nbsp; &nbsp;yes</td>
</tr>
<tr>
<td>&nbsp;Zone 24/7:</td>
<td>&nbsp;&nbsp;&nbsp;yes</td>
</tr>
<tr>
<td>
<p>&nbsp;Access</p>

<p>for&nbsp;wheelchair:</p>
</td>
<td>&nbsp;&nbsp;&nbsp;yes</td>
</tr>
</tbody>
</table>
',
                'lang' => 'en',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 09:46:24',
                'updated_at' => '2018-07-27 09:46:24',
            ),
            8 => 
            array (
                'id' => '12',
                'title' => 'BRANCH KAPISTEC',
                'slug' => 'branch-kapistec',
                'image' => 'branch-kapistec.png',
                'imagemedium' => 'branch-kapistec.png',
                'imagethumb' => 'branch-kapistec.png',
                'category' => '87',
                'cities' => '33',
                'lat' => '41.9946707166',
                'lng' => '21.4101043244',
                'description' => '<table border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td>Address:</td>
<td>&nbsp; &nbsp;Jurij Gagarin, 17</td>
</tr>
<tr>
<td>&nbsp;Telephone:</td>
<td>&nbsp; &nbsp;++ 389 2 244 60 00&nbsp;</td>
</tr>
<tr>
<td>Mail:&nbsp;</td>
<td>&nbsp; &nbsp;<a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">info@procreditbank.com.mk</a><a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">&nbsp;</a></td>
</tr>
<tr>
<td>&nbsp;Working Hours:</td>
<td>&nbsp; &nbsp;mon-fri 09:00 - 17:00</td>
</tr>
<tr>
<td>&nbsp;Saturdays:</td>
<td>&nbsp; &nbsp;no</td>
</tr>
<tr>
<td>&nbsp;Car park:</td>
<td>&nbsp; &nbsp;yes</td>
</tr>
<tr>
<td>&nbsp;Zone 24/7:</td>
<td>&nbsp;&nbsp;&nbsp;yes</td>
</tr>
<tr>
<td>
<p>&nbsp;Access</p>

<p>for&nbsp;wheelchair:</p>
</td>
<td>&nbsp; &nbsp;yes</td>
</tr>
</tbody>
</table>
',
                'lang' => 'en',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 09:48:27',
                'updated_at' => '2018-07-27 09:48:27',
            ),
            9 => 
            array (
                'id' => '13',
                'title' => 'Банкомат - Велес',
                'slug' => 'bankomat-veles',
                'image' => 'bankomat-veles.png',
                'imagemedium' => 'bankomat-veles.png',
                'imagethumb' => 'bankomat-veles.png',
                'category' => '86',
                'cities' => '31',
                'lat' => '41.7142499181',
                'lng' => '21.7671253010',
                'description' => 'Адреса: ул. Благој Ѓорев бр.107<br />
Спроти Трговски Центар во Нова населба<br />
Тип на банкомати: Уплата/ Исплата во МКД/ЕУР',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:09:19',
                'updated_at' => '2018-07-27 10:09:19',
            ),
            10 => 
            array (
                'id' => '14',
                'title' => 'Банкомат - Битола',
                'slug' => 'bankomat-bitola',
                'image' => 'bankomat-bitola.png',
                'imagemedium' => 'bankomat-bitola.png',
                'imagethumb' => 'bankomat-bitola.png',
                'category' => '86',
                'cities' => '34',
                'lat' => '41.0298931708',
                'lng' => '21.3363771895',
                'description' => 'Адреса: ул. Добривое Радосавлевиќ бр.10<br />
Тип на банкомати: Сеф за дневен промет, Уплати/ Исплати во МКД/ЕУР',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:10:45',
                'updated_at' => '2018-07-27 10:10:45',
            ),
            11 => 
            array (
                'id' => '15',
                'title' => 'Банкомат - Гостивар',
                'slug' => 'bankomat-gostivar',
                'image' => 'bankomat-gostivar.png',
                'imagemedium' => 'bankomat-gostivar.png',
                'imagethumb' => 'bankomat-gostivar.png',
                'category' => '86',
                'cities' => '20',
                'lat' => '41.7913247054',
                'lng' => '20.9076395534',
                'description' => 'Адреса :ул. Браќа Ѓиноски<br />
(спроти општината)<br />
Тип на банкомати: Сеф за дневен промет, Уплата/Исплата во МКД/ЕУР',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:11:46',
                'updated_at' => '2018-07-27 10:11:46',
            ),
            12 => 
            array (
                'id' => '16',
                'title' => 'Банкомат - Кавадрци',
                'slug' => 'bankomat-kavadrtsi',
                'image' => 'bankomat-kavadrtsi.png',
                'imagemedium' => 'bankomat-kavadrtsi.png',
                'imagethumb' => 'bankomat-kavadrtsi.png',
                'category' => '86',
                'cities' => '21',
                'lat' => '41.4329364000',
                'lng' => '22.0088861000',
                'description' => '',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:17:44',
                'updated_at' => '2018-07-27 10:17:44',
            ),
            13 => 
            array (
                'id' => '17',
                'title' => 'Банкомат - Куманово',
                'slug' => 'bankomat-kumanovo',
                'image' => 'bankomat-kumanovo.png',
                'imagemedium' => 'bankomat-kumanovo.png',
                'imagethumb' => 'bankomat-kumanovo.png',
                'category' => '86',
                'cities' => '22',
                'lat' => '42.1370112000',
                'lng' => '21.7180196000',
                'description' => 'Адреса: Доне Божинов, зграда 1<br />
Тип на банкомати: Сеф за дневен промет, Уплата/Исплата во МКД/ЕУР/УСД',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:19:35',
                'updated_at' => '2018-07-27 10:19:35',
            ),
            14 => 
            array (
                'id' => '18',
                'title' => 'ЕКСПОЗИТУРА КУМАНОВО',
                'slug' => 'ekspozitura-kumanovo',
                'image' => 'ekspozitura-kumanovo.jpg',
                'imagemedium' => 'ekspozitura-kumanovo.jpg',
                'imagethumb' => 'ekspozitura-kumanovo.jpg',
                'category' => '87',
                'cities' => '22',
                'lat' => '42.1370500351',
                'lng' => '21.7180221151',
                'description' => '<table border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td>&nbsp;Адреса:</td>
<td>&nbsp; &nbsp;Доне Божинов, зграда 1</td>
</tr>
<tr>
<td>&nbsp;Телефон:</td>
<td>&nbsp; &nbsp;+ 389 2 244 60 00&nbsp;</td>
</tr>
<tr>
<td>Е-пошта:&nbsp;</td>
<td>&nbsp; &nbsp;<a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">info@procreditbank.com.mk</a><a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">&nbsp;</a></td>
</tr>
<tr>
<td>&nbsp;Работно време:</td>
<td>&nbsp; &nbsp;пон-пет 09:00 - 17:00</td>
</tr>
<tr>
<td>&nbsp;Работи во сабота:</td>
<td>&nbsp; &nbsp;не</td>
</tr>
<tr>
<td>&nbsp;Сопствен паркинг:</td>
<td>&nbsp; &nbsp;не</td>
</tr>
<tr>
<td>&nbsp;Зона 24/7:</td>
<td>&nbsp; &nbsp;да</td>
</tr>
<tr>
<td>
<p>&nbsp;Пристап за</p>

<p>инвалидска&nbsp;количка:</p>
</td>
<td>&nbsp; &nbsp;да</td>
</tr>
</tbody>
</table>
',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:20:51',
                'updated_at' => '2018-07-27 10:20:51',
            ),
            15 => 
            array (
                'id' => '19',
                'title' => 'Банкомат - Охрид',
                'slug' => 'bankomat-okhrid',
                'image' => 'bankomat-okhrid.png',
                'imagemedium' => 'bankomat-okhrid.png',
                'imagethumb' => 'bankomat-okhrid.png',
                'category' => '86',
                'cities' => '24',
                'lat' => '41.1165236758',
                'lng' => '20.8028432908',
                'description' => 'Адреса: Туристичка бб<br />
Тип на банкомати: Уплата/ Исплата во МКД/ЕУР',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:22:04',
                'updated_at' => '2018-07-27 10:22:04',
            ),
            16 => 
            array (
                'id' => '20',
                'title' => 'ЕКСПОЗИТУРА СТРУГА',
                'slug' => 'ekspozitura-struga',
                'image' => 'ekspozitura-struga.jpg',
                'imagemedium' => 'ekspozitura-struga.jpg',
                'imagethumb' => 'ekspozitura-struga.jpg',
                'category' => '86',
                'cities' => '27',
                'lat' => '41.1788202000',
                'lng' => '20.6762418000',
                'description' => '<table border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td>&nbsp;Адреса:</td>
<td>&nbsp; &nbsp;Пролетерска Бригада, бб</td>
</tr>
<tr>
<td>&nbsp;Телефон:</td>
<td>&nbsp; &nbsp;++ 389 2 244 60 00</td>
</tr>
<tr>
<td>Е-пошта:&nbsp;</td>
<td>&nbsp; &nbsp;<a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">info@procreditbank.com.mk</a><a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">&nbsp;</a></td>
</tr>
<tr>
<td>&nbsp;Работно време:</td>
<td>&nbsp; &nbsp;пон-пет 09:00 - 17:00</td>
</tr>
<tr>
<td>&nbsp;Работи во сабота:</td>
<td>&nbsp;&nbsp;&nbsp;не</td>
</tr>
<tr>
<td>Паркинг:</td>
<td>&nbsp; &nbsp;не</td>
</tr>
<tr>
<td>&nbsp;Зона 24/7:</td>
<td>&nbsp;&nbsp;&nbsp;да</td>
</tr>
<tr>
<td>
<p>&nbsp;Пристап за</p>

<p>инвалидска&nbsp;количка:</p>
</td>
<td>&nbsp; &nbsp;не</td>
</tr>
</tbody>
</table>
',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:24:42',
                'updated_at' => '2018-07-27 10:24:42',
            ),
            17 => 
            array (
                'id' => '21',
                'title' => 'Банкомат - Струга',
                'slug' => 'bankomat-struga',
                'image' => 'bankomat-struga.png',
                'imagemedium' => 'bankomat-struga.png',
                'imagethumb' => 'bankomat-struga.png',
                'category' => '86',
                'cities' => '27',
                'lat' => '41.1788202000',
                'lng' => '20.6762418000',
                'description' => 'Адреса: Пролетерска бригада бб<br />
Тип на банкомат: Исплата и уплата во МКД/ ЕУР, &nbsp;сеф за дневен промет',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:25:57',
                'updated_at' => '2018-07-27 10:25:57',
            ),
            18 => 
            array (
                'id' => '22',
                'title' => 'ЕКСПОЗИТУРА БИТОЛА',
                'slug' => 'ekspozitura-bitola',
                'image' => 'ekspozitura-bitola.jpg',
                'imagemedium' => 'ekspozitura-bitola.jpg',
                'imagethumb' => 'ekspozitura-bitola.jpg',
                'category' => '87',
                'cities' => '34',
                'lat' => '41.0302642000',
                'lng' => '21.3359956000',
                'description' => '<table border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td>&nbsp;Адреса:</td>
<td>&nbsp; &nbsp;Добривое Радосављевиќ 10</td>
</tr>
<tr>
<td>&nbsp;Телефон:</td>
<td>&nbsp; &nbsp;++ 389 2 244 60 00</td>
</tr>
<tr>
<td>Е-пошта:&nbsp;</td>
<td>&nbsp; &nbsp;<a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">info@procreditbank.com.mk</a><a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">&nbsp;</a></td>
</tr>
<tr>
<td>&nbsp;Работно време:</td>
<td>&nbsp; &nbsp;пон-пет 09:00 - 17:00</td>
</tr>
<tr>
<td>&nbsp;Работи во сабота:</td>
<td>&nbsp;&nbsp;&nbsp;не</td>
</tr>
<tr>
<td>Паркинг</td>
<td>&nbsp; &nbsp;не</td>
</tr>
<tr>
<td>
<p>Зона 24/7</p>
</td>
<td>&nbsp;&nbsp;&nbsp;да</td>
</tr>
<tr>
<td>
<p>&nbsp;Пристап за</p>

<p>инвалидска&nbsp;количка</p>

<p></p>
</td>
<td>&nbsp;не</td>
</tr>
</tbody>
</table>
',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:26:18',
                'updated_at' => '2018-07-27 10:26:18',
            ),
            19 => 
            array (
                'id' => '23',
                'title' => 'Банкомат - Струмица',
                'slug' => 'bankomat-strumitsa',
                'image' => 'bankomat-strumitsa.png',
                'imagemedium' => 'bankomat-strumitsa.png',
                'imagethumb' => 'bankomat-strumitsa.png',
                'category' => '86',
                'cities' => '28',
                'lat' => '41.4373136000',
                'lng' => '22.6371736000',
                'description' => 'Адреса: Благој Јанков Мучето 2<br />
Тип на банкомати: Сеф за дневен промет, Уплата/Исплата во МКД/ЕУР',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:27:02',
                'updated_at' => '2018-07-27 10:27:02',
            ),
            20 => 
            array (
                'id' => '24',
                'title' => 'ЕКСПОЗИТУРА СТРУМИЦА',
                'slug' => 'ekspozitura-strumitsa',
                'image' => 'ekspozitura-strumitsa.jpg',
                'imagemedium' => 'ekspozitura-strumitsa.jpg',
                'imagethumb' => 'ekspozitura-strumitsa.jpg',
                'category' => '87',
                'cities' => '28',
                'lat' => '41.4392704526',
                'lng' => '22.6381545479',
                'description' => '',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:29:08',
                'updated_at' => '2018-07-27 10:29:08',
            ),
            21 => 
            array (
                'id' => '25',
                'title' => 'Банкомат - Тетово',
                'slug' => 'bankomat-tetovo',
                'image' => 'bankomat-tetovo.png',
                'imagemedium' => 'bankomat-tetovo.png',
                'imagethumb' => 'bankomat-tetovo.png',
                'category' => '86',
                'cities' => '29',
                'lat' => '42.0104530570',
                'lng' => '20.9713968129',
                'description' => '',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:33:00',
                'updated_at' => '2018-07-27 10:33:00',
            ),
            22 => 
            array (
                'id' => '26',
                'title' => 'ЕКСПОЗИТУРА ШТИП',
                'slug' => 'ekspozitura-shtip',
                'image' => 'ekspozitura-shtip.jpg',
                'imagemedium' => 'ekspozitura-shtip.jpg',
                'imagethumb' => 'ekspozitura-shtip.jpg',
                'category' => '87',
                'cities' => '16',
                'lat' => '41.7464290000',
                'lng' => '22.1996540000',
                'description' => '<table border="0" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td>&nbsp;Адреса:</td>
<td>&nbsp; &nbsp;Тоше Арсов, 46</td>
</tr>
<tr>
<td>&nbsp;Телефон:</td>
<td>&nbsp; &nbsp;++ 389 2 244 60 00</td>
</tr>
<tr>
<td>Е-пошта:&nbsp;</td>
<td>&nbsp; &nbsp;<a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">info@procreditbank.com.mk</a><a href="http://staging.procreditbank.com.mk/mailto:info@procreditbank.com.mk">&nbsp;</a></td>
</tr>
<tr>
<td>&nbsp;Работно време:</td>
<td>&nbsp; &nbsp;пон-пет 09:00 - 17:00</td>
</tr>
<tr>
<td>&nbsp;Работи во сабота:</td>
<td>&nbsp; &nbsp;не</td>
</tr>
<tr>
<td>&nbsp;Паркинг:</td>
<td>&nbsp; &nbsp;да</td>
</tr>
<tr>
<td>&nbsp;Зона 24/7:</td>
<td>&nbsp;&nbsp;&nbsp;да</td>
</tr>
<tr>
<td>
<p>&nbsp;Пристап за</p>

<p>инвалидска&nbsp;количка:</p>
</td>
<td>&nbsp; &nbsp;не</td>
</tr>
</tbody>
</table>
',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:34:14',
                'updated_at' => '2018-07-27 10:34:14',
            ),
            23 => 
            array (
                'id' => '27',
                'title' => 'Банкомат - Прилеп',
                'slug' => 'bankomat-prilep',
                'image' => 'bankomat-prilep.png',
                'imagemedium' => 'bankomat-prilep.png',
                'imagethumb' => 'bankomat-prilep.png',
                'category' => '86',
                'cities' => '25',
                'lat' => '41.3474977358',
                'lng' => '21.5561023509',
                'description' => 'Адреса: ул.Маршал Тито бр.1/1<br />
Тип на банкомати: Уплата, Испалта во МКД/ ЕУР',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:38:34',
                'updated_at' => '2018-07-27 10:38:34',
            ),
            24 => 
            array (
                'id' => '28',
                'title' => 'Банкомат - Штип',
                'slug' => 'bankomat-shtip',
                'image' => 'bankomat-shtip.png',
                'imagemedium' => 'bankomat-shtip.png',
                'imagethumb' => 'bankomat-shtip.png',
                'category' => '86',
                'cities' => '16',
                'lat' => '41.7372295285',
                'lng' => '22.1922902908',
                'description' => '',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:40:57',
                'updated_at' => '2018-07-27 10:40:57',
            ),
            25 => 
            array (
                'id' => '29',
                'title' => 'Банкомат - Скопје - Карпош 4',
                'slug' => 'bankomat-karposh-4-skopje',
                'image' => 'bankomat-karposh-4-skopje.png',
                'imagemedium' => 'bankomat-karposh-4-skopje.png',
                'imagethumb' => 'bankomat-karposh-4-skopje.png',
                'category' => '86',
                'cities' => '33',
                'lat' => '42.0073671000',
                'lng' => '21.3929341000',
                'description' => 'ул. Манапо бр.7, Скопје<br />
Тип на банкомат: Уплата и исплата во ЕУР/МКД, Сеф за дневен промет, Уплата на кованици',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:43:38',
                'updated_at' => '2018-07-27 11:06:45',
            ),
            26 => 
            array (
                'id' => '30',
                'title' => 'Банкомат - Скопје - Капиштец',
                'slug' => 'bankomat-skopje-kapishtets',
                'image' => 'bankomat-skopje-kapishtets.png',
                'imagemedium' => 'bankomat-skopje-kapishtets.png',
                'imagethumb' => 'bankomat-skopje-kapishtets.png',
                'category' => '86',
                'cities' => '33',
                'lat' => '41.9949199000',
                'lng' => '21.4098245000',
                'description' => 'Адреса: Јуриј Гагарин 17<br />
Тип на банкомати:&nbsp;Исплати во МКД&nbsp;<br />
Банкомати во експозитура: Сеф за дневен промет,&nbsp;Уплати/ Исплати во МКД/ЕУР',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 10:44:26',
                'updated_at' => '2018-07-27 10:44:26',
            ),
            27 => 
            array (
                'id' => '31',
                'title' => 'Банкомат - Скопје - Центар',
                'slug' => 'bankomat-skopje-tsentar',
                'image' => 'bankomat-skopje-tsentar.png',
                'imagemedium' => 'bankomat-skopje-tsentar.png',
                'imagethumb' => 'bankomat-skopje-tsentar.png',
                'category' => '86',
                'cities' => '33',
                'lat' => '41.9922574600',
                'lng' => '21.4266674275',
                'description' => 'Мито Хаџивасилев Јасмин бб',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 11:04:33',
                'updated_at' => '2018-07-27 11:04:33',
            ),
            28 => 
            array (
                'id' => '32',
                'title' => 'Банкомат - Скопје - Ново лисиче',
                'slug' => 'bankomat-skopje-novo-lisiche',
                'image' => 'bankomat-skopje-novo-lisiche.png',
                'imagemedium' => 'bankomat-skopje-novo-lisiche.png',
                'imagethumb' => 'bankomat-skopje-novo-lisiche.png',
                'category' => '86',
                'cities' => '33',
                'lat' => '41.9821712445',
                'lng' => '21.4730078380',
                'description' => 'Адреса: Видое Смилевски Бато 3<br />
Тип на банкомати: Сеф за дневен промет, Уплати/ Исплати во МКД/ЕУР',
                'lang' => 'mk',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 11:05:49',
                'updated_at' => '2018-07-27 11:05:49',
            ),
        ));
        
        
    }
}