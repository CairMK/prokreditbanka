<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(ConnectRelationshipsSeeder::class);
        $this->call(ThemesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(WorkflowTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(CountriesSeeder::class);
        $this->call(NeighborhoodTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(SettigsTableSeeder::class);
        $this->call(AtmTableSeeder::class);
        $this->call(StaticpageTableSeeder::class);
        $this->call(BankomatTableSeeder::class);
        $this->call(SliderTableSeeder::class);
        $this->call(SlidersTableSeeder::class);


        Model::reguard();
        $this->call(FeaturedTableSeeder::class);
    }
}
