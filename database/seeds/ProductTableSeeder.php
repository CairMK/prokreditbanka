<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product')->delete();
        
        \DB::table('product')->insert(array (
            0 => 
            array (
                'id' => '15',
                'title' => '',
                'slug' => '',
                'image' => '.jpg',
                'imagemedium' => '.jpg',
                'imagethumb' => '.jpg',
                'category' => '1',
                'description' => '',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-24 11:43:33',
                'updated_at' => '2018-07-24 11:43:33',
                'cities' => '0',
                'neighborhood' => '0',
                'countries' => '0',
                'keywords' => 'page, static',
                'lang' => 'mk',
                'featured_id' => '1',
                'title1' => '',
                'description1' => '',
                'title2' => '',
                'description2' => '',
                'title3' => '',
                'description3' => '',
                'title4' => '',
                'description4' => '',
                'title5' => '',
                'description5' => '',
                'title6' => '',
                'description6' => '',
            ),
            1 => 
            array (
                'id' => '18',
                'title' => '',
                'slug' => '-1',
                'image' => '-1.jpg',
                'imagemedium' => '-1.jpg',
                'imagethumb' => '-1.jpg',
                'category' => '1',
                'description' => '',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-24 11:54:29',
                'updated_at' => '2018-07-24 11:54:29',
                'cities' => '0',
                'neighborhood' => '0',
                'countries' => '0',
                'keywords' => 'page, static',
                'lang' => 'mk',
                'featured_id' => '1',
                'title1' => '',
                'description1' => '',
                'title2' => '',
                'description2' => '',
                'title3' => '',
                'description3' => '',
                'title4' => '',
                'description4' => '',
                'title5' => '',
                'description5' => '',
                'title6' => '',
                'description6' => '',
            ),
            2 => 
            array (
                'id' => '19',
                'title' => '',
                'slug' => '-2',
                'image' => '-2.jpg',
                'imagemedium' => '-2.jpg',
                'imagethumb' => '-2.jpg',
                'category' => '1',
                'description' => '',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-24 11:54:40',
                'updated_at' => '2018-07-24 11:54:40',
                'cities' => '0',
                'neighborhood' => '0',
                'countries' => '0',
                'keywords' => 'page, static',
                'lang' => 'mk',
                'featured_id' => '1',
                'title1' => '',
                'description1' => '',
                'title2' => '',
                'description2' => '',
                'title3' => '',
                'description3' => '',
                'title4' => '',
                'description4' => '',
                'title5' => '',
                'description5' => '',
                'title6' => '',
                'description6' => '',
            ),
        ));
        
        
    }
}