<?php

use Illuminate\Database\Seeder;

class SliderTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('slider')->delete();
        
        \DB::table('slider')->insert(array (
            0 => 
            array (
                'id' => '4',
                'title' => '',
                'image' => '.jpg',
                'imagemedium' => '.jpg',
                'imagethumb' => '.jpg',
                'link' => 'https://www.procreditbank-direct.com/macedonia/mk',
                'description' => '',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 08:31:48',
                'updated_at' => '2018-07-27 08:31:48',
                'lang' => 'mk',
            ),
            1 => 
            array (
                'id' => '5',
                'title' => '',
                'image' => '-1.jpg',
                'imagemedium' => '-1.jpg',
                'imagethumb' => '-1.jpg',
                'link' => 'https://www.procreditbank-direct.com/macedonia/mk',
                'description' => '',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 08:31:58',
                'updated_at' => '2018-07-27 08:31:58',
                'lang' => 'mk',
            ),
            2 => 
            array (
                'id' => '6',
                'title' => '',
                'image' => '-2.jpg',
                'imagemedium' => '-2.jpg',
                'imagethumb' => '-2.jpg',
                'link' => 'https://www.procreditbank-direct.com/macedonia/mk',
                'description' => '',
                'user_id' => '1',
                'workflow_id' => '1',
                'created_at' => '2018-07-27 08:32:10',
                'updated_at' => '2018-07-27 08:32:10',
                'lang' => 'mk',
            ),
        ));
        
        
    }
}