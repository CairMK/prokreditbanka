<?php

use Illuminate\Database\Seeder;

class WorkflowTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('workflow')->delete();
        
        \DB::table('workflow')->insert(array (
            0 => 
            array (
                'id' => '1',
                'name' => 'Published',
                'color' => 'colored-palegreen',
            ),
            1 => 
            array (
                'id' => '2',
                'name' => 'Waiting',
                'color' => 'colored-orange',
            ),
        ));
        
        
    }
}