

(function($) {

$('#businessClient-dropdown1').click(function(){
  $('#navbarNavDropdown,#businessClient-dropdown,#aboutUs-dropdown,#eco-dropdown').removeClass('show');
});

$(function() {
    var xpathname = window.location.pathname;
    if (xpathname == '/') {
        $('body').addClass('page-home');
    }
});


$(document).ready(function() {
$('.page-home .dynamic-element').remove();
});



$(document).ready(function() {
    
    var hash = window.location.hash;
    if(hash) {
        $("a[data-target='"+hash+"']").click()
    }
	$(".dropdown-menu a").click(function() {
		setTimeout(function() {
		var hash = window.location.hash;
		$("a[data-target='"+hash+"']").click()	
		console.log(hash);
		},200);
	});
    
    
$('.priceformat').each(function( index ) {
    $(this).priceFormat({ prefix: '',  thousandsSeparator: '' });
});

function calculator(){
$('#reset-btn').click(function(e) {
e.preventDefault();
  $(".form")[0].reset();
  $('.results-calc').fadeOut();

});

$('#presmetaj').click(function(e) {
  e.preventDefault();
   rocnost = parseInt( $('.rocnost').val()); 
 suma = parseInt( $('#sumaInput').val());
  suma1 = parseInt( $('#sumaOutput').val());
 
if(($('.valuta').val() ==="MKD" && suma1 < 300000 ) || $('.valuta').val() ==="EUR" && suma1 < 5000 ) {
    alert('Не можете да внесете помала сума од дадената');
   
}
else {
$('.results-calc').fadeIn();
 if ( $('.valuta').val() ==="MKD") {
  kamata= parseFloat( $('select.rocnost').find(':selected').data('kamataden')/10);
  valuta_label=" den."
 } 
 else if ($('.valuta').val() ==="EUR"){
  kamata= parseFloat( $('select.rocnost').find(':selected').data('kamataeur')/10);
  valuta_label=" €";
 }
}


result = ( (kamata/10)*suma) + suma;

$('.valuta-res span').text($('.valuta').val());
$('.suma-res span').text(suma);
$('.suma-res em').text(valuta_label);
$('.rocnost-res span').text(rocnost);
$('.kamata-res span').text(kamata);

$('#calcResult span').text(result);
$('#calcResult em').text(valuta_label);


});

}

calculator();
});


$(document).ready(function() {

function calculator1(){
$('#reset-btn').click(function(e) {
e.preventDefault();
  $(".form")[0].reset();
  $('.results-calc').fadeOut();

});

 $('select.tip').on('change',function(){
        
        var val = $("select option:selected").val();
       if (val == 1) {
            $('#mkd-choice').attr('disabled','true');  
        $('p.small-rok span').text(240);
          $('p.small-range').text("Минималната сума за овој тип на кредит е 30.000 евра, а максималната е неограничена");
        $('#sumaInput').attr('max','1000000');
        $('#sumaOutput').attr('max','1000000');
        $('#sumaInput').attr('min','30000');
        $('#sumaOutput').attr('min','30000');
        $('#sumaInput').val(30000);
        $('#sumaOutput').val(30000);
        
        
        $('#rokInput').attr('max','240');
        $('#rokOutput').attr('max','240');
        $('#rokInput').val(12);
        $('#rokOutput').val(12);
       }
              else if (val ==2 ) {

           
        $('#mkd-choice').removeAttr('disabled');             
       $('#sumaInput').attr('min','10000');
        $('#sumaOutput').attr('min','10000');
        $('#sumaInput').attr('max','30000');
        $('#sumaOutput').attr('max','30000');
        $('#sumaInput').val(10000);
        $('#sumaOutput').val(10000);      
                  
        $('p.small-rok span').text(60);
        $('p.small-range').text("Минималната сума за овој тип на кредит е 10.000 евра, а максималната 30.000 евра");
       $('#rokInput').attr('max','60');
        $('#rokOutput').attr('max','60');
          $('#rokOutput').val(12);
          $('#rokInput').val(12);
       }
    });

 $('select.valuta').on('change',function(){
 var valuta_val = $(this).find("option:selected").val();

  if (valuta_val == "EUR") {
     
        $('#sumaInput').attr('min','10000');
        $('#sumaOutput').attr('min','10000');
        $('#sumaInput').attr('max','30000');
        $('#sumaOutput').attr('max','30000');
        $('#sumaInput').val(10000);
        $('#sumaOutput').val(10000);      
                  
        $('p.small-rok span').text(60);
        $('p.small-range').text("Минималната сума за овој тип на кредит е 10.000 евра, а максималната 30.000 евра");
        $('.price.price-price span').text('€');

  }

  else if (valuta_val =="MKD") {
      
        $('#sumaInput').attr('min','600000');
        $('#sumaInput').attr('max','1800000');
          $('#sumaOutput').attr('min','600000');
        $('#sumaOutput').attr('max','1800000');
        $('#sumaInput').val(600000);
        $('#sumaOutput').val(600000);      
                  
        $('p.small-rok span').text(60);
        $('p.small-range').text("Минималната сума за овој тип на кредит е 600.000 денари, а максималната 1.800.000 денари");
        $('.price.price-price span').text('ден.');
  }
  
  
});



$('#presmetaj1').click(function(e) {
  e.preventDefault();
$('.results-calc').fadeIn();
 if ( $('.tip').val() ==="1") {
  tip="Станбен кредит"; 
  formula=1;// vtora variabla za tip
 } 
 else if ($('.tip').val() ==="2"){
  tip="Кредит за инвестиции";
  formula=2;// vtora variabla za tip
 }

if  ($('select.valuta').val()==="EUR") {
  valuta_label = "€";
 }
else if  ($('select.valuta').val()==="MKD") {
  valuta_label = "ден.";
 }



if(formula==1){}
else if(formula==2) {


suma = parseInt( $('#sumaOutput').val());
rocnost = parseInt( $('#rokInput').val()); 
kamata_initial= parseFloat(6/100);
kamata=parseFloat( 1 + kamata_initial);
stepen= parseFloat( Math.pow(kamata,rocnost));
mesecna_kamata = (suma * kamata_initial * stepen  ) / ( (stepen ) -1);

mesecna_kamatna_stapka = 6;

vkupen_iznos = mesecna_kamata * rocnost;
vkupna_kamata = vkupen_iznos - suma;
ednokratna_provizija = 2.0;
iznos_ednokratna_provizija =(suma * ednokratna_provizija)/100;
podiganje_na_klient = vkupen_iznos - iznos_ednokratna_provizija;

}



$('.tip-res span').text(tip);
$('.suma-res span').text(suma.toFixed(2));
$('#calcResult span').text(mesecna_kamata.toFixed(2));
$('#calcResult em').text(valuta_label);

$('.kamata-res span').text(mesecna_kamatna_stapka);
$('.rocnost-res span').text(rocnost);
$('.vkupen-iznos-res span').text(vkupen_iznos.toFixed(2));
$('.vkupen-iznos-res em').text(valuta_label);
$('.vkupna-kamata-res span').text(vkupna_kamata.toFixed(2));
$('.vkupna-kamata-res em').text(valuta_label);
$('.ednokratna-provizija-res span').text(ednokratna_provizija);
$('.iznos-ednokratna-provizija-res span').text(ednokratna_provizija);
$('.iznos-ednokratna-provizija-res em').text(valuta_label);
$('.podiganje-na-klient-res span').text(podiganje_na_klient.toFixed(2));
$('.podiganje-na-klient-res em').text(valuta_label);
});

}



calculator1();
});


})(jQuery);
