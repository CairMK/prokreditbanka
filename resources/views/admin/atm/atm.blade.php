@extends('layouts.app')

@section('content')
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <p><a class="btn btn-labeled" href="/admin/atm/create"> <i class="btn-label fa fa-plus"></i>Add FAQ </a>
                </p>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>FAQ</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">

                                <tbody>
                                @foreach($atms as $atms)
                                    <tr>
                                        {{--<td>--}}
                                        {{--<div class="round-img">--}}
                                        {{--<a href="/admin/atm/{{ $atms->id }}/edit"><img--}}
                                        {{--src="/assets/img/atm/thumbnails/{{ $atms->image }}"--}}
                                        {{--alt=""></a>--}}
                                        {{--</div>--}}
                                        {{--</td>--}}
                                        <td><a href="/admin/atm/{{ $atms->id }}/edit">{{ $atms->title }}</a></td>
                                        <td>{{ $atms->address }}</td>
                                        <td><span>Workflow: {{ $atms->workflow->name }}</span></td>

                                    </tr>
                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 right">
                {{--{!! $atms->links() !!}--}}
            </div>

        </div>
    </div>
@endsection
