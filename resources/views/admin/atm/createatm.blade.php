@extends('layouts.app')
@section('content')

    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif
                <div class="widget-header bordered-bottom bordered-warning">
                    <span class="widget-caption">Add FAQ</span>
                </div>
                <div class="widget-body">
                    <div id="horizontal-form">
                        {{ Form::model('atm', array('route' => array('admin.atm.store'), 'method' => 'POST', 'files'=>true)) }}
                        {!! csrf_field() !!}
                        {{--<div class="input-group{{ $errors->has('image') ? ' has-error' : '' }}">--}}
                        {{--<span class="input-group-btn">--}}
                        {{--<span>--}}
                        {{--Browse... <input type="file" name="image">--}}
                        {{--</span>--}}
                        {{--</span>--}}
                        {{--</div>--}}
                        {{--<br/>--}}
                        {{--@if ($errors->has('image')) <p--}}
                        {{--class="alert alert-danger">{{ $errors->first('image') }}</p> @endif--}}
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" placeholder="Title" name="title">
                        </div>

                        @if ($errors->has('title')) <p
                                class="alert alert-danger">{{ $errors->first('title') }}</p> @endif
                        <div class="form-group">
                            <label for="description">Atm description</label>
                            <textarea class="ckeditor" id="elm3" name="description"></textarea>
                        </div>
                        @if ($errors->has('description')) <p
                                class="alert alert-danger">{{ $errors->first('description') }}</p> @endif
                        <br>
                        <div class="form-group">
                            <label for="title">Adrress</label>
                            <input type="text" class="form-control" placeholder="Address" name="address">
                        </div>

                        @if ($errors->has('title')) <p
                                class="alert alert-danger">{{ $errors->first('address') }}</p> @endif
                        <br>

                        <label for="lang">Izberete jazik: </label>
                        <select name="lang" id="lang" class="form-control">
                            <option value="mk">Makedonski</option>
                            <option value="en">Angliski</option>
                            <option value="sq">Albanski</option>
                        </select>
                        <div class="form-group">
                            <label for="user">User</label>
                            <select name="user_id" id="user" class="form-control">
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}"
                                            @if(Auth::user()->id == $user->id) selected @endif >{{ $user->name }}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                            <label>Workflow: </label>
                            @foreach($workflows as $workflow)
                                <label>
                                    <input name="workflow_id" type="radio"
                                           class="form-control {{ $workflow->color }}" value="{{ $workflow->id }}"
                                           @if($workflow->id  == 1) checked @endif>
                                    <span class="text"> {{ $workflow->name }}</span>
                                </label>
                            @endforeach
                        </div>
                        <!-- Hidden inputs -->
                        <input type="hidden" name="creator" value="{{ Auth::user()->id  }}">
                        <input type="hidden" id="lat" class="form-control" name="lat">
                        <input type="hidden" id="lng" class="form-control" name="lng">
                        <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                                    class="btn-label fa fa-plus"></i> Create
                        </button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div><!--zavrsuva widget -->
        </div>
    </div>
    </div>
@endsection
@section('scripts')
    <!-- Google Maps -->
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDv7dvAdPbvPV1By-YberT3NGyrLL1_v2c&callback=initMap">
    </script>
@endsection