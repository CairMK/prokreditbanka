@extends('layouts.app')

@section('content')
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <p><a class="btn btn-labeled" href="/admin/bankomat/create"> <i class="btn-label fa fa-plus"></i>Додади
                        банкомати и експозитури </a></p>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Банкомати и експозитури</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">

                                <tbody>
                                @foreach($bankomat as $bankomats)
                                    <tr>
                                        <td>

                                            <div class="round-img">
                                                <a href="/admin/bankomat/{{ $bankomats->id }}/edit"><img
                                                            src="/assets/img/bankomat/thumbnails/{{ $bankomats->image }}"
                                                            alt=""></a>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="/admin/bankomat/{{ $bankomats->id }}/edit">{{ $bankomats->title }}</a>
                                        </td>
                                        <td>{{ $bankomats->created_at }}</td>

                                    </tr>
                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 right">
                {!! $bankomat->links() !!}
            </div>

        </div>
    </div>
@endsection
