@extends('layouts.app')
@section('content')
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Подесувања</span>
                    </div>
                    <div class="widget-body">
                        <div id="horizontal-form">

                            {{--{{ Form::model('settings', array('route' => array('admin.settings.update'), 'method' => 'PUT','role' => 'form','files' => true)) }}--}}
                            {!! Form::model('kurs', ['action' => ['KursController@update', $kurs->id], 'method' => 'PUT']) !!}

                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="title">Долар откупна</label>
                                    <input type="text" name="usa" class="form-control" value="{{ $kurs->usa }}">
                                </div>
                                @if ($errors->has('usa')) <p
                                        class="alert alert-danger">{{ $errors->first('usa') }}</p> @endif


                                <div class="form-group col-md-6">
                                    <label>Долар продажна</label>
                                    <input name="usa1" type="text" class="form-control" value="{{ $kurs->usa1 }}">
                                </div>
                                @if ($errors->has('usa1')) <p
                                        class="alert alert-danger">{{ $errors->first('usa1') }}</p> @endif


                                <div class="form-group col-md-6">
                                    <label>Евро откупна</label>
                                    <input name="euro" type="text" class="form-control" value="{{ $kurs->euro }}">
                                </div>
                                @if ($errors->has('euro')) <p
                                        class="alert alert-danger">{{ $errors->first('euro') }}</p> @endif

                                <div class="form-group col-md-6">
                                    <label>Евро продажна</label>
                                    <input name="euro1" type="text" class="form-control" value="{{ $kurs->euro1 }}">
                                </div>
                                @if ($errors->has('euro1')) <p
                                        class="alert alert-danger">{{ $errors->first('euro1') }}</p> @endif

                                <div class="form-group col-md-6">
                                    <label>Фунта откупна</label>
                                    <input name="funta" type="text" class="form-control" value="{{ $kurs->funta }}">
                                </div>
                                @if ($errors->has('funta')) <p
                                        class="alert alert-danger">{{ $errors->first('funta') }}</p> @endif

                                <div class="form-group col-md-6">
                                    <label>Фунта продажна</label>
                                    <input name="funta1" type="text" class="form-control" value="{{ $kurs->funta1 }}">
                                </div>
                                @if ($errors->has('funta1')) <p
                                        class="alert alert-danger">{{ $errors->first('twitter') }}</p> @endif

                                <div class="form-group col-md-6">
                                    <label>Франк откупна</label>
                                    <input name="frank" type="text" class="form-control" value="{{ $kurs->frank }}">
                                </div>
                                @if ($errors->has('frank')) <p
                                        class="alert alert-danger">{{ $errors->first('frank') }}</p> @endif

                                <div class="form-group col-md-6">
                                    <label>Франк продажна </label>
                                    <input name="frank1" type="text" class="form-control" value="{{ $kurs->frank1 }}">
                                </div>
                                @if ($errors->has('frank1')) <p
                                        class="alert alert-danger">{{ $errors->first('frank1') }}</p> @endif
                            </div>

                            <div class="form-group">
                                <p>Workflow: </p>
                                @foreach($workflows as $workflow)
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="workflow_id"
                                                   value="{{ $workflow->id }}" @if($workflow->id  == 1) checked @endif>
                                        </label>
                                        <span class="text"> {{ $workflow->name }}</span>
                                    </div>

                                @endforeach
                            </div>


                            <!-- Hidden inputs -->


                            <input type="hidden" id="id" class="form-control" name="id" value="{{ $kurs->id }}">

                            <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                                        class="btn-label fa fa-plus"></i> Обнови
                            </button>
                            {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
