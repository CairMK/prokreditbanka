@extends('layouts.app')
@section('content')
    <div class="col-lg-10">
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif
                @foreach($kurs as $kurss)
                    <p><a class="btn btn-labeled " href="/admin/kurs/{{ $kurss->id }}/edit"> <i
                                    class="btn-label fa fa-plus"></i>Промени </a></p>
                @endforeach
            </div>
        </div>
        <div class="row"> <!-- /# card1 -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Долар откупна</p>
                    </div>
                    <hr/>
                    <div class="media">
                        @foreach($kurs as $kurss)
                            @if ($kurss->usa != NULL)
                                {{ $kurss->usa }}
                            @else
                                n/a
                            @endif
                        @endforeach

                    </div>

                </div>

            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Долар продажна</p>
                    </div>
                    <hr/>
                    <div class="media">
                        @foreach($kurs as $kurss)
                            @if ($kurss->usa1 != NULL)
                                {{ $kurss->usa1 }}
                            @else
                                n/a
                            @endif
                        @endforeach

                    </div>

                </div>

            </div>
        </div> <!-- /# card1 -->

        <div class="row"> <!-- /# card2 -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Евро откупна</p>
                    </div>
                    <hr/>
                    <div class="media">

                        @foreach($kurs as $kurss)
                            @if ($kurss->euro != NULL)
                                {{ $kurss->euro }}
                            @else
                                n/a
                            @endif
                        @endforeach

                    </div>

                </div>
                <!-- /# card -->
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Евро продажна</p>
                    </div>
                    <hr/>
                    <div class="media">
                        @foreach($kurs as $kurss)
                            @if ($kurss->euro1 != NULL)
                                {{ $kurss->euro1 }}
                            @else
                                n/a
                            @endif
                        @endforeach

                    </div>

                </div>
            </div>
        </div> <!-- /# card2 -->
        <div class="row"> <!-- /# card3 -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Фунта откупна</p>
                    </div>
                    <hr/>
                    <div class="media">

                        @foreach($kurs as $kurss)
                            @if ($kurss->frank != NULL)
                                {{ $kurss->frank }}
                            @else
                                n/a
                            @endif
                        @endforeach
                    </div>

                </div>

            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Фунта продажна</p>
                    </div>
                    <hr/>
                    <div class="media">
                        @foreach($kurs as $kurss)
                            @if ($kurss->frank1 != NULL)
                                {{ $kurss->frank1 }}
                            @else
                                n/a
                            @endif
                        @endforeach
                    </div>

                </div>

            </div>
        </div> <!-- /# card3 -->
        <div class="row"> <!-- /# card4 -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Франк откупна</p>
                    </div>
                    <hr/>
                    <div class="media">
                        @foreach($kurs as $kurss)
                            @if ($kurss->funta != NULL)
                                {{ $kurss->funta }}
                            @else
                                n/a
                            @endif
                        @endforeach
                    </div>

                </div>

            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Франк продажна </p>
                    </div>
                    <hr/>
                    <div class="media">
                        @foreach($kurs as $kurss)
                            @if ($kurss->funta1 != NULL)
                                {{ $kurss->funta1 }}
                            @else
                                n/a
                            @endif
                        @endforeach
                    </div>

                </div>

            </div>
        </div> <!-- /# card4 -->


    </div>

@endsection


