@extends('layouts.app')
@section('content')

    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Edit static page: {{ strip_tags($news->title) }}</span>
                    </div>
                    <div class="widget-body">

                        <div class="img-blog">
                            @if(!!$news->image)
                                <img class="img-responsive"
                                     src="/assets/img/news/medium/{{ $news->imagemedium }}"
                                     alt="{{ $news->title }}"/>
                            @endif
                        </div>
                        <br/>


                        <div id="horizontal-form">

                            {{ Form::model('news', array('route' => array('admin.news.update', $news->id), 'method' => 'PUT','files' => true)) }}
                            {!! csrf_field() !!}

                            <div class="input-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <span class="input-group-btn">
                                <span>
                                    Browse... <input type="file" name="image">
                                </span>
                            </span>
                            </div>
                            <br/>
                            @if ($errors->has('image')) <p
                                    class="alert alert-danger">{{ $errors->first('image') }}</p> @endif

                            <div class="alform-group row">
                                <label for="title" class="col-2 col-form-label">Title</label>
                                <div class="col-12">
                                    <input class="form-control" type="text" value="{{ $news->title }}"
                                           name="title"
                                           id="title">
                                </div>
                            </div>
                            @if ($errors->has('title')) <p
                                    class="alert alert-danger">{{ $errors->first('title') }}</p> @endif


                            <div class="form-group">
                                <label for="description">Product description</label>
                                <textarea class="ckeditor" id="elm3"
                                          name="description">{{ $news->description }}</textarea>
                            </div>
                            @if ($errors->has('description')) <p
                                    class="alert alert-danger">{{ $errors->first('description') }}</p> @endif

                            <div class="form-group">
                                <label for="lang">Izberete jazik: </label>
                                <select name="lang" id="lang" class="form-control">
                                    <option value="mk">Makedonski</option>
                                    <option value="en">Angliski</option>
                                    <option value="sq">Albanski</option>
                                </select>
                            </div>
                            @if ($errors->has('lang')) <p
                                    class="alert alert-danger">{{ $errors->first('lang') }}</p> @endif
                            <br>
                            <div class="form-group">
                                <label for="user">Creator</label>
                                <select name="user_id" id="user" class="form-control">
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}"
                                                @if($news->user_id == $user->id) selected @endif >{{ $user->name }}</option>
                                    @endforeach

                                </select>
                            </div>


                            <div class="form-group">
                                <p>Workflow: </p>
                                @foreach($workflows as $workflow)
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="workflow_id"
                                                   value="{{ $workflow->id }}" @if($workflow->id  == 1) checked @endif>
                                        </label>
                                        <span class="text"> {{ $workflow->name }}</span>
                                    </div>

                                @endforeach
                            </div>

                            <button type="submit" class="btn btn-labeled shiny btn-info btn-large"><i
                                        class="btn-label fa fa-plus"></i> Update
                            </button>
                            {!! Form::close() !!}

                            <hr/>


                            {{ Form::model('news', array('route' => array('admin.news.destroy', $news->id), 'method' => 'DELETE', 'id' => 'news'))}}
                            {!! csrf_field() !!}

                            <button type="submit" class="btn btn-labeled shiny btn-danger delete"><i
                                        class="btn-label fa fa-trash"></i> Delete
                            </button>
                            {{ Form::close() }}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
