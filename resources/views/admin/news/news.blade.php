@extends('layouts.app')

@section('content')
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-12">
                <p><a class="btn btn-labeled w" href="/admin/news/create"> <i
                                class="btn-label fa fa-plus"></i>Add Static Page </a></p>
                <div class="card">
                    <div class="card-title">
                        <h4>Статични страни</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                @foreach($news as $newss)
                                    <tr>
                                        <td>
                                            <div class="round-img">
                                                <a href="/admin/news/{{ $newss->id }}/edit"> @if($newss->image)
                                                        <img src="/assets/img/news/{{ $newss->imagethumb }}"
                                                             alt="">
                                                    @endif</a>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="/admin/news/{{ $newss->id }}/edit">{{ $newss->title }}</a>
                                        </td>
                                        <td><span>Original time: {{ $newss->created_at }}</span></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            {!! $news->links() !!}
        </div>
    </div>



@endsection




