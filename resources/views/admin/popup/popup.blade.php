@extends('layouts.app')

@section('content')
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <p><a class="btn btn-labeled" href="/admin/popup/create"> <i class="btn-label fa fa-plus"></i>Add PopUp
                    </a></p>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>PopUp</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                @foreach($popup as $popup)
                                    <tr>
                                        <td>
                                            <div class="round-img">
                                                <a href="/admin/popup/{{ $popup->id }}/edit"><img
                                                            src="/assets/img/popup/thumbnails/{{ $popup->image }}"
                                                            alt=""></a>
                                            </div>
                                        </td>
                                        <td>{{ $popup->title }}</td>
                                        <td><span><a href="/admin/sliders/{{ $popup->id }}/popup" class="btn btn-info">Додади галерија</a></span>
                                        </td>
                                        <td><span>Слики во слајдер: {{ $popup->images()->count() }}</span></td>
                                        {{--                 <td><span>Workflow: {{ $popup->workflow->name }}</span></td>
                                        --}}
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 right">
            </div>

        </div>
    </div>
@endsection
