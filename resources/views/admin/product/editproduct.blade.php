@extends('layouts.app')
@section('content')

    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Edit product: {{ strip_tags($product->title) }}</span>
                    </div>
                    <div class="widget-body">

                        <div class="img-blog">
                            @if(!!$product->image)
                                <img class="img-responsive"
                                     src="/assets/img/products/medium/{{ $product->imagemedium }}"
                                     alt="{{ $product->title }}"/>
                            @endif
                        </div>
                        <br/>


                        <div id="horizontal-form">

                            {{ Form::model('product', array('route' => array('admin.product.update', $product->id), 'method' => 'PUT','files' => true)) }}
                            {!! csrf_field() !!}


                            <div class="input-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <span class="input-group-btn">
                                <span>
                                    Browse... <input type="file" name="image">
                                </span>
                            </span>
                            </div>
                            <br/>
                            @if ($errors->has('image')) <p
                                    class="alert alert-danger">{{ $errors->first('image') }}</p> @endif

                            <div class="form-group row">
                                <label for="title" class="col-2 col-form-label">Title</label>
                                <div class="col-12">
                                    <input class="form-control" type="text" value="{{ $product->title }}" name="title"
                                           id="title">
                                </div>
                            </div>
                            @if ($errors->has('title')) <p
                                    class="alert alert-danger">{{ $errors->first('title') }}</p> @endif


                            <div class="form-group">
                                <label for="description">Product description</label>
                                <textarea class="ckeditor" id="elm3"
                                          name="description">{{ $product->description }}</textarea>
                            </div>
                            @if ($errors->has('description')) <p
                                    class="alert alert-danger">{{ $errors->first('description') }}</p> @endif


                            <div id="accordion" role="tablist">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h5 class="mb-0">
                                            <a data-toggle="collapse" href="#collapse1" aria-expanded="true"
                                               aria-controls="collapseOne">
                                                Наслов и опис на таб 1
                                            </a>
                                        </h5>
                                    </div>
                                </div>
                                <div id="collapse1" class="collapse show" role="tabpanel"
                                     aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <label for="title" class="col-2 col-form-label">Title</label>
                                        <div class="col-12">
                                            <input class="form-control" type="text" value="{{ $product->title1 }}"
                                                   name="title1" id="title1">
                                        </div>
                                        @if ($errors->has('title1')) <p
                                                class="alert alert-danger">{{ $errors->first('title1') }}</p> @endif
                                        <br>
                                        <div class="form-group">
                                            <label for="description1">Product description</label>
                                            <textarea class="ckeditor" id="elm3"
                                                      name="description1">{{ $product->description1 }}</textarea>
                                        </div>

                                        @if ($errors->has('description1')) <p
                                                class="alert alert-danger">{{ $errors->first('description1') }}</p> @endif
                                    </div>
                                </div>
                                <label for="lang">Izberete jazik: </label>
                                <select name="lang" id="lang" class="form-control">
                                    <option value="mk">Makedonski</option>
                                    <option value="en">Angliski</option>
                                    <option value="de">Albanski</option>
                                </select>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingTwo">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse2"
                                               aria-expanded="false" aria-controls="collapse2">
                                                Наслов и опис на таб 2
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse2" class="collapse" role="tabpanel"
                                         aria-labelledby="headingTwo"
                                         data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text" value="{{ $product->title2 }}"
                                                       name="title2" id="title2">
                                            </div>
                                            @if ($errors->has('title1')) <p
                                                    class="alert alert-danger">{{ $errors->first('title2') }}</p> @endif
                                            <br>
                                            <div class="form-group">
                                                <label for="description2">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description2">{{ $product->description2 }}</textarea>
                                            </div>
                                            @if ($errors->has('description2')) <p
                                                    class="alert alert-danger">{{ $errors->first('description2') }}</p> @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse3"
                                               aria-expanded="false" aria-controls="collapseThree">
                                                Наслов и опис на таб 3
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse3" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text" value="{{ $product->title3 }}"
                                                       name="title3" id="title3">
                                            </div>
                                            @if ($errors->has('title3')) <p
                                                    class="alert alert-danger">{{ $errors->first('title3') }}</p> @endif
                                            <br>
                                            <div class="form-group">
                                                <label for="description3">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description3">{{ $product->description3 }}</textarea>
                                            </div>
                                            @if ($errors->has('description3')) <p
                                                    class="alert alert-danger">{{ $errors->first('description3') }}</p> @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse4"
                                               aria-expanded="false" aria-controls="collapse4">
                                                Наслов и опис на таб 4
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse4" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text" value="{{ $product->title4 }}"
                                                       name="title4" id="title4">
                                            </div>
                                            @if ($errors->has('title4')) <p
                                                    class="alert alert-danger">{{ $errors->first('title4') }}</p> @endif
                                            <br>
                                            <div class="form-group">
                                                <label for="description4">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description4">{{ $product->description4 }}</textarea>
                                            </div>
                                            @if ($errors->has('description4')) <p
                                                    class="alert alert-danger">{{ $errors->first('description4') }}</p> @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse5"
                                               aria-expanded="false" aria-controls="collapseThree">
                                                Наслов и опис на таб 5
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse5" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text" value="{{ $product->title5 }}"
                                                       name="title5" id="title5">
                                            </div>
                                            @if ($errors->has('title5')) <p
                                                    class="alert alert-danger">{{ $errors->first('title5') }}</p> @endif
                                            <br>
                                            <div class="form-group">
                                                <label for="description5">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description5">{{ $product->description5 }}</textarea>
                                            </div>
                                            @if ($errors->has('description5')) <p
                                                    class="alert alert-danger">{{ $errors->first('description5') }}</p> @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse6"
                                               aria-expanded="false" aria-controls="collapseThree">
                                                Наслов и опис на таб 6
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse6" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text" value="{{ $product->title6 }}"
                                                       name="title6" id="title6">
                                            </div>
                                            @if ($errors->has('title6')) <p
                                                    class="alert alert-danger">{{ $errors->first('title6') }}</p> @endif
                                            <br>
                                            <div class="form-group">
                                                <label for="description6">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description6">{{ $product->description6 }}</textarea>
                                            </div>
                                            @if ($errors->has('description6')) <p
                                                    class="alert alert-danger">{{ $errors->first('description6') }}</p> @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="lang">Izberete jazik: </label>
                                <select name="lang" id="lang" class="form-control">
                                    <option value="mk">Makedonski</option>
                                    <option value="en">Angliski</option>
                                    <option value="sq">Albanski</option>
                                </select>
                            </div>
                            @if ($errors->has('lang')) <p
                                    class="alert alert-danger">{{ $errors->first('lang') }}</p> @endif
                            <br>
                            <div class="form-group">
                                <label for="user">Creator</label>
                                <select name="user_id" id="user" class="form-control">
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}"
                                                @if($product->user_id == $user->id) selected @endif >{{ $user->name }}</option>
                                    @endforeach

                                </select>
                            </div>

                            <div class="form-group">
                                <p>Featured: </p>
                                @foreach($featureds as $featured)
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="featured_id"
                                                   value="{{ $featured->id }}" @if($featured->id  == 1) checked @endif>
                                        </label>
                                        <span class="text"> {{ $featured->name }}</span>
                                    </div>

                                @endforeach
                            </div>
                            <div class="form-group">
                                <p>Workflow: </p>
                                @foreach($workflows as $workflow)
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="workflow_id"
                                                   value="{{ $workflow->id }}" @if($workflow->id  == 1) checked @endif>
                                        </label>
                                        <span class="text"> {{ $workflow->name }}</span>
                                    </div>

                                @endforeach
                            </div>

                            <button type="submit" class="btn btn-labeled shiny btn-info btn-large"><i
                                        class="btn-label fa fa-plus"></i> Update
                            </button>
                            {!! Form::close() !!}

                            <hr/>

                            {{ Form::model('product', array('route' => array('admin.product.destroy', $product->id), 'method' => 'DELETE', 'id' => 'deleteproduct'))}}
                            {!! csrf_field() !!}

                            <button type="submit" class="btn btn-labeled shiny btn-danger delete"><i
                                        class="btn-label fa fa-trash"></i> Delete
                            </button>
                            {{ Form::close() }}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
