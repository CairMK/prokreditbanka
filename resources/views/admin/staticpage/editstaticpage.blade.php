@extends('layouts.app')
@section('content')

    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Edit static page: {{ strip_tags($staticpage->title) }}</span>
                    </div>
                    <div class="widget-body">

                        <div class="img-blog">
                            @if(!!$staticpage->image)
                                <img class="img-responsive"
                                     src="/assets/img/staticpage/medium/{{ $staticpage->imagemedium }}"
                                     alt="{{ $staticpage->title }}"/>
                            @endif
                        </div>
                        <br/>


                        <div id="horizontal-form">

                            {{ Form::model('staticpage', array('route' => array('admin.staticpage.update', $staticpage->id), 'method' => 'PUT','files' => true)) }}
                            {!! csrf_field() !!}

                            <div class="input-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <span class="input-group-btn">
                                <span>
                                    Browse... <input type="file" name="image">
                                </span>
                            </span>
                            </div>
                            <br/>
                            @if ($errors->has('image')) <p
                                    class="alert alert-danger">{{ $errors->first('image') }}</p> @endif

                            <div class="alform-group row">
                                <label for="title" class="col-2 col-form-label">Title</label>
                                <div class="col-12">
                                    <input class="form-control" type="text" value="{{ $staticpage->title }}"
                                           name="title"
                                           id="title">
                                </div>
                            </div>
                            @if ($errors->has('title')) <p
                                    class="alert alert-danger">{{ $errors->first('title') }}</p> @endif


                            <div class="form-group">
                                <label for="description">Product description</label>
                                <textarea class="ckeditor" id="elm3"
                                          name="description">{{ $staticpage->description }}</textarea>
                            </div>
                            @if ($errors->has('description')) <p
                                    class="alert alert-danger">{{ $errors->first('description') }}</p> @endif


                            <div id="accordion" role="tablist">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h5 class="mb-0">
                                            <a data-toggle="collapse" href="#collapse1" aria-expanded="true"
                                               aria-controls="collapseOne">
                                                Наслов и опис на таб 1
                                            </a>
                                        </h5>
                                    </div>
                                </div>
                                <div id="collapse1" class="collapse show" role="tabpanel"
                                     aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <label for="title" class="col-2 col-form-label">Title</label>
                                        <div class="col-12">
                                            <input class="form-control" type="text" value="{{ $staticpage->title1 }}"
                                                   name="title1" id="title1">
                                        </div>
                                        @if ($errors->has('title1')) <p
                                                class="alert alert-danger">{{ $errors->first('title1') }}</p> @endif
                                        <br>
                                        <div class="form-group">
                                            <label for="description1">Product description</label>
                                            <textarea class="ckeditor" id="elm3"
                                                      name="description1">{{ $staticpage->description1 }}</textarea>
                                        </div>

                                        @if ($errors->has('description1')) <p
                                                class="alert alert-danger">{{ $errors->first('description1') }}</p> @endif
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingTwo">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse2"
                                               aria-expanded="false" aria-controls="collapse2">
                                                Наслов и опис на таб 2
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse2" class="collapse" role="tabpanel"
                                         aria-labelledby="headingTwo"
                                         data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text"
                                                       value="{{ $staticpage->title2 }}"
                                                       name="title2" id="title2">
                                            </div>
                                            @if ($errors->has('title1')) <p
                                                    class="alert alert-danger">{{ $errors->first('title2') }}</p> @endif
                                            <br>
                                            <div class="form-group">
                                                <label for="description2">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description2">{{ $staticpage->description2 }}</textarea>
                                            </div>
                                            @if ($errors->has('description2')) <p
                                                    class="alert alert-danger">{{ $errors->first('description2') }}</p> @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse3"
                                               aria-expanded="false" aria-controls="collapseThree">
                                                Наслов и опис на таб 3
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse3" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text"
                                                       value="{{ $staticpage->title3 }}"
                                                       name="title3" id="title3">
                                            </div>
                                            @if ($errors->has('title3')) <p
                                                    class="alert alert-danger">{{ $errors->first('title3') }}</p> @endif
                                            <br>
                                            <div class="form-group">
                                                <label for="description3">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description3">{{ $staticpage->description3 }}</textarea>
                                            </div>
                                            @if ($errors->has('description3')) <p
                                                    class="alert alert-danger">{{ $errors->first('description3') }}</p> @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse4"
                                               aria-expanded="false" aria-controls="collapse4">
                                                Наслов и опис на таб 4
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse4" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text"
                                                       value="{{ $staticpage->title4 }}"
                                                       name="title4" id="title4">
                                            </div>
                                            @if ($errors->has('title4')) <p
                                                    class="alert alert-danger">{{ $errors->first('title4') }}</p> @endif
                                            <br>
                                            <div class="form-group">
                                                <label for="description4">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description4">{{ $staticpage->description4 }}</textarea>
                                            </div>
                                            @if ($errors->has('description4')) <p
                                                    class="alert alert-danger">{{ $errors->first('description4') }}</p> @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse5"
                                               aria-expanded="false" aria-controls="collapseThree">
                                                Наслов и опис на таб 5
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse5" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text"
                                                       value="{{ $staticpage->title5 }}"
                                                       name="title5" id="title5">
                                            </div>
                                            @if ($errors->has('title5')) <p
                                                    class="alert alert-danger">{{ $errors->first('title5') }}</p> @endif
                                            <br>
                                            <div class="form-group">
                                                <label for="description5">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description5">{{ $staticpage->description5 }}</textarea>
                                            </div>
                                            @if ($errors->has('description5')) <p
                                                    class="alert alert-danger">{{ $errors->first('description5') }}</p> @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse6"
                                               aria-expanded="false" aria-controls="collapseThree">
                                                Наслов и опис на таб 6
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse6" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text"
                                                       value="{{ $staticpage->title6 }}"
                                                       name="title6" id="title6">
                                            </div>
                                            @if ($errors->has('title6')) <p
                                                    class="alert alert-danger">{{ $errors->first('title6') }}</p> @endif
                                            <br>
                                            <div class="form-group">
                                                <label for="description6">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description6">{{ $staticpage->description6 }}</textarea>
                                            </div>
                                            @if ($errors->has('description6')) <p
                                                    class="alert alert-danger">{{ $errors->first('description6') }}</p> @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse7"
                                               aria-expanded="false" aria-controls="collapseThree">
                                                Наслов и опис на таб 7
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse7" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text"
                                                       value="{{ $staticpage->title7 }}"
                                                       name="title7" id="title7">
                                            </div>
                                            @if ($errors->has('title7')) <p
                                                    class="alert alert-danger">{{ $errors->first('title7') }}</p> @endif
                                            <br>
                                            <div class="form-group">
                                                <label for="description6">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description7">{{ $staticpage->description7 }}</textarea>
                                            </div>
                                            @if ($errors->has('description7')) <p
                                                    class="alert alert-danger">{{ $errors->first('description7') }}</p> @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse8"
                                               aria-expanded="false" aria-controls="collapseThree">
                                                Наслов и опис на таб 8
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse8" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text"
                                                       value="{{ $staticpage->title8 }}"
                                                       name="title8" id="title8">
                                            </div>
                                            @if ($errors->has('title8')) <p
                                                    class="alert alert-danger">{{ $errors->first('title8') }}</p> @endif
                                            <br>
                                            <div class="form-group">
                                                <label for="description6">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description8">{{ $staticpage->description8 }}</textarea>
                                            </div>
                                            @if ($errors->has('description8')) <p
                                                    class="alert alert-danger">{{ $errors->first('description8') }}</p> @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse9"
                                               aria-expanded="false" aria-controls="collapseThree">
                                                Наслов и опис на таб 9
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse9" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text"
                                                       value="{{ $staticpage->title9 }}"
                                                       name="title9" id="title9">
                                            </div>
                                            @if ($errors->has('title9')) <p
                                                    class="alert alert-danger">{{ $errors->first('title9') }}</p> @endif
                                            <br>
                                            <div class="form-group">
                                                <label for="description6">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description9">{{ $staticpage->description9 }}</textarea>
                                            </div>
                                            @if ($errors->has('description9')) <p
                                                    class="alert alert-danger">{{ $errors->first('description9') }}</p> @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="lang">Izberete jazik: </label>
                                <select name="lang" id="lang" class="form-control">
                                    <option value="mk">Makedonski</option>
                                    <option value="en">Angliski</option>
                                    <option value="sq">Albanski</option>
                                </select>
                            </div>
                            @if ($errors->has('lang')) <p
                                    class="alert alert-danger">{{ $errors->first('lang') }}</p> @endif
                            <br>
                            <div class="form-group">
                                <label for="user">Creator</label>
                                <select name="user_id" id="user" class="form-control">
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}"
                                                @if($staticpage->user_id == $user->id) selected @endif >{{ $user->name }}</option>
                                    @endforeach

                                </select>
                            </div>


                            <div class="form-group">
                                <p>Workflow: </p>
                                @foreach($workflows as $workflow)
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="workflow_id"
                                                   value="{{ $workflow->id }}" @if($workflow->id  == 1) checked @endif>
                                        </label>
                                        <span class="text"> {{ $workflow->name }}</span>
                                    </div>

                                @endforeach
                            </div>

                            <button type="submit" class="btn btn-labeled shiny btn-info btn-large"><i
                                        class="btn-label fa fa-plus"></i> Update
                            </button>
                            {!! Form::close() !!}

                            <hr/>


                            {{ Form::model('staticpage', array('route' => array('admin.staticpage.destroy', $staticpage->id), 'method' => 'DELETE', 'id' => 'staticpage'))}}
                            {!! csrf_field() !!}

                            <button type="submit" class="btn btn-labeled shiny btn-danger delete"><i
                                        class="btn-label fa fa-trash"></i> Delete
                            </button>
                            {{ Form::close() }}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
