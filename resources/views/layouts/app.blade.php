<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8"/>
    <title>CMS</title>
    <meta name="description" content="Dashboard"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" href="/assets/img/favicon.png" type="image/x-icon">
    <!-- Bootstrap Core CSS -->
    <link href="/assets/admin/css/app.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/assets/admin/css/helper.css" rel="stylesheet">
    <link href="/assets/admin/css/style.css" rel="stylesheet">
    <link href="/assets/admin/css/custom.css" rel="stylesheet">
</head>
<body>

@include('partials.nav')

@yield('content')



{{-- Scripts --}}
{{--<script src="{{ mix('/js/app.js') }}"></script>--}}

{{--@if(config('settings.googleMapsAPIStatus'))--}}
{{--{!! HTML::script('//maps.googleapis.com/maps/api/js?key='.config("settings.googleMapsAPIKey").'&libraries=places&dummy=.js', array('type' => 'text/javascript')) !!}--}}
{{--@endif--}}

<!-- JavaScript files placed at the end of the document so the pages load faster -->
<!-- ================================================== -->
<!-- Jquery and Bootstap core js files -->
<!-- All Jquery -->
<script src="/assets/admin/js/lib/jquery/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="/assets/admin/js/app.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="/assets/admin/js/jquery.slimscroll.js"></script>
<!--Menu sidebar -->
<script src="/assets/admin/js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="/assets/admin/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
<!--Custom JavaScript -->
<script src="/assets/admin/js/custom.min.js"></script>
<!--ck editor -->
<script type="text/javascript" src="/assets/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/assets/js/ckconf.js"></script>
<script>(function () {
        'use strict'

        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
            var msViewportStyle = document.createElement('style')
            msViewportStyle.appendChild(
                document.createTextNode(
                    '@-ms-viewport{width:auto!important}'
                )
            )
            document.head.appendChild(msViewportStyle)
        }

    }())</script>

@yield('scripts')

</body>
</html>
