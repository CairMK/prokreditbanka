<!DOCTYPE html>
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<meta charset="utf-8">
<title>{{$settings->title}}</title>
<meta name="description" content="{!! str_limit(strip_tags($settings->description), 150, '...')!!}">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{ str_limit(strip_tags($settings->description),170, $end = '...' ) }}">
    <meta name="author" content="">
    <meta name="keywords"
          content="ПроКредит,мали,средни,претпријатија,банка,кредит,кредити,сметка,сметки,денарска сметка,еко, девизна сметка,закажи средба,контакт инфо, инфо, картички,физички лица, Master,Maestro,Visa,штедење,денарско штедење,девизно штедење,платен промет,гаранции, станбен кредит,готовински кредит, курсна,потрошувачки кредит,кредит,banka,kredit,krediti,smetka,smetki,denarska smetka,devizna smetka, prokredit,karticki,stedenje,denarsko stedenje,devizno stedenje,platen promet,garancii,gotovinski kredit, stamben kredit, appointment,sredba,info, News,Apply Online, Price List, mali, sredni, pretprijatija"/>
    <!-- Bootstrap core CSS -->
    <link href="/assets/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<div class="page-wrapper">

    @yield('menu')

    @yield('slider')

    <div id="page-start"></div>

    @yield('content')

<!-- FOOTER -->
    <div class="row footer-wrapper px-0 mx-0">
        <div class="container">
            <footer>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12 order-lg-1 order-md-2 order-2 news-footer">
                        <div class="news-label">
                            <a href="/news/all"><h5>{{trans('menu.news')}}</h5></a>
                        </div>
                        @foreach($newshome as $newshomes)
                            <p>
                                <a href="/news/all">{!! str_limit(strip_tags($newshomes->title),55, $end = '...' ) !!}</a>
                            </p>
                        @endforeach
                    </div>

                    <div class="col-lg-3 col-md-6 col-12 order-lg-2 order-md-2 order-2 ">
                        <ul class="list-unstyled p-2">
                            <li class="mb-1  footer-icon-1"><a href="/network"><h4
                                            class="frontpage">{{trans('menu.NETWORK')}}</h4></a>
                            </li>
                            <li class="mb-1 footer-icon-2"><a
                                        href="http://pcb.com.mk/content/pdf/cenovnik-fizicki-lica01.02.pdf.pdf"
                                        target="_blank"><h4
                                            class="frontpage">{{trans('menu.price_list_private')}}</h4>
                                </a>
                            </li>
                            <li class="mb-1" footer-icon-3><a
                                        href="http://pcb.com.mk/content/pdf/cenovnik-pravni-lica.pdf"
                                        target="_blank">
                                    <h4 class="frontpage">{{trans('menu.price_list_busines')}}</h4>
                                </a></li>
                            <li class="mb-1" footer-icon-4><a
                                        href="/calculator1">
                                    <h4 class="frontpage">{{trans('menu.calculator_invest')}}</h4>
                                </a></li>
                            <li class="mb-1" footer-icon-5><a
                                        href="/calculator">
                                    <h4 class="frontpage">{{trans('menu.calculator_deposit')}}</h4>
                                </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12 order-lg-2 order-md-2 order-2 ">
                        <ul class="list-unstyled p-2">
                            <li class="d-flex align-items-center"><i class="swift-icon"></i>
                                <h3 class="frontpage">SWIFT:PRBUMK22XXX</h3></li>
                            <li class="d-flex align-items-center"><i class="envelope-icon"></i>
                                <h3 class="frontpage">info@procreditbank.com.mk</h3>
                            </li>
                            <li class="d-flex align-items-center"><i class="location-icon"></i>
                                <h3 class="frontpage">Manapo br.7 1000 Skopje</h3>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-6 order-lg-4 order-md-4 order-4 ">
                        <ul class="list-unstyled p-2">
                            <!-- Button to Open the Modal -->
                            <li class="mb-1" data-toggle="modal" data-target="#myModal">
                                {{trans('menu.exchange_rate')}}
                            </li>

                            <!-- The Modal -->
                            <div class="modal" id="myModal">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title"> {{trans('menu.exchange_rate')}}</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Валута</th>
                                                    <th>Откупен</th>
                                                    <th>Продажен</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>EUR</td>
                                                    <td> @foreach($kurs as $kurss)
                                                            @if ($kurss->euro != NULL)
                                                                {{ $kurss->euro }}
                                                            @else
                                                                n/a
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    <td> @foreach($kurs as $kurss)
                                                            @if ($kurss->euro1 != NULL)
                                                                {{ $kurss->euro1 }}
                                                            @else
                                                                n/a
                                                            @endif
                                                        @endforeach</td>
                                                </tr>
                                                <tr>
                                                    <td>USD</td>
                                                    <td> @foreach($kurs as $kurss)
                                                            @if ($kurss->usa != NULL)
                                                                {{ $kurss->usa }}
                                                            @else
                                                                n/a
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    <td> @foreach($kurs as $kurss)
                                                            @if ($kurss->usa1 != NULL)
                                                                {{ $kurss->usa1 }}
                                                            @else
                                                                n/a
                                                            @endif
                                                        @endforeach</td>
                                                </tr>
                                                <tr>
                                                    <td>GBP</td>
                                                    <td> @foreach($kurs as $kurss)
                                                            @if ($kurss->funta != NULL)
                                                                {{ $kurss->funta }}
                                                            @else
                                                                n/a
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    <td> @foreach($kurs as $kurss)
                                                            @if ($kurss->funta1 != NULL)
                                                                {{ $kurss->funta1 }}
                                                            @else
                                                                n/a
                                                            @endif
                                                        @endforeach</td>
                                                </tr>
                                                <tr>
                                                    <td>CHF</td>
                                                    <td> @foreach($kurs as $kurss)
                                                            @if ($kurss->frank != NULL)
                                                                {{ $kurss->frank }}
                                                            @else
                                                                n/a
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    <td> @foreach($kurs as $kurss)
                                                            @if ($kurss->frank1 != NULL)
                                                                {{ $kurss->frank1 }}
                                                            @else
                                                                n/a
                                                            @endif
                                                        @endforeach</td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <li class="mb-1"><a href="/tenderi"><h5
                                            class="frontpage">{{trans('menu.tenders')}}</h5></a></li>
                            <li class="mb-1"><a href="/prodazba"><h5
                                            class="frontpage">{{trans('menu.sales')}}</h5></a></li>
                            <li class="mb-1"><a href="/terms-and-conditions"><h5
                                            class="frontpage">{{trans('menu.terms')}}</h5></a></li>
                        </ul>
                    </div>
                    <div class="col-lg-12  col-12 order-lg-5 order-md-5 order-5 row mx-0 px-0 border-top pt-2">
                        <div class="col-6">
                            <div class="social-media-footer d-flex align-items-center justify-content-start flex-wrap">
                                @if($settings->twitter)
                                    <a target="_blank" href="{{ $settings->twitter }}"><i
                                                class="fa fa-twitter"></i></a>
                                @endif
                                @if($settings->skype)
                                    <a target="_blank" href="{{ $settings->skype }}"><i
                                                class="fa fa-skype"></i></a>
                                @endif
                                @if($settings->linkedin)
                                    <a target="_blank" href="{{ $settings->linkedin }}"><i
                                                class="fa fa-linkedin"></i></a>
                                @endif
                                @if($settings->gplus)
                                    <a target="_blank" href="{{$settings->gplus}}"><i
                                                class="fa fa-google-plus"></i></a>
                                @endif
                                @if($settings->youtube)
                                    <a target="_blank" href="{{$settings->youtube}}"><i
                                                class="fa fa-youtube-play"></i></a>
                                @endif
                                @if($settings->flickr)
                                    <a target="_blank" href="{{ $settings->flickr }}"><i
                                                class="fa fa-flickr"></i></a>
                                @endif
                                @if($settings->facebook)
                                    <a target="_blank" href="{{ $settings->facebook }}"><i
                                                class="fa fa-facebook"></i></a>
                                @endif
                                @if($settings->pinterest)
                                    <a target="_blank" href="{{$settings->pinterest}}"><i
                                                class="fa fa-pinterest"></i></a>
                                @endif

                            </div>
                        </div>
                        <div class="col-6">
                            <h2 class="copyright mt-2 text-right">&copy; All Rights Reserved. Pro Credit Bank 2018
                            </h2>
                        </div>
                    </div>

                </div>

            </footer>
        </div>
    </div>
    <!-- FOOTER -->
    <!-- Bootstrap core JavaScript -->
    <script src="/assets/vendor/jquery/jquery.min.js"></script>
    <script defer="defer" src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    @yield('scripts')
    <script defer="defer" type="text/javascript" src="/assets/vendor/scrolloverflow.js"></script>
    <script defer="defer" type="text/javascript" src="/assets/js/jquery.fullPage.js"></script>
    <script defer="defer" src="/assets/js/main.js"></script>


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

    <script defer="defer" src="/assets/slick/slick.min.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">
        $(window).load(function () {
            $(".lazy").slick({

                infinite: true,
                dots: true,
                prevArrow: false,
                nextArrow: false,
                autoplay: true,
                autoplaySpeed: 2000

            });
        });
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-76143150-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-76143150-2');
    </script>


    </body>
</html>
