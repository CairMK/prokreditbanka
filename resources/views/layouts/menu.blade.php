@section('menu')
    {{--<!-- HEADER -->--}}
    {{--@if (Request::path() == "/index.php" || Request::path() == "/")--}}
    <div id="header">
        <div class="container-fluid px-0">
            <nav class="navbar navbar-expand-lg">
                <a class="navbar-brand" href="/">ProCredit</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                        aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="aboutUs-dropdown"
                               data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                {{trans('menu.aboutus')}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="aboutUs-dropdown">
                                <a class="dropdown-item" href="/abou-us#page-tab-1"
                                   data-expand="#page-tab-1">{{trans('menu.our_mission')}}</a>
                                <a class="dropdown-item" href="/abou-us#page-tab-2"
                                   data-expand="#page-tab-2">{{trans('menu.shareholders')}}</a>
                                <a class="dropdown-item" href="/abou-us#page-tab-3"
                                   data-expand="#page-tab-3">{{trans('menu.management')}}</a>
                                <a class="dropdown-item"
                                   href="/abou-us#page-tab-4"
                                   data-expand="#page-tab-4">{{trans('menu.group_of_responsible_banks')}}</a>
                                <a class="dropdown-item"
                                   href="/abou-us#page-tab-5"
                                   data-expand="#page-tab-5">{{trans('menu.business_ethics')}}</a>
                                <a class="dropdown-item"
                                   href="/the-banks-capital#page-tab-1">{{trans('menu.risk_Management')}}</a>
                                <a class="dropdown-item"
                                   href="/the-banks-capital#page-tab-2">{{trans('menu.Annual_report')}}</a>
                                <a class="dropdown-item"
                                   href="/the-banks-capital#page-tab-3">{{trans('menu.Corporate_information_according_to_NBRM')}}</a>
                                <a class="dropdown-item"
                                   href="/the-banks-capital#page-tab-4">{{trans('menu.the_bank’s_capital')}}</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="eco-dropdown" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                {{trans('menu.ECO')}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="eco-dropdown">
                                <a class="dropdown-item"
                                   href="/our-eco-strategy">{{trans('menu.OUR_ECO_STRATEGY')}}</a>
                                <a class="dropdown-item" href="/eco">{{trans('menu.eco_loans')}}</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                {{trans('menu.24/7_BANKING')}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="">
                                <a class="dropdown-item"
                                   href="/#">{{trans('menu.ZONA_24/7')}}</a>
                                <a class="dropdown-item"
                                   href="/#">{{trans('menu.SIGNING LOAN CONTRACTS ELECTRONICALLY')}}</a>
                                <a class="dropdown-item"
                                   href="/#">{{trans('menu.APPLY_ON_LINE')}}</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"
                               href="https://hr.procredit-group.com/mk?languagecode=en">{{trans('menu.OUR_STAFF')}}</a>
                        </li>
                        <div class="d-flex  ml-lg-auto mx-auto">
                            <li class="nav-item e-banking-button  m-1 w-100">
                                <a class="nav-link btn btn-primary"
                                   href="https://ebank.pcb.com.mk/User/LogOn">{{trans('menu.e-banking')}}</a>

                            </li>
                            <li class="nav-item e-banking-button m-1 w-100">
                                <a class="nav-link btn btn-primary"
                                   href="https://sso.pcb.com.mk/account/signin?">{{trans('menu.e-signing')}}</a>

                            </li>
                        </div>
                        <div class="language-switcher">
                            <ul class="list-unstyled d-flex" style="text-transform: uppercase;">

                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    <li>
                                        <a rel="alternate" hreflang="{{ $localeCode }}"
                                           href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                            {{ $localeCode }}
                                        </a>
                                    </li>
                                @endforeach

                            </ul>

                        </div>
                    </ul>
                </div>

            </nav>

        </div>
    </div>
    <!-- HEADER -->
    {{--@else--}}
    {{--<div id="header">--}}
    {{--<div class="container-fluid px-0">--}}
    {{--<nav class="navbar navbar-expand-lg">--}}
    {{--<a class="navbar-brand" href="/">ProCredit</a>--}}
    {{--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"--}}
    {{--aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">--}}
    {{--<span class="navbar-toggler-icon"></span>--}}
    {{--</button>--}}
    {{--<div class="collapse navbar-collapse" id="navbarNavDropdown">--}}
    {{--<ul class="navbar-nav">--}}
    {{--<li class="nav-item dynamic-element ">--}}
    {{--<a class="nav-link" target="_blank"--}}
    {{--href="https://www.procreditbank-direct.com/macedonia/mk">{{trans('menu.private_clients')}}</a>--}}
    {{--</li>                            --}}
    {{--<li class="nav-item dropdown">--}}
    {{--<a class="nav-link dropdown-toggle" href="#" id="aboutUs-dropdown"--}}
    {{--data-toggle="dropdown"--}}
    {{--aria-haspopup="true" aria-expanded="false">--}}
    {{--{{trans('menu.business_clients')}}--}}
    {{--</a>--}}
    {{--<div class="dropdown-menu" aria-labelledby="businessClient-dropdown1">--}}
    {{--<a class="dropdown-item"--}}
    {{--href="/accounts-and-payments">{{trans('menu.accounts')}}</a>--}}
    {{--<a class="dropdown-item" href="/credit-offers">{{trans('menu.credit_line')}}</a>--}}
    {{--<a class="dropdown-item"--}}
    {{--href="/documentary-operations">{{trans('menu.documentary_operations')}}</a>--}}
    {{--<a class="dropdown-item"--}}
    {{--href="/term-deposits">{{trans('menu.deposits_and_state_bonds')}}</a>--}}
    {{--<a class="dropdown-item"--}}
    {{--href="/services-for-non-resident-legal-entities">{{trans('menu.services_for_non_resident_legal_entities')}}</a>--}}
    {{--</div>--}}
    {{--</li>--}}
    {{--<li class="nav-item dropdown">--}}
    {{--<a class="nav-link dropdown-toggle" href="#" id="aboutUs-dropdown"--}}
    {{--data-toggle="dropdown"--}}
    {{--aria-haspopup="true" aria-expanded="false">--}}
    {{--{{trans('menu.aboutus')}}--}}
    {{--</a>--}}
    {{--<div class="dropdown-menu" aria-labelledby="aboutUs-dropdown">--}}
    {{--<a class="dropdown-item" href="/abou-us#page-tab-1"--}}
    {{--data-expand="#page-tab-1">{{trans('menu.our_mission')}}</a>--}}
    {{--<a class="dropdown-item" href="/abou-us#page-tab-2"--}}
    {{--data-expand="#page-tab-2">{{trans('menu.shareholders')}}</a>--}}
    {{--<a class="dropdown-item" href="/abou-us#page-tab-3"--}}
    {{--data-expand="#page-tab-3">{{trans('menu.management')}}</a>--}}
    {{--<a class="dropdown-item"--}}
    {{--href="/abou-us#page-tab-4"--}}
    {{--data-expand="#page-tab-4">{{trans('menu.group_of_responsible_banks')}}</a>--}}
    {{--<a class="dropdown-item"--}}
    {{--href="/abou-us#page-tab-5"--}}
    {{--data-expand="#page-tab-5">{{trans('menu.business_ethics')}}</a>--}}
    {{--<a class="dropdown-item"--}}
    {{--href="/the-banks-capital#page-tab-1">{{trans('menu.risk_Management')}}</a>--}}
    {{--<a class="dropdown-item"--}}
    {{--href="/the-banks-capital#page-tab-2">{{trans('menu.Annual_report')}}</a>--}}
    {{--<a class="dropdown-item"--}}
    {{--href="/the-banks-capital#page-tab-3">{{trans('menu.Corporate_information_according_to_NBRM')}}</a>--}}
    {{--<a class="dropdown-item"--}}
    {{--href="/the-banks-capital#page-tab-4">{{trans('menu.the_bank’s_capital')}}</a>--}}
    {{--</div>--}}
    {{--</li>--}}


    {{--<li class="nav-item dropdown">--}}
    {{--<a class="nav-link dropdown-toggle" href="#" id="eco-dropdown" data-toggle="dropdown"--}}
    {{--aria-haspopup="true" aria-expanded="false">--}}
    {{--{{trans('menu.ECO')}}--}}
    {{--</a>--}}
    {{--<div class="dropdown-menu" aria-labelledby="eco-dropdown">--}}
    {{--<a class="dropdown-item"--}}
    {{--href="/our-eco-strategy">{{trans('menu.OUR_ECO_STRATEGY')}}</a>--}}
    {{--<a class="dropdown-item" href="/eco">{{trans('menu.eco_loans')}}</a>--}}
    {{--</div>--}}
    {{--</li>--}}
    {{--<li class="nav-item dropdown">--}}
    {{--<a class="nav-link dropdown-toggle" href="#" id="24-dropdown" data-toggle="dropdown"--}}
    {{--aria-haspopup="true" aria-expanded="false">--}}
    {{--{{trans('menu.24/7_BANKING')}}--}}
    {{--</a>--}}
    {{--<div class="dropdown-menu" aria-labelledby="24-dropdown">--}}
    {{--<a class="dropdown-item"--}}
    {{--href="/#">{{trans('menu.ZONA_24/7')}}</a>--}}
    {{--<a class="dropdown-item"--}}
    {{--href="/#">{{trans('menu.SIGNING LOAN CONTRACTS ELECTRONICALLY')}}</a>--}}
    {{--<a class="dropdown-item"--}}
    {{--href="/#">{{trans('menu.APPLY_ON_LINE')}}</a>--}}
    {{--</div>--}}
    {{--</li>--}}

    {{--<li class="nav-item">--}}
    {{--<a class="nav-link"--}}
    {{--href="https://hr.procredit-group.com/mk?languagecode=en">{{trans('menu.OUR_STAFF')}}</a>--}}
    {{--</li>--}}
    {{--<div class="d-flex  ml-lg-auto mx-auto">--}}
    {{--<li class="nav-item e-banking-button m-1 w-100">--}}
    {{--<a class="nav-link btn btn-primary"--}}
    {{--href="https://ebank.pcb.com.mk/User/LogOn">{{trans('menu.e-banking')}}</a>--}}

    {{--</li>--}}
    {{--<li class="nav-item e-banking-button m-1 w-100">--}}
    {{--<a class="nav-link btn btn-primary"--}}
    {{--href="https://sso.pcb.com.mk/account/signin?">{{trans('menu.e-signing')}}</a>--}}

    {{--</li>--}}
    {{--</div>--}}
    {{--</ul>--}}

    {{--<div class="language-switcher">--}}
    {{--<ul class="list-unstyled d-flex" style="text-transform: uppercase;">--}}

    {{--@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)--}}
    {{--<li>--}}
    {{--<a rel="alternate" hreflang="{{ $localeCode }}"--}}
    {{--href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">--}}
    {{--{{ $localeCode }}--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--@endforeach--}}

    {{--</ul>--}}

    {{--</div>--}}
    {{--</nav>--}}

    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- HEADER -->--}}
    {{--@endif--}}
    <body class="page-1">
    <!-- SUB HEADER -->
    <div class="bg-primary-home entry-points-wrapper">
        <div class="row px-0 mx-0">
            <div class="container px-0">
                <ul class="navbar-nav entry-points">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="businessClient-dropdown1"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{trans('menu.business_clients')}}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="businessClient-dropdown1">
                            <a class="dropdown-item" href="/accounts-and-payments">{{trans('menu.accounts')}}</a>
                            <a class="dropdown-item" href="/credit-offers">{{trans('menu.credit_line')}}</a>
                            <a class="dropdown-item"
                               href="/documentary-operations">{{trans('menu.documentary_operations')}}</a>
                            <a class="dropdown-item"
                               href="/term-deposits">{{trans('menu.deposits_and_state_bonds')}}</a>
                            <a class="dropdown-item"
                               href="/services-for-non-resident-legal-entities">{{trans('menu.services_for_non_resident_legal_entities')}}</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" target="_blank"
                           href="https://www.procreditbank-direct.com/macedonia/mk">{{trans('menu.private_clients')}}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection