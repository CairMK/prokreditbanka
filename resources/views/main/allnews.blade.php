@extends('layouts.main')
@include('layouts.menu')

@section('content')
    <section>
        <div class="row px-0 mx-0 ">
            <div class="container-fluid bg-primary">
                <div class="container">
                    <div class="sub-header-content d-flex justify-content-end">
                        <a href="javascript:void(window.open('http://prokredit.dadigitalpresent.mk/livezilla/chat.php','','width=400,height=600,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'))"
                           class="chat-now-button">Започни разговор</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">

            <div class="row">
                @include('main.sidebar')
                <div class="col-lg-9">
                    <div class="row">

                        @foreach($news as $newss)
                            {{--<div class="col-lg-4">--}}
                            {{--@if(! empty($newss['image']))--}}
                            {{--<img src="/assets/img/news/medium/{{ $newss->image }}"--}}
                            {{--alt="{{ $newss->title }}">--}}
                            {{--@endif--}}
                            {{--</div>--}}
                            <div class="col-lg-12 single-news">
                                <h2 class="blogtitle">
                                    <a href="/news/{{ $newss->slug }}">{!! $newss->title !!}</a>
                                </h2>
                                <p>{!! str_limit(strip_tags($newss->description),350, $end = '...' ) !!}</p>
                            </div>
                        @endforeach
                    </div>
                    {!! $news->links() !!}

                </div>
            </div>

        </div>
    </section>
@endsection