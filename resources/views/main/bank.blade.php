@extends('layouts.main')
@include('layouts.menu')
@section('content')
    <!-- SUB HEADER -->
    @if( empty($staticpage['image']))
        <div class="container-fluid bg-light">
            <div class="container">
                <div class="row px-0 mx-0 py-3">
                    <h1>{{ $staticpage->title }}</h1>
                </div>
            </div>
        </div>
        <div class="row px-0 mx-0 ">
            <div class="container-fluid bg-primary">
                <div class="container">
                    <div class="sub-header-content d-flex justify-content-between">
                        <ul class="breadcrumbs d-flex list-unstyled">
                            <li><a href="/">{{trans('menu.home')}}</a></li>
                            <li><a href="">{{ $staticpage->title }}</a></li>
                        </ul>
                        <a href="javascript:void(window.open('http://prokredit.dadigitalpresent.mk/livezilla/chat.php?v=2&linkid=NGQ0ZGExNWZiMDQ4ZjkyN2VmMGE1YmUyOGM2NmFmNWE_','','width=400,height=600,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'))"
                           class="chat-now-button">Започни разговор</a>

                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="page-content">
                <div class="row mx-0 px-0">
                    <!-- SIDEBAR -->
                    <div class="col-md-12">
                        <div class="tab-content" id="pageTabs">
                            <div class="tab-pane fade show active" id="page-tab-1" role="tabpanel">
                                <h2 class="heading">{{ $staticpage->title }}</h2>
                                <br>
                                {!! $staticpage->description !!}
                            </div>
                        </div>
                    </div>
                    <!-- CONTENT -->
                </div>
            </div>

            <!-- MAIN CONTENT CONTAINER -->
            @else
                <div class="sub-header-image"
                     style="background-image: url('/assets/img/staticpage/{{ $staticpage->image }}');"></div>
                <div class="bg-primary">
                    <div class="row px-0 mx-0 ">
                        <div class="container-fluid bg-primary">
                            <div class="container">
                                <div class="sub-header-content d-flex justify-content-between">
                                    <ul class="breadcrumbs d-flex list-unstyled">
                                        <li><a href="/">{{trans('menu.home')}}</a></li>
                                        <li><a href="">{{ $staticpage->title }}</a></li>
                                    </ul>
                                    <a href="#" class="chat-now-button">{{trans('menu.chat_now')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SUB HEADER -->
                <!-- main-container start -->
                <!-- ================ -->
                <!-- MAIN CONTENT CONTAINER -->
                <div class="container">
                    <div class="page-content">
                        <div class="row mx-0 px-0">
                            <!-- SIDEBAR -->
                            <div class="col-md-3">
                                <div class="sidebar">
                                    <ul class="list-unstyled nav nav-tabs">
                                        @if( ! empty($staticpage['title']))
                                            <li><a class="nav-link active" data-toggle="tab"
                                                   data-target="#page-tab-1"
                                                   href="#page-tab-1">{{ $staticpage->title }}</a>
                                            </li>
                                        @endif
                                        @if( ! empty($staticpage['title1']))
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-2" href="
                                   #page-tab-2">{{ $staticpage->title1 }}</a>
                                            </li>
                                        @endif
                                        @if( ! empty($staticpage['title2']))
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-3" href="
                                   #page-tab-3">{{ $staticpage->title2 }}</a>
                                            </li>
                                        @endif
                                        @if( ! empty($staticpage['title3']))
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-4" href="
                                   #page-tab-4">{{ $staticpage->title3 }}</a>
                                            </li>
                                        @endif
                                        @if( ! empty($staticpage['title4']))
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-5" href="
                                   #page-tab-5">{{ $staticpage->title4 }}</a>
                                            </li>
                                        @endif
                                        @if( ! empty($staticpage['title5']))
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-6"
                                                   href="#page-tab-6">{{ $staticpage->title5 }}</a>
                                            </li>
                                        @endif
                                        @if( ! empty($staticpage['title6']))
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-7"
                                                   href="#page-tab-7">{{ $staticpage->title6 }}</a>
                                            </li>
                                        @endif
                                        @if( ! empty($staticpage['title7']))
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-8"
                                                   href="#page-tab-7">{{ $staticpage->title7 }}</a>
                                            </li>
                                        @endif
                                        @if( ! empty($staticpage['title8']))
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-9"
                                                   href="#page-tab-7">{{ $staticpage->title8 }}</a>
                                            </li>
                                        @endif
                                        @if( ! empty($staticpage['title9']))
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-10"
                                                   href="#page-tab-7">{{ $staticpage->title9 }}</a>
                                            </li>
                                        @endif
                                    </ul>
                                    @include('main.sidebar')
                                </div>
                            </div>
                            <!-- SIDEBAR -->
                            <!-- CONTENT -->
                            <div class="col-md-9">
                                <div class="col-md-12">
                                    {{--<img class="img-fluid" src="/assets/img/staticpage/{{ $staticpage->image }}">--}}
                                </div>
                                {{--<div class="col-md-12">--}}
                                {{--<h3 class="subheading text-uppercase">Accounts</h3>--}}
                                {{--</div>--}}
                                <div class="tab-content" id="pageTabs">

                                    <div class="tab-pane fade show active" id="page-tab-1" role="tabpanel">
                                        <div class="col-md-12 border-bottom mb-3">
                                            <h2 class="heading">{{ $staticpage->title }}</h2>
                                        </div>

                                        <div class="col-md-12">
                                            {!! $staticpage->description !!}
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-2" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading">{!! $staticpage->title1 !!}</h2>
                                        </div>
                                        <div class="col-md-12">
                                            {!!$staticpage->description1 !!}
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-3" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading">{!! $staticpage->title2 !!}</h2>
                                        </div>
                                        <div class="col-md-12">
                                            {!! $staticpage->description2 !!}
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-4" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading">{{ $staticpage->title3 }}</h2>
                                        </div>
                                        <div class="col-md-12">
                                            {!! $staticpage->description3 !!}
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-5" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading">{{ $staticpage->title4 }}</h2>
                                        </div>
                                        <div class="col-md-12">
                                            {!! $staticpage->description4 !!}
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-6" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading">{{ $staticpage->title5 }}</h2>
                                        </div>
                                        <div class="col-md-12">
                                            {!! $staticpage->description5!!}
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-7" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading">{{ $staticpage->title6 }}</h2>
                                        </div>
                                        <div class="col-md-12">
                                            {!! $staticpage->description6 !!}
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-8" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading">{{ $staticpage->title7 }}</h2>
                                        </div>
                                        <div class="col-md-12">
                                            {!! $staticpage->description7 !!}
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-9" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading">{{ $staticpage->title8 }}</h2>
                                        </div>
                                        <div class="col-md-12">
                                            {!! $staticpage->description8 !!}
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-10" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading">{{ $staticpage->title9 }}</h2>
                                        </div>
                                        <div class="col-md-12">
                                            {!! $staticpage->description9 !!}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- CONTENT -->
                    </div>
                </div>
        </div>
    @endif
    <!-- MAIN CONTENT CONTAINER -->
@endsection
