@extends('layouts.main')
@include('layouts.menu')

@section('content')
    <!-- SUB HEADER -->
    <div class="bg-light">
        <div class="row px-0 mx-0 py-4">
            <div class="container">
                <h1 class="page-title">Kалкулатор за орочен депозит</h1>
            </div>
        </div>
    </div>
    <div class="bg-primary">
        <div class="row px-0 mx-0 ">
            <div class="container">
                <div class="sub-header-content d-flex justify-content-between">
                    <ul class="breadcrumbs d-flex list-unstyled">
                        <li></li>
                    </ul>
                    <a href="#" class="chat-now-button">Започни разговор</a>
                </div>
            </div>
        </div>
    </div>
    <!-- MAIN CONTENT CONTAINER -->
    <div class="container">
        <div class="col-md-5 mx-auto ">
            <div class="row mx-0 px-0">
                <div class="col-md-12 mx-auto mt-5 text-center bg-secondary">
                    <h5 class="heading my-2 mt-3 text-white text-uppercase">Калкулатор за орочен депозит</h5>
                    <p></p>
                </div>

                <div class="col-md-12 mx-auto calculator-wrapper">
                    <form method="post" action="" class="form">

                        <div class="row mx-0 p-x0">

                            <div class="col-md-12 col-12  my-2">
                                <div class="row mx-0 px-0 py-3 py-md-4">
                                    <div class="col-md-5 mx-0 px-0">
                                        <label for="valuta">Избери Валута</label>
                                    </div>
                                    <div class="col-md-7 mx-0 px-0">
                                        <select class="valuta" id="valuta" name="valuta">
                                            <option value="MKD">МКД</option>
                                            <option value="EUR" >ЕУР</option>
                                        </select>
                                    </div>
                                </div>
                            </div>



                            <div class="col-md-12 col-12  my-2 ">
                                <div class="row mx-0 px-0 border-top py-3 py-md-4">
                                    <div class="col-md-5 mx-0 px-0">
                                        <label for="valuta">Избери сума:</label>
                                        <p class="small">Минималната сума за орочен депозит е 5.000 евра или 300.000 денари</p>
                                    </div>
                                    <div class="col-md-7 mx-0 px-0">
                                        <input type="range" name="sumaInput" id="sumaInput" value="300000" step="100"  min="300000"
                                               max="10000000" oninput="sumaOutput.value = sumaInput.value">
                                        <div class="price"><input name="sumaOutput" value="300000" id="sumaOutput" class="priceformat" step="100"  min="300000" max="10000000"
                                                                  oninput="sumaInput.value = sumaOutput.value "><span> ден.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-12   my-2">
                                <div class="row mx-0 px-0 border-top py-3 py-md-4">
                                    <div class="col-md-5 mx-0 px-0">
                                        <label for="valuta">Ороченост на:</label>
                                    </div>
                                    <div class="col-md-7 mx-0 px-0">
                                        <select class="rocnost" id="rocnost" name="rocnost">
                                            <option value="12" data-kamataeur="10" data-kamataden="18">12 месеци
                                            </option>
                                            <option value="18" data-kamataeur="12" data-kamataden="20">18 месеци
                                            </option>
                                            <option value="24" data-kamataeur="18" data-kamataden="23">24месеци</option>
                                            <option value="36" data-kamataeur="15" data-kamataden="26">36 месеци
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 mx-auto">
                                <div class="col-md-12 my-3 px-0 mx-auto">
                                    <button type="submit" id="presmetaj" class="btn btn-primary">Пресметај</button>
                                </div>
                            </div>
                            <div class="col-12 results-calc mt-5" style="display: none;">
                                <h6>Пресметаниот износ на податоците е:</h6>
                                <div class=" mx-0 border-top border-bottom pt-4 pb-2">
                                    <p class="valuta-res">Валута: <span></span></p>
                                    <p class="suma-res">Вложена сума: <span></span><em></em></p>
                                    <p class="rocnost-res">Ороченост на: <span></span> месеци</p>
                                    <p class="kamata-res">Вкупна камата: <span></span> %</p>
                                </div>
                                <p class="mt-2">Вкупна заработка:</p>
                                <h5 id="calcResult"><span>600</span><em></em></h5>
                                <button id="reset-btn" class="reset-btn my-3" value=""><i class="fa fa-refresh"></i>Ресетирај
                                    ги податоците
                                </button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- MAIN CONTENT CONTAINER -->

