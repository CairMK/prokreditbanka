@extends('layouts.main')
@include('layouts.menu')

@section('content')
    <!-- SUB HEADER -->
    <div class="bg-light">
        <div class="row px-0 mx-0 py-4">
            <div class="container">
                <h1 class="page-title">Kалкулатор</h1>
            </div>
        </div>
    </div>
    <!-- CALCULATOR -->

    <div class="container">
        <div class="col-md-5 mx-auto ">
            <div class="row mx-0 px-0">
                <div class="col-md-12 mx-auto mt-5 text-center bg-secondary">
                    <h5 class="heading my-2 mt-3 text-white text-uppercase">Калкулатор за кредити за инвестиции</h5>
                    <p></p>
                </div>
                <div class="col-md-12 mx-auto calculator-wrapper">
                    <form method="post" action="" class="form">
                        <div class="row mx-0 p-x0">
                            <div class="col-md-12 col-12  my-2">
                                <div class="row mx-0 px-0 py-3 py-md-4">
                                    <div class="col-md-5 mx-0 px-0">
                                        <label for="tip">Тип на кредит</label>
                                    </div>
                                    <div class="col-md-7 mx-0 px-0">
                                        <select class="tip" id="tip" name="tip">
                                            <option value="1">Станбен кредит</option>
                                            <option value="2">Кредит за инвестиции</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-12  my-2">
                                <div class="row mx-0 px-0 py-3 py-md-4">
                                    <div class="col-md-5 mx-0 px-0">
                                        <label for="valuta">Избери Валута</label>
                                    </div>
                                    <div class="col-md-7 mx-0 px-0">
                                        <select class="valuta" id="valuta" name="valuta">
                                            <option value="EUR">ЕУР</option>
                                            <option value="MKD" id="mkd-choice"  disabled>МКД</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            <div class="col-md-12 col-12  my-2 ">
                                <div class="row mx-0 px-0 border-top py-3 py-md-4">
                                    <div class="col-md-5 mx-0 px-0">
                                        <label for="suma">Избери сума:</label>
                                         <p class="small small-range">Минималната сума за овој тип на кредит е 30.000 евра, а максималната е неограничена</p>
                                    </div>
                                    <div class="col-md-7 mx-0 px-0">
                                        <input type="range" name="sumaInput" id="sumaInput" step="100" value="30000" min="30000"
                                               max="10000000" oninput="sumaOutput.value = sumaInput.value">
                                        <div class="price price-price"><input name="sumaOutput" step="100"  id="sumaOutput" value="30000" min="30000"
                                                                  max="1000000"
                                                                  oninput="sumaInput.value = sumaOutput.value "><span> €</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-12  my-2 ">
                                <div class="row mx-0 px-0 border-top py-3 py-md-4">
                                    <div class="col-md-5 mx-0 px-0">
                                        <label for="valuta">Рок на враќање:</label>
                                        <p class="small small-rok">Маскималниот рок за овој тип на кредит е: <span>240</span>
                                            месеци</p>
                                    </div>
                                    <div class="col-md-7 mx-0 px-0">

                                        <input type="range" name="rokInput" id="rokInput" value="240" min="12" max="240"
                                               oninput="rokOutput.value = rokInput.value">
                                        <div class="price price-meseci"><input name="rokOutput" id="rokOutput" value="240" min="12"
                                                                  max="240 "
                                                                  oninput="rokInput.value = rokOutput.value "><br><span> месеци.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 mx-auto">
                                <div class="col-md-12 my-3 px-0 mx-auto">
                                    <button type="submit" id="presmetaj1" class="btn btn-primary">Пресметај</button>
                                </div>
                            </div>
                            <div class="col-12 results-calc mt-5" style="display: none;">
                                <h6>Пресметаниот износ на податоците е:</h6>
                                <div class=" mx-0 border-top border-bottom pt-4 pb-2">
                                    <p class="tip-res">Тип: <span></span></p>
                                    <p class="suma-res">Износ на кредитот : <span></span><em></em></p>
                                    <p class="kamata-res">Месечна каматна стапка: <span>6</span> %</p>
                                    <p class="rocnost-res">Рок на враќање (во месеци): <span></span> месеци</p>
                                    <p class="vkupen-iznos-res">Вкупен износ на кредитот : <span></span><em></em></p>
                                    <p class="vkupna-kamata-res">Вкупна камата: <span></span> <em></em></p>
                                    <p class="ednokratna-provizija-res">Процент на еднократна провизија на банката: <span></span> %</p>
                                    <p class="iznos-ednokratna-provizija-res">Износ на еднократна провизија на банката (се одбива при исплатата на кредитот) <span></span> <em></em></p>
                                    <p class="podiganje-na-klient-res">Колку ќе подигне клиентот: <span></span> <em></em></p>
                                   
                    
                                    
                                </div>
                                <p class="mt-2">Износ на месечна рата:</p>
                                <h5 id="calcResult"><span></span><em> €</em></h5>
                                <button id="reset-btn" class="reset-btn my-3" value=""><i class="fa fa-refresh"></i>Ресетирај
                                    ги податоците
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- CALCULATOR  -->
    
@endsection
<!-- MAIN CONTENT CONTAINER -->

