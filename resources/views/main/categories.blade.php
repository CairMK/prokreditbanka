@extends('layouts.main')
@include('layouts.menu')

@section('content')
    <div class="container">
        {{--<div class="breadcrumb-container">--}}
        {{--<div class="container">--}}
        {{--<ol class="breadcrumb">--}}
        {{--<li><i class="fa fa-home pr-10"></i><a href="/">Почетна</a></li>--}}
        {{--<li><a href="/">Категории</a></li>--}}
        {{--<li class="active">{{ $category->name }}</li>--}}
        {{--</ol>--}}
        {{--</div>--}}
        {{--</div>--}}
        <section>
            @if($category->image)
                <img src="/assets/img/categories/{{$category->image}}" alt="slidebg1">
                @foreach($sliders as $slider)
                    <img src="/assets/img/sliders/{{$slider->image}}" alt="slidebg1">
                @endforeach
            @endif
            <div class="col-md-12">
                <h1 class="page-title">{{ $category->name }}</h1>
                <div class="separator-2"></div>
                {!! $category->description !!}
                @foreach($category->getImmediateDescendants() as $descendant)
                    <div class="row">
                        <div class="col-md-4">
                            <article class="blogpost shadow light-gray-bg bordered">
                                @if($descendant->image)
                                    <div class="overlay-container">
                                        <img src="/assets/img/categories/{{ $descendant->image }}" alt="">
                                        <a class="overlay-link" href="/categories/{{ $descendant->slug }}""><i
                                                class="fa fa-link"></i></a>
                                    </div>
                                @endif
                                <header>
                                    <h2><a href="/categories/{{ $descendant->slug }}">{{ $descendant->name }}</a>
                                    </h2>

                                </header>
                                <div class="blogpost-content">
                                    <p>{!! strip_tags(str_limit($descendant->description,100,'...')) !!}</p>
                                </div>
                                <footer class="clearfix">

                                    <div class="link pull-right"><i class="icon-link"></i><a
                                                href="/categories/{{ $descendant->slug }}">Повеќе</a>
                                    </div>
                                </footer>
                            </article>
                        </div>
                    </div>
                        @endforeach
                        <div class="row">
                            @foreach($products as $product)
                                <div class="col-md-4">
                                    <article>
                                        <div class="overlay-container">
                                            <img src="/assets/img/products/{{ $product->image }}" alt="">
                                            <a class="overlay-link" href="/product/{{ $product->slug }}""><i
                                                    class="fa fa-link"></i></a>
                                        </div>
                                        <header>
                                            <h2><a href="/product/{{ $product->slug }}">{{ $product->title }}</a>
                                            </h2>

                                        </header>
                                        <div class="blogpost-content">
                                            <p>{!! strip_tags(str_limit($product->description,100,'...')) !!}</p>
                                        </div>
                                        <footer class="clearfix">

                                            <div class="link pull-right"><i class="icon-link"></i><a
                                                        href="/product/{{ $product->slug }}">Повеќе</a>
                                            </div>
                                        </footer>
                                    </article>
                                </div>
                            @endforeach
                        </div>

                        <aside class="col-md-2 col-lg-2 col-lg-offset-1">

                            <a href="/assets/files/categories/{{ $category->file }}"
                               alt="{{ $category->filename }}"
                               target="_blank">{{ $category->filename }}</a>


                        </aside>
                    </div>
            </div>
        </section>
    </div>

    <!-- main-container end -->
@endsection