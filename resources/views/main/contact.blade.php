@extends('layouts.main')
@include('layouts.menu')

@section('content')
    <!-- MAIN CONTENT CONTAINER -->
    <div class="sub-header-image"
         style="background-image: url('/assets/img/apliciraj-mk.jpg');">

    </div>
    <div class="container">

        <div class="page-content">
            <div class="row mx-0 px-0">
                <!-- SIDEBAR -->
                <div class="col-md-3">
                    <div class="sidebar">
                        <ul class="list-unstyled nav nav-tabs">
                            <li><a class="nav-link active" data-toggle="tab"
                                   href="#page-tab-1">{{trans('menu.aplication_f')}}</a></li>
                            <li><a class="nav-link" data-toggle="tab"
                                   href="#page-tab-2">{{trans('menu.aplication_p')}}</a>
                            </li>
                        </ul>
                    </div>
                    <div class="sidebar-bottom mt-3">
                        <ul class="list-unstyled">
                            <li class="sidebar-button pricelist"><a
                                        href="http://staging.procreditbank.com.mk/content/Aneks%206A_Aplikacija%20za%20registracija%20na%20klient%20-%20fizicko%20lice%20MK.pdf"
                                        target="_blank"> {{trans('menu.aplicationpdf_f')}}</a></li>
                            <li class="sidebar-button pricelist"><a
                                        href="http://staging.procreditbank.com.mk/content/pdf/Aneks%207A_Aplikacija%20za%20registracija%20na%20klient%20pravno%20lice%20MK.pdf"
                                        target="_blank">{{trans('menu.aplicationpdf_p')}}</a></li>
                        </ul>
                    </div>
                </div>
                <!-- SIDEBAR -->
                <!-- CONTENT -->
                <div class="col-md-9">
                    <div class="tab-content" id="pageTabs">

                        <div class="tab-pane fade show active" id="page-tab-1" role="tabpanel">


                            <div class="col-md-12 mx-auto">
                                <div class="row mx-0">
                                    <!-- Aplikacija-->
                                    <div class="col-md-12">
                                        <a href="/ebank-f">
                                            <div class="apply-box ">
                                                <h2 class="">Аплицирај за е-банкарство</h2>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- Aplikacija -->
                                    <!-- Aplikacija-->
                                    <div class="col-md-12">
                                        <a href="">
                                            <div class="apply-box ">
                                                <h2 class="">Аплицирај за картичка</h2>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- Aplikacija -->
                                    <!-- Aplikacija-->
                                    <div class="col-md-12">
                                        <a href="/flex">
                                            <div class="apply-box ">
                                                <h2 class="">Аплицирај за flex save</h2>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- Aplikacija -->
                                    <!-- Aplikacija-->
                                    <div class="col-md-12">
                                        <a href="/deposit">
                                            <div class="apply-box ">
                                                <h2 class="">Аплицирај за орочен депозит</h2>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- Aplikacija -->
                                    <!-- Aplikacija-->
                                    <div class="col-md-12">
                                        <a href="/stamben">
                                            <div class="apply-box ">
                                                <h2 class="">Аплицирај за станбен кредит</h2>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- Aplikacija -->
                                    <!-- Aplikacija-->
                                    <div class="col-md-12">
                                        <a href="/invest">
                                            <div class="apply-box ">
                                                <h2 class="">Аплицирај за кредит за инвестиции</h2>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- Aplikacija -->
                                    <!-- Aplikacija-->
                                    <div class="col-md-12">
                                        <a href="">
                                            <div class="apply-box ">
                                                <h2 class="">Аплицирај за flex fund</h2>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- Aplikacija -->
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="page-tab-2" role="tabpanel">
                            <div class="col-md-12 mx-auto">
                                <div class="row mx-0">
                                    <!-- Aplikacija-->
                                    <div class="col-md-12">
                                        <a href="">
                                            <div class="apply-box ">
                                                <h2 class="">Аплицирај за гаранции</h2>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- Aplikacija -->
                                    <!-- Aplikacija-->
                                    <div class="col-md-12">
                                        <a href="">
                                            <div class="apply-box ">
                                                <h2 class="">Аплицирај за акредитив</h2>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- Aplikacija -->
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- CONTENT -->
            </div>
        </div>
    </div>

    <!-- MAIN CONTENT CONTAINER -->
@endsection
<!-- MAIN CONTENT CONTAINER -->

