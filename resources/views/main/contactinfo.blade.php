@extends('layouts.main')
@include('layouts.menu')

@section('content')
    <!-- SUB HEADER -->
    <div class="bg-light">
        <div class="row px-0 mx-0 py-4">
            <div class="container">
                <h1 class="page-title">Контакт</h1>
            </div>
        </div>
    </div>
    <div class="bg-primary">
        <div class="row px-0 mx-0 ">
            <div class="container">
                <div class="sub-header-content d-flex justify-content-between">
                    <ul class="breadcrumbs d-flex list-unstyled">
                        <li><a href="/">Home</a></li>
                        <li><a href="">Contact</a></li>
                    </ul>
                    <a href="javascript:void(window.open('http://prokredit.dadigitalpresent.mk/livezilla/chat.php','','width=400,height=600,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'))"
                       class="chat-now-button">{{trans('menu.start_conversation')}}</a></div>
            </div>
        </div>
    </div>
    <!-- MAIN CONTENT CONTAINER -->

    <div class="container contact-page-container">
        <div class="row mx-0 px-0">
            <div class="col-md-5 mt-5">
                <h5 class="heading my-2 pb-2 border-bottom">Контакт информации</h5>
                <ul class="list-unstyled p-2 contact-info-icons">
                    <li class="d-flex align-items-center"><i class="swift-icon"></i>+389 2 / 244 60 00</li>

                    <li class="d-flex align-items-center"><i class="envelope-icon"></i>info@procreditbank.com.mk</li>
                    <li class="d-flex align-items-center"><i class="location-icon"></i>Manapo br.7 1000 Skopje</li>
                </ul>
                <h5 class="heading mt-5 mb-2 pb-2 border-bottom">Често поставувани прашања</h5>
                <a href="/faq" class="btn btn-primary">Пронајди одговор</a>

            </div>

            <div class="col-md-7 mt-5">
                <h5 class="heading my-2 pb-2 border-bottom">Испрати ни е-маил</h5>

                <form method="post" action="" class="form">
                    <div class="row">
                        <div class="col-lg-6  py-4">
                            <div class="col-lg-12 col-md-12 px-0 ">
                                <div class="col-md-12 my-3 px-0 ">
                                    <label for="name">Име</label>
                                    <input type="text" name="name" id="name" placeholder="Име">
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 px-0 ">
                                <div class="col-md-12 my-3 px-0 ">
                                    <label for="last_name">Презиме</label>
                                    <input type="text" name="last_name" id="last_name" placeholder="Презиме">
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 px-0 ">
                                <div class="col-md-12 my-3 px-0 ">
                                    <label for="email">Email</label>
                                    <input type="text" name="email" id="email" placeholder="Е-маил">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 py-4">
                            <div class="col-lg-12 col-md-12 px-0 ">
                                <div class="col-md-12 my-3 px-0 ">
                                    <label for="email">Message</label>
                                    <textarea name="message" id="message" placeholder="Порака" rows="5"></textarea>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 px-0 ">
                                <div class="col-md-12 my-3 px-0 ">
                                    <button type="submit" id="submit_contact" class="btn btn-primary">Испрати</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="col-md-12 mt-4 py-3">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11858.678172762437!2d21.384179369775385!3d42.0073671!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1354146fc220a7b9%3A0xe42f618f120961ab!2sProKredit+Banka+HQ!5e0!3m2!1sen!2smk!4v1530535321389"
                        width="100%" height="250" frameborder="0" style="width:100%" style="border:0"
                        allowfullscreen></iframe>
            </div>

        </div>
    </div>

    <!-- MAIN CONTENT CONTAINER -->

@endsection