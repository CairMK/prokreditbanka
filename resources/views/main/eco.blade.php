@extends('layouts.main')
@include('layouts.menu')

@section('content')
    <!-- SUB HEADER -->
    <div class="bg-light">
        <div class="row px-0 mx-0 py-4">
            <div class="container">
                <h1 class="page-title">Eco</h1>
            </div>
        </div>
    </div>
    <div class="bg-primary">
        <div class="row px-0 mx-0 ">
            <div class="container">
                <div class="sub-header-content d-flex justify-content-between">
                    <ul class="breadcrumbs d-flex list-unstyled">
                        <li><a href="/">Почетна</a></li>
                        <li><a href="">Еко кредитирање</a></li>
                    </ul>
                    <a href="javascript:void(window.open('http://prokredit.dadigitalpresent.mk/livezilla/chat.php','','width=400,height=600,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'))"
                       class="chat-now-button">Започни разговор</a>
                </div>
            </div>
        </div>
    </div>
    <!-- MAIN CONTENT CONTAINER -->
    <div class="container">
        <div class="page-content">
            <div class="row mx-0 px-0">
                <!-- BOX 1 -->
                <div class="col-md-6">
                    <a href="/ecohouseholds">
                        <div class="eco-box green-box">
                            <h2 class="eco-title text-uppercase text-white">За вашиот дом</h2>
                            <p class="eco-content">Дали вашиот план е:
                                <br>покомфорен дом
                                <br>рeновирање на домот
                                <br>потопол дом во зима
                                <br>посвеж дом во лето
                                <br>пониски трошоци за комуналии
                            </p>
                            <img src="/assets/img/eco-icon-1.png" class="eco-icon">
                            <img src="/assets/img/eco-image-1.png" class="eco-image">
                        </div>
                    </a>
                </div>
                <!-- BOX 1 -->
                <!-- BOX 2 -->
                <div class="col-md-6">
                    <a href="/ecobusinesses">
                        <div class="eco-box red-box">
                            <h2 class="eco-title text-uppercase text-white">За вашиот бизнис</h2>
                            <p class="eco-content">Дали вашиот план е:
                                <br>модернизација на бизнисот
                                <br>помали трошоци во работењето
                                <br>поголема конкурентност
                                <br>користење енергија од обновливи извори
                            </p>
                            <img src="/assets/img/eco-icon-2.png" class="eco-icon">
                            <img src="/assets/img/eco-image-2.png" class="eco-image">
                        </div>
                    </a>
                </div>
                <!-- BOX 2 -->
            </div>
        </div>
    </div>
    <!-- MAIN CONTENT CONTAINER -->

@endsection