@extends('layouts.main')
@include('layouts.menu')
@section('content')
    <!-- SUB HEADER -->
    <div class="bg-light">
        <div class="row px-0 mx-0 py-4">
            <div class="container">
                <h1 class="page-title">Еко кредитирање за вашиот бизнис </h1>
            </div>
        </div>
    </div>
    <div class="bg-primary">
        <div class="row px-0 mx-0 ">
            <div class="container">
                <div class="sub-header-content d-flex justify-content-between">
                    <ul class="breadcrumbs d-flex list-unstyled">
                        <li><a href="/">Почетна</a></li>
                        <li><a href="">Еко кредитирање за вашиот бизнис </a></li>
                    </ul>
                    <a href="javascript:void(window.open('http://prokredit.dadigitalpresent.mk/livezilla/chat.php','','width=400,height=600,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'))"
                       class="chat-now-button">{{trans('menu.start_conversation')}}</a>
                </div>
            </div>
        </div>
    </div>
    <!-- SUB HEADER -->
    <div class="container">
        <div class="page-content">
            <div class="row mx-0 px-0">
                <!-- CONTENT -->
                <div class="col-lg-8 col-md-12">
                    <div class="tab-content" id="eco-tab-content">
                        <div class="tab-pane fade show active" id="eco-tab-1" role="tabpanel"><img class="img-fluid"
                                                                                                   src="assets/img//eco-b-1.jpg">
                        </div>
                        <div class="tab-pane fade" id="eco-tab-2" role="tabpanel"><img class="img-fluid"
                                                                                       src="assets/img/eco-b-2.jpg">
                        </div>
                        <div class="tab-pane fade" id="eco-tab-3" role="tabpanel"><img class="img-fluid"
                                                                                       src="assets/img/eco-b-3.jpg">
                        </div>
                    </div>
                </div>
                <!-- CONTENT -->
                <!-- SIDEBAR -->
                <div class="col-lg-4 col-md-12 eco-for-b">
                    <ul class="list-unstyled nav nav-tabs fancy-tabs">
                        <li><a class="nav-link active" data-toggle="tab" href="#eco-tab-1">
                                <i class="white-icon icon-home"></i>
                                Модернизирајте го Вашиот бизнис и бидете поконкурентни
                            </a></li>
                        <li><a class="nav-link" data-toggle="tab" href="#eco-tab-2">
                                <i class="white-icon icon-bulb"></i>Заштедете на трошоци
                            </a></li>
                        <li><a class="nav-link" data-toggle="tab" href="#eco-tab-3">
                                <i class="white-icon icon-solar"></i>
                                Користете енергија од обновливи извори
                            </a></li>

                    </ul>
                </div>
                <!-- SIDEBAR -->

            </div>
        </div>
    </div>
    <!-- MAIN CONTENT CONTAINER -->
@endsection