@extends('layouts.main')
@include('layouts.menu')
@section('content')
    <!-- SUB HEADER -->
    <div class="container-fluid bg-light">
        <div class="container">
            <div class="row px-0 mx-0 py-3">
                <h1></h1>
            </div>
        </div>
    </div>
    <div class="row px-0 mx-0 ">
        <div class="container-fluid bg-primary">
            <div class="container">
                <div class="sub-header-content d-flex justify-content-between">
                    <ul class="breadcrumbs d-flex list-unstyled">
                        <li><a href="/">{{trans('menu.home')}}</a></li>
                        <li><a href="">{{trans('menu.faq')}}</a></li>
                    </ul>
                    <a href="#" class="chat-now-button">{{trans('menu.chat_now')}}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="accordion" id="accordionExample">
            @foreach($atm as $key => $atms)

                <div class="card">
                    <div class="card-header" id="tab{{ $key }}">
                        <h5 class="mb-0">

                            <button class="btn btn-link collapsed " type="button"
                                    data-toggle="collapse"
                                    data-target="#tab_div{{ $key }}" aria-expanded="false"
                                    aria-controls="tab_div{{ $key }}"><i class="fa fa-angle-double-down"></i>
                                {{ $atms->title }}
                            </button>
                        </h5>
                    </div>
                    <div id="tab_div{{ $key }}" class="collapse" aria-labelledby="tab{{ $key }}"
                         data-parent="#accordionExample">
                        <div class="card-body">
                            {!! $atms->description !!}
                        </div>
                    </div>
                </div>

            @endforeach
        </div>

@endsection
