@extends('layouts.main')
@include('layouts.menu')
@section('content')
    <!-- MAIN CONTENT CONTAINER -->
    <style type="text/css">label {
            display: block !important;
            font-size: .8rem;
        }

        input[type="radio"], input[type="checkbox"] {
            width: 30px;
        } </style>
    <div class="container contact-page-container">
        <div class="row mx-0 px-0">
            <div class="col-md-6 mt-5 mx-auto">
                <h5 class="heading my-2 pb-2 border-bottom text-uppercase">Апликација за отварање Орочен депозит</h5>
                @php
                    echo'
                    <form method="post" action="/contact_submit.php" class="form">
                <div class="row">
                    <!-- input -->
                    <div class="col-md-4 my-2 ">
                        <label for="name">Име и презиме</label>
                    </div>
                    <div class="col-md-8 my-2">
                        <input type="text" name="name" id="name">
                    </div>
                    <!-- input end -->
                    <!-- input -->
                    <div class="col-md-4 my-2 ">
                        <label for="iznos">Износ на кредитот во ЕУР</label>
                    </div>
                    <div class="col-md-8 my-2">
                        <input type="number" min="300000" name="iznos" id="iznos" placeholder="">
                        <p class="small text-muted">Износот на кредитот може да биде од 10.000 – 30.000 ЕУР</p>
                    </div>
                    <!-- input end -->

                    <div class="col-md-4 my-2 ">
                        <label for="embg">ЕМБГ</label>
                    </div>
                    <div class="col-md-8 my-2">
                        <input type="text" name="embg" id="embg">
                    </div>
                    <!-- input end -->
                    <!-- input -->
                    <div class="col-md-4 my-2 ">
                        <label for="phone">Контакт телефон</label>
                    </div>
                    <div class="col-md-8 my-2">
                        <input type="text" name="phone" id="phone">
                    </div>
                    <!-- input end -->
                    <!-- input -->
                    <div class="col-md-4 my-2 ">
                        <label for="email">E-mail</label>
                    </div>
                    <div class="col-md-8 my-2">
                        <input type="email" name="email" id="email">
                    </div>
                    <!-- input end -->
                <input type="hidden" id="subject" name="subject" value="Апликација за кредит инвестирање">

                    <!-- input -->
                    <div class="col-md-1 my-4 ">
                        <input type="checkbox" name="soglasnost" id="soglasnost" >
                    </div>
                    <div class="col-md-11 my-4">
                        <p class="small text-muted">Согласност за правилата и условите
                            Одговорно тврдам, под морална, материјална и кривичнаодговорност дека податоците наведени во барањето се вистинити. Се согласувам да погоре наведените податоци бидат обработени од страна на ПроКредит Банка А.Д. Скопје за потребите на електронското барање за електронско банкарство и дигитален сертификат. Во случај да не дојде до склучување на деловен однос со Банката се согласувам истите да бидат избришани од базата на Банката
                        </p>
                    </div>
                    <!-- input end -->
                    <!-- Submit buton -->
                    <div class="col-lg-12 col-md-12 px-0 ">
                        <div class="col-md-12 my-3 px-0 ">
                            <button type="submit" id="submit_contact" class="btn btn-primary">Испрати</button>
                        </div>
                    </div>
                </div>
            </form>';
                @endphp
            </div>
        </div>
    </div>
@endsection
<!-- MAIN CONTENT CONTAINER -->