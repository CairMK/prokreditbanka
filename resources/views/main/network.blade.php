@extends('layouts.main')
@include('layouts.menu')
@section('content')
    <section><!-- src box start -->
        <div class=" col-md-12 map">
            <div class="row">

                <style>
                    #map {
                        height: 60vh;
                        width: 100%;
                    }
                </style>

                <div id="map"></div>
            @section('scripts')
                <!-- Google Maps -->
                    {{--<script async defer--}}
                    {{--src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDv7dvAdPbvPV1By-YberT3NGyrLL1_v2c&callback=initMap">--}}
                    {{--</script>--}}

                    <script type="text/javascript">
                        jQuery(function ($) {
                            <!-- Asynchronously Load the map API  -->
                            var script = document.createElement('script');
                            script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyDv7dvAdPbvPV1By-YberT3NGyrLL1_v2c&callback=initialize";
                            document.body.appendChild(script);
                        });

                        function initialize() {
                            var map;
                            var bounds = new google.maps.LatLngBounds();
                            var mapOptions = {
                                mapTypeId: 'roadmap'
                            };

                            <!-- Display a map on the page -->
                            map = new google.maps.Map(document.getElementById("map"), mapOptions);
                            map.setTilt(45);

                            <!-- Multiple Markers -->
                            var markers = [
                                    @foreach($bankomat as $bankomats)
                                ['{{ $bankomats->title }}', {{ $bankomats->lat }}, {{ $bankomats->lng }}],
                                @endforeach
                            ];

                            <!-- Info Window Content -->
                            var infoWindowContent = [
                                    @foreach($bankomat as $bankomats)
                                ['<div class="info_content">' +
                                '<h3>{!! trim (preg_replace('/\s+/', ' ' , $bankomats->description) )!!}</h3>' +
                                '</div>'],
                                @endforeach
                            ];
                            var icon = {
                                url: "assets/img/map-marker.svg",
                                anchor: new google.maps.Point(40, 40),
                                scaledSize: new google.maps.Size(40, 40)
                            }


                            <!-- Display multiple markers on a map -->
                            var infoWindow = new google.maps.InfoWindow(), marker, i;

                            <!-- Loop through our array of markers & place each one on the map   -->
                            for (i = 0; i < markers.length; i++) {
                                var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                                bounds.extend(position);
                                marker = new google.maps.Marker({
                                    position: position,
                                    map: map,
                                    icon: icon,
                                    title: markers[i][0]
                                });

                                <!-- Allow each marker to have an info window     -->
                                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                                    return function () {
                                        infoWindow.setContent(infoWindowContent[i][0]);
                                        infoWindow.open(map, marker);
                                    }
                                })(marker, i));
                                <!-- Automatically center the map fitting all markers on the screen -->
                                map.fitBounds(bounds);
                            }
                            <!-- Override our map zoom level once our fitBounds function runs (Make sure it only runs once) -->
                            var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
                                this.setZoom(9);
                                google.maps.event.removeListener(boundsListener);
                            });

                        }
                    </script>
                @endsection

            </div>
        </div>
    </section>
@endsection

