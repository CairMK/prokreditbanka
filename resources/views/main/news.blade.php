@extends('layouts.main')
@include('layouts.menu')
@section('content')
    @if(! empty($news['image']))
        <div class="sub-header-image"
             style="background-image: url('/assets/img/news/{{ $news->image }}');"></div>
        <div class="bg-primary">
            @endif
            <div class="row px-0 mx-0 ">
                <div class="container-fluid bg-primary">
                    <div class="container">
                        <div class="sub-header-content d-flex justify-content-end">

                            <a href="javascript:void(window.open('http://prokredit.dadigitalpresent.mk/livezilla/chat.php','','width=400,height=600,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'))"
                               class="chat-now-button">{{trans('menu.start_conversation')}}</a></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SUB HEADER -->
        <!-- main-container start -->
        <section>
            <div class="container ">
                <div class="row">
                    @include('main.sidebar')

                    <div class="col-lg-9">
                        <div class="blogDec hlafWidthdec">
                            <h2 class="blogtitle">
                                <br>
                                {!! $news->title !!}
                            </h2>
                            <p>{!! $news->description !!}</p>

                        </div>
                    </div>
                </div>
            </div>

        </section>



@endsection