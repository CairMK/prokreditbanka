@extends('layouts.main')
@include('layouts.menu')

@section('content')
    <div class="container">
        <div class="row mx-0">
            <div class="col-4">
                <div class="box m-2 bg-light">
                <p>
                    <a  class="text-uppercase font-weight-bold" href="/categories/prodazhba-na-imot-vo-sopstvenost-na-bankata">{{trans('menu.ongoing_tenders')}}</a>
                </p>
                </div>
            </div>
            <div class="col-4">
                 <div class="box m-2 bg-light">
                <p><a class="text-uppercase font-weight-bold"  href="/categories/prodazhba-na-imot-preku-izvrshitel">{{trans('menu.sale_through_executor')}}</a></p>
            </div>
             </div>
        </div>
    </div>
@endsection