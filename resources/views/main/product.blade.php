@extends('layouts.main')
@include('layouts.menu')
@section('content')

    <!-- SUB HEADER -->
    <div class="sub-header-image" style="background-image: url('/assets/img/products/{{ $product->image }}');"></div>
    <div class="bg-primary">
        <div class="row px-0 mx-0 ">
            <div class="container">
                <div class="sub-header-content d-flex justify-content-between">
                    <ul class="breadcrumbs d-flex list-unstyled">
                        <li><a href="/">Home</a></li>
                        <li><a href="">{{ $product->title }}</a></li>
                    </ul>
                    <a href="#" class="chat-now-button">Chat now</a>
                </div>
            </div>
        </div>
    </div>
    <!-- SUB HEADER -->
    <!-- main-container start -->
    <!-- ================ -->
    <!-- MAIN CONTENT CONTAINER -->

    <div class="container">
        <div class="page-content">
            <div class="row mx-0 px-0">
                <!-- SIDEBAR -->
                <div class="col-md-3">
                    <div class="sidebar">
                        <ul class="list-unstyled nav nav-tabs">
                            <li><a class="nav-link active" data-toggle="tab"
                                   href="#page-tab-1">{{ $product->title1 }}</a>
                            </li>
                            <li><a class="nav-link" data-toggle="tab" href="#page-tab-2">{{ $product->title2 }}</a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#page-tab-3">{{ $product->title3 }}</a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#page-tab-4">{{ $product->title4 }}</a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#page-tab-5">{{ $product->title5 }}</a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#page-tab-6">{{ $product->title6 }}</a></li>
                        </ul>
                    </div>
                </div>
                <!-- SIDEBAR -->
                <!-- CONTENT -->
                <div class="col-md-9">
               
             
                    <div class="tab-content" id="pageTabs">

                        <div class="tab-pane fade show active" id="page-tab-1" role="tabpanel">
                            <div class="col-md-12 border-bottom mb-3">
                                <h2 class="heading">{!!  $product->title1 !!}</h2>
                            </div>

                            <div class="col-md-12">
                                {!!$product->description1 !!}
                            </div>
                        </div>
                        <div class="tab-pane fade" id="page-tab-2" role="tabpanel">
                            <div class="col-md-12">
                                <h2 class="heading">{!!  $product->title2 !!}</h2>
                            </div>
                            <div class="col-md-12">
                                {!!$product->description2 !!}
                            </div>
                        </div>
                        <div class="tab-pane fade" id="page-tab-3" role="tabpanel">
                            <div class="col-md-12">
                                <h2 class="heading">{!!  $product->title3 !!}</h2>
                            </div>
                            <div class="col-md-12">
                                {!!$product->description3 !!}
                            </div>
                        </div>
                        <div class="tab-pane fade" id="page-tab-4" role="tabpanel">
                            <div class="col-md-12">
                                <h2 class="heading">{!!  $product->title4 !!}</h2>
                            </div>
                            <div class="col-md-12">
                                {!!$product->description4 !!}
                            </div>
                        </div>
                        <div class="tab-pane fade" id="page-tab-5" role="tabpanel">
                            <div class="col-md-12">
                                <h2 class="heading">{!!  $product->title5 !!}</h2>
                            </div>
                            <div class="col-md-12">
                                {!!$product->description5 !!}
                            </div>
                        </div>
                        <div class="tab-pane fade" id="page-tab-6" role="tabpanel">
                            <div class="col-md-12">
                                <h2 class="heading">{!!  $product->title6 !!}</h2>
                            </div>
                            <div class="col-md-12">
                                {!!$product->description6 !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- CONTENT -->
            </div>
        </div>
    </div>

    <!-- MAIN CONTENT CONTAINER -->



@endsection

