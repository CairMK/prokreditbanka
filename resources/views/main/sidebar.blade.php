@if (\Request::is('news'))
    <div class="sidebar-bottom mt-3">
        <ul class="list-unstyled">
            <li class="sidebar-button location"><a
                        href="/contact">{{trans('menu.APPLY_ON_LINE')}}</a>
            </li>
            <li class="sidebar-button pricelist"><a
                        href="">{{trans('menu.price_list_busines')}}</a>
            </li>
            <li class="sidebar-button pricelist"><a
                        href="">{{trans('menu.price_list_private')}}</a>
            </li>
            {{--<li class="sidebar-button flat-rate"><a--}}
            {{--href="">{{trans('menu.exchange_rate')}}</a>--}}
            {{--</li>--}}
        </ul>
    </div>
@elseif (\Request::is('accounts-and-payments'))
    <div class="sidebar-bottom mt-3">
        <ul class="list-unstyled">
            <li class="sidebar-button location"><a
                        href="/contact">{{trans('menu.APPLY_ON_LINE')}}</a>
            </li>
            <li class="sidebar-button pricelist"><a
                        href="">{{trans('menu.price_list_private')}}</a>
            </li>
            {{--<li class="sidebar-button flat-rate"><a--}}
            {{--href="">{{trans('menu.exchange_rate')}}</a>--}}
            {{--</li>--}}
            <li class="sidebar-button flat-rate"><a
                        href="/faq">{{trans('menu.faq')}}</a>
            </li>
        </ul>
    </div>
@elseif (\Request::is('contactinfo'))
    <div class="sidebar-bottom mt-3">
        <ul class="list-unstyled">
            <li class="sidebar-button location"><a
                        href="/contact">{{trans('menu.APPLY_ON_LINE')}}</a>
            </li>
            <li class="sidebar-button pricelist"><a
                        href="">{{trans('menu.price_list_busines')}}</a>
            </li>
            <li class="sidebar-button pricelist"><a
                        href="">{{trans('menu.price_list_private')}}</a>
            </li>
            {{--<li class="sidebar-button flat-rate"><a--}}
            {{--href="">{{trans('menu.exchange_rate')}}</a>--}}
            {{--</li>--}}
            <li class="sidebar-button flat-rate"><a
                        href="/faq">{{trans('menu.faq')}}</a>
            </li>
        </ul>
    </div>
@else
    <div class="sidebar-bottom mt-3">
        <ul class="list-unstyled">
            <li class="sidebar-button location"><a
                        href="/contact">{{trans('menu.APPLY_ON_LINE')}}</a>
            </li>
            <li class="sidebar-button pricelist"><a
                        href="">{{trans('menu.price_list_busines')}}</a>
            </li>
            <li class="sidebar-button pricelist"><a
                        href="">{{trans('menu.price_list_private')}}</a>
            </li>
            {{--<li class="sidebar-button flat-rate"><a--}}
            {{--href="">{{trans('menu.exchange_rate')}}</a>--}}
            {{--</li>--}}
            <li class="sidebar-button flat-rate"><a
                        href="/faq">{{trans('menu.faq')}}</a>
            </li>
        </ul>
    </div>
@endif