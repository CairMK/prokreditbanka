@extends('layouts.main')
@include('layouts.menu')

@section('content')
    <div class="container">
        <div class="row mx-0">
            <div class="col-4">
                <div class="m-2 bg-light box">
                <p><a class="text-uppercase font-weight-bold" href="/categories/tekovni">{{trans('menu.ongoing_tenders')}}</a></p>
            </div>
            </div>
            <div class="col-4 ">
                <div class="m-2 bg-light box">
                <p><a class="text-uppercase font-weight-bold" href="/categories/predkhodni">{{trans('menu.previos_tenders')}}</a></p>
            </div>
            </div>
            <div class="col-4">
                <div class="m-2 bg-light box">
                <a class="text-uppercase font-weight-bold" href="/categories/odluki">{{trans('menu.tender_decisions')}}</a></p>
            </div>
            </div>
        </div>
    </div>
@endsection