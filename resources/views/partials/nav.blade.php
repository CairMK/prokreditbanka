<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        {{--<a class="navbar-brand" href="{{ url('/') }}">--}}
        {{--{!! config('app.name', trans('titles.app')) !!}--}}
        {{--</a>--}}
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            <span class="sr-only">{!! trans('titles.toggleNav') !!}</span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            {{-- Left Side Of Navbar --}}
            <ul class="navbar-nav mr-auto">
                @role('admin')
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {!! trans('titles.adminDropdownNav') !!}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item {{ Request::is('users', 'users/' . Auth::user()->id, 'users/' . Auth::user()->id . '/edit') ? 'active' : null }}"
                           href="{{ url('/users') }}">
                            @lang('titles.adminUserList')
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item {{ Request::is('users/create') ? 'active' : null }}"
                           href="{{ url('/users/create') }}">
                            @lang('titles.adminNewUser')
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item {{ Request::is('themes','themes/create') ? 'active' : null }}"
                           href="{{ url('/themes') }}">
                            @lang('titles.adminThemesList')
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item {{ Request::is('logs') ? 'active' : null }}" href="{{ url('/logs') }}">
                            @lang('titles.adminLogs')
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item {{ Request::is('activity') ? 'active' : null }}"
                           href="{{ url('/activity') }}">
                            @lang('titles.adminActivity')
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item {{ Request::is('phpinfo') ? 'active' : null }}"
                           href="{{ url('/phpinfo') }}">
                            @lang('titles.adminPHP')
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item {{ Request::is('routes') ? 'active' : null }}"
                           href="{{ url('/routes') }}">
                            @lang('titles.adminRoutes')
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item {{ Request::is('active-users') ? 'active' : null }}"
                           href="{{ url('/active-users') }}">
                            @lang('titles.activeUsers')
                        </a>
                    </div>
                </li>
            </ul>
            {{-- Right Side Of Navbar --}}
            <ul class="navbar-nav ml-auto">
                {{-- Authentication Links --}}
                @guest
                    <li><a class="nav-link" href="{{ route('login') }}">{{ trans('titles.login') }}</a></li>
                    <li><a class="nav-link" href="{{ route('register') }}">{{ trans('titles.register') }}</a></li>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            @if ((Auth::User()->profile) && Auth::user()->profile->avatar_status == 1)
                                <img src="{{ Auth::user()->profile->avatar }}" alt="{{ Auth::user()->name }}"
                                     class="user-avatar-nav">
                            @else
                                <div class="user-avatar-nav"></div>
                            @endif
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item {{ Request::is('profile/'.Auth::user()->name, 'profile/'.Auth::user()->name . '/edit') ? 'active' : null }}"
                               href="{{ url('/profile/'.Auth::user()->name) }}">
                                @lang('titles.profile')
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest

            </ul>
        </div>
    </div>
</nav>
<div class="row mx-0">
    <div class="left-sidebar col-lg-2">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <li><a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-tachometer"></i><span
                                    class="hide-menu">Dashboard </span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="/admin/settings">Подесувања</a></li>
                            <li><a href="/admin/analytics">Analytics </a></li>
                        </ul>
                    </li>
                    <li><a class="categories" href="/admin/categories" aria-expanded="false"><i
                                    class="fa fa-bars"></i><span class="hide-menu">Категории</span></a>
                    </li>
                    <li><a class="categories" href="/admin/slider" aria-expanded="false"><i
                                    class="fa fa-file-image-o"></i><span class="hide-menu">Слајдер</span></a>
                    </li>
                    <li><a class="categories" href="/admin/product" aria-expanded="false"><i class="fa fa fa-cubes"></i><span
                                    class="hide-menu">Тендери и продажба на имот</span></a></li>
                    <li><a class="categories" href="/admin/news" aria-expanded="false"><i
                                    class="fa fa fa-cubes"></i><span
                                    class="hide-menu">Новости</span></a></li>
                    <li><a class="categories" href="/admin/kurs" aria-expanded="false"><i
                                    class="fa fa-money"></i><span
                                    class="hide-menu">Курсна листа</span></a></li>
                    <li><a class="categories" href="/admin/atm" aria-expanded="false"><i
                                    class="fa fa-credit-card"></i><span class="hide-menu">FAQ</span></a></li>
                    <li><a class="categories" href="/admin/bankomat" aria-expanded="false"><i
                                    class="fa fa-cc-visa"></i><span class="hide-menu">Банкомати и експозитури</span></a>
                    </li>
                    <li><a class="categories" href="/admin/popup" aria-expanded="false"><i
                                    class="fa fa-credit-card"></i><span class="hide-menu">PopUp</span></a></li>
                    <li><a class="categories" href="/admin/staticpage" aria-expanded="false"><i
                                    class="fa fa-newspaper-o"></i><span class="hide-menu">Страни</span></a>
                    </li>
                    <li><a class="categories" href="/admin/services" aria-expanded="false"><i
                                    class="fa fa-server"></i><span class="hide-menu">Услуги</span></a>
                    </li>
                    <li><a class="categories" href="/admin/referrals" aria-expanded="false"><i
                                    class="fa fa-address-card-o"></i><span class="hide-menu">Референти</span></a>
                    </li>
                </ul>
            </nav>
            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
    </div>
    @endrole


