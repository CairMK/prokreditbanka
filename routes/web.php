<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
| Middleware options can be located in `app/Http/Kernel.php`
|
*/

// Homepage Route

// Authentication Routes
Auth::routes();

// Public Routes
Route::group(['middleware' => ['web', 'activity']], function () {

    // Activation Routes
    Route::get('/activate', ['as' => 'activate', 'uses' => 'Auth\ActivateController@initial']);

    Route::get('/activate/{token}', ['as' => 'authenticated.activate', 'uses' => 'Auth\ActivateController@activate']);
    Route::get('/activation', ['as' => 'authenticated.activation-resend', 'uses' => 'Auth\ActivateController@resend']);
    Route::get('/exceeded', ['as' => 'exceeded', 'uses' => 'Auth\ActivateController@exceeded']);

    // Socialite Register Routes
    Route::get('/social/redirect/{provider}', ['as' => 'social.redirect', 'uses' => 'Auth\SocialController@getSocialRedirect']);
    Route::get('/social/handle/{provider}', ['as' => 'social.handle', 'uses' => 'Auth\SocialController@getSocialHandle']);

    // Route to for user to reactivate their user deleted account.
    Route::get('/re-activate/{token}', ['as' => 'user.reactivate', 'uses' => 'RestoreUserController@userReActivate']);
});

// Registered and Activated User Routes
Route::group(['middleware' => ['auth', 'activated', 'activity']], function () {

    // Activation Routes
    Route::get('/activation-required', ['uses' => 'Auth\ActivateController@activationRequired'])->name('activation-required');
    Route::get('/logout', ['uses' => 'Auth\LoginController@logout'])->name('logout');
});

// Registered and Activated User Routes
Route::group(['middleware' => ['auth', 'activated', 'activity', 'twostep']], function () {

    //  Homepage Route - Redirect based on user role is in controller.
    Route::get('/home', ['as' => 'public.home', 'uses' => 'UserController@index']);

    // Show users profile - viewable by other users.
    Route::get('profile/{username}', [
        'as' => '{username}',
        'uses' => 'ProfilesController@show',
    ]);
});

// Registered, activated, and is current user routes.
Route::group(['middleware' => ['auth', 'activated', 'currentUser', 'activity', 'twostep']], function () {

    // User Profile and Account Routes
    Route::resource(
        'profile',
        'ProfilesController', [
            'only' => [
                'show',
                'edit',
                'update',
                'create',
            ],
        ]
    );
    Route::put('profile/{username}/updateUserAccount', [
        'as' => '{username}',
        'uses' => 'ProfilesController@updateUserAccount',
    ]);
    Route::put('profile/{username}/updateUserPassword', [
        'as' => '{username}',
        'uses' => 'ProfilesController@updateUserPassword',
    ]);
    Route::delete('profile/{username}/deleteUserAccount', [
        'as' => '{username}',
        'uses' => 'ProfilesController@deleteUserAccount',
    ]);

    // Route to show user avatar
    Route::get('images/profile/{id}/avatar/{image}', [
        'uses' => 'ProfilesController@userProfileAvatar',
    ]);

    // Route to upload user avatar.
    Route::post('avatar/upload', ['as' => 'avatar.upload', 'uses' => 'ProfilesController@upload']);
});

// Registered, activated, and is admin routes.
Route::group(['middleware' => ['auth', 'activated', 'role:admin', 'activity', 'twostep']], function () {
    Route::resource('/users/deleted', 'SoftDeletesController', [
        'only' => [
            'index', 'show', 'update', 'destroy',
        ],
    ]);

    Route::resource('users', 'UsersManagementController', [
        'names' => [
            'index' => 'users',
            'destroy' => 'user.destroy',
        ],
        'except' => [
            'deleted',
        ],
    ]);
    Route::post('search-users', 'UsersManagementController@search')->name('search-users');

    Route::resource('themes', 'ThemesManagementController', [
        'names' => [
            'index' => 'themes',
            'destroy' => 'themes.destroy',
        ],
    ]);

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::get('routes', 'AdminDetailsController@listRoutes');
    Route::get('active-users', 'AdminDetailsController@activeUsers');
});

Route::redirect('/php', '/phpinfo', 301);

Route::group(['middleware' => ['auth', 'activated', 'role:admin', 'activity', 'twostep']], function () {

    Route::get('/admin/sliders/{id}/category', 'CategoriesController@addslider')->name('admin.addcategoryslider.index');
    Route::post('/admin/addcategorystore', 'CategoriesController@sliderstore')->name('admin.slidercategory.store');
    Route::post('/admin/addcategorydestroy', 'CategoriesController@sliderdestroy')->name('admin.slidercategory.destroy');
    Route::resource('/admin/sliders/{id}/product', 'SlidersController', ['names' => [
        'index' => 'admin.sliders.index',
        'store' => 'admin.sliders.store',
        'destroy' => 'admin.sliders.destroy',

    ]]);

    Route::resource('/admin/sliders', 'SlidersController', ['names' => [
        'index' => 'admin.sliders.index',
        'store' => 'admin.sliders.store',
        'destroy' => 'admin.sliders.destroy',

    ]]);
    Route::resource('/admin/settings', 'HomeController', ['names' => [
        'index' => 'admin.settings.index',
        'create' => 'admin.settings.create',
        'store' => 'admin.settings.store',
        'edit' => 'admin.settings.edit',
        'update' => 'admin.settings.update',
        'destroy' => 'admin.settings.destroy',

    ]]);
    Route::resource('/admin/product', 'ProductsController', ['names' => [
        'index' => 'admin.product.index',
        'create' => 'admin.product.create',
        'store' => 'admin.product.store',
        'edit' => 'admin.product.edit',
        'update' => 'admin.product.update',
        'destroy' => 'admin.product.destroy',

    ]]);
    Route::resource('/admin/slider', 'SliderController', ['names' => [
        'index' => 'admin.slider.index',
        'create' => 'admin.slider.create',
        'store' => 'admin.slider.store',
        'edit' => 'admin.slider.edit',
        'update' => 'admin.slider.update',
        'destroy' => 'admin.slider.destroy',

    ]]);
    Route::resource('/admin/news', 'NewsController', ['names' => [
        'index' => 'admin.news.index',
        'create' => 'admin.news.create',
        'store' => 'admin.news.store',
        'edit' => 'admin.news.edit',
        'update' => 'admin.news.update',
        'destroy' => 'admin.news.destroy',

    ]]);
    Route::resource('/admin/kurs', 'KursController', ['names' => [
        'index' => 'admin.kurs.index',
        'create' => 'admin.kurs.create',
        'store' => 'admin.kurs.store',
        'edit' => 'admin.kurs.edit',
        'update' => 'admin.kurs.update',
        'destroy' => 'admin.kurs.destroy',

    ]]);
    Route::resource('/admin/categories', 'CategoriesController', ['names' => [
        'index' => 'admin.categories.index',
        'create' => 'admin.categories.create',
        'store' => 'admin.categories.store',
        'edit' => 'admin.categories.edit',
        'update' => 'admin.categories.update',
        'destroy' => 'admin.categories.destroy',

    ]]);
    Route::resource('/admin/staticpage', 'StaticController', ['names' => [
        'index' => 'admin.staticpage.index',
        'create' => 'admin.staticpage.create',
        'store' => 'admin.staticpage.store',
        'edit' => 'admin.staticpage.edit',
        'update' => 'admin.staticpage.update',
        'destroy' => 'admin.staticpage.destroy',
    ]]);
    Route::resource('/admin/referrals', 'ReferralController', ['names' => [
        'index' => 'admin.referrals.index',
        'create' => 'admin.referrals.create',
        'store' => 'admin.referrals.store',
        'edit' => 'admin.referrals.edit',
        'update' => 'admin.referrals.update',
        'destroy' => 'admin.referrals.destroy',
    ]]);
    Route::resource('/admin/services', 'ServicesController', ['names' => [
        'index' => 'admin.services.index',
        'create' => 'admin.services.create',
        'store' => 'admin.services.store',
        'edit' => 'admin.services.edit',
        'update' => 'admin.services.update',
        'destroy' => 'admin.services.destroy',
    ]]);
    Route::resource('/admin/atm', 'AtmController', ['names' => [
        'index' => 'admin.atm.index',
        'create' => 'admin.atm.create',
        'store' => 'admin.atm.store',
        'edit' => 'admin.atm.edit',
        'update' => 'admin.atm.update',
        'destroy' => 'admin.atm.destroy',

    ]]);
    Route::resource('/admin/popup', 'PopUpController', ['names' => [
        'index' => 'admin.popup.index',
        'create' => 'admin.popup.create',
        'store' => 'admin.popup.store',
        'edit' => 'admin.popup.edit',
        'update' => 'admin.popup.update',
        'destroy' => 'admin.popup.destroy',

    ]]);
    Route::resource('/admin/bankomat', 'BankomatController', ['names' => [
        'index' => 'admin.bankomat.index',
        'create' => 'admin.bankomat.create',
        'store' => 'admin.bankomat.store',
        'edit' => 'admin.bankomat.edit',
        'update' => 'admin.bankomat.update',
        'destroy' => 'admin.bankomat.destroy',

    ]]);

    Route::get('/admin/analytics', 'HomeController@all');


});
// Homepage Route
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {
        $lang = LaravelLocalization::getCurrentLocale();
        Session::put('locale', $lang);
        Route::get('/', 'HomePageController@index');
        Route::get('/{slug}', 'HomePageController@staticpages');
        Route::get('/referents/{slug}', 'HomePageController@referents');
        Route::get('/services/{slug}', 'HomePageController@services');
        Route::get('/product/{slug}', 'HomePageController@product');
        Route::get('/categories/{slug}', 'HomePageController@categories');
    });

