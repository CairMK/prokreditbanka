<?php $__env->startSection('content'); ?>
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <p><a class="btn btn-labeled" href="/admin/product/create"> <i class="btn-label fa fa-plus"></i>Add
                        product </a></p>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Products</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">

                                <tbody>
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td>
                                            <div class="round-img">
                                                <a href="/admin/product/<?php echo e($product->id); ?>/edit"><img
                                                            src="/assets/img/products/thumbnails/<?php echo e($product->image); ?>"
                                                            alt=""></a>
                                            </div>
                                        </td>
                                        <td><?php echo e($product->title); ?></td>
                                        <td><span><a href="/admin/sliders/<?php echo e($product->id); ?>/product"
                                                     class="btn btn-info">Додади галерија</a></span></td>
                                        <td><span>Слики во слајдер: <?php echo e($product->images()->count()); ?></span></td>
                                        
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 right">
                <?php echo $products->links(); ?>

            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>