<?php echo $__env->make('layouts.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('content'); ?>
    <!-- SUB HEADER -->
    <!-- MAIN CONTENT -->
    <!-- livezilla.net PLACE SOMEWHERE IN BODY -->
    <script type="text/javascript" id="4d4da15fb048f927ef0a5be28c66af5a"
            src="http://prokredit.dadigitalpresent.mk/livezilla/script.php?id=4d4da15fb048f927ef0a5be28c66af5a"
            defer></script>
    <!-- livezilla.net PLACE SOMEWHERE IN BODY -->
    <div class="container" id="homepage">
        <div class="row px-0 mx-0">
            <div class=" col-md-4  col-lg-3 px-0 order-md-1 order-2">
                <ul class="contact-list">
                    <li>
                        <a href="javascript:void(open('http://prokredit.dadigitalpresent.mk/livezilla/chat.php?v=2&linkid=NGQ0ZGExNWZiMDQ4ZjkyN2VmMGE1YmUyOGM2NmFmNWE_','','width=400,height=600,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'))"><i
                                    class="chat-icon"></i>
                            <h1 class="frontpage"><?php echo e(trans('menu.chat_now')); ?></h1></a></li>
                    <li><a href="/contactinfo"><i class="contact-icon"></i>
                            <h1 class="frontpage"><?php echo e(trans('menu.contact_info')); ?></h1></a></li>
                    <li><a href="/contact"><i class="appointment-icon"></i>
                            <h1 class="frontpage"><?php echo e(trans('menu.APPLY_ON_LINE')); ?></h1></a></li>
                </ul>
            </div>
            <div class="col-md-8 col-lg-9 px-0 order-md-2 order-1">
                <!-- SLIDER -->
                <section id="main-slider" class="lazy slider" data-sizes="50vw">
                    <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sliders): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="single-slide">
                            <a href="<?php echo e($sliders->link); ?>"><img class="img-fluid"
                                                                src="/assets/img/slider/<?php echo e($sliders->image); ?>"
                                                                alt="image"></a>
                            <?php if( ! empty($sliders['title'])): ?>
                                <div class="slide-content">
                                    <a href="<?php echo e($sliders->slug); ?>"><h2
                                                class="small-heading"><?php echo e(str_limit(strip_tags($sliders->title),22, $end = '...' )); ?></h2>
                                    </a>
                                    <h2 class="heading"><?php echo e(str_limit(strip_tags($sliders->description),22, $end = '...' )); ?></h2>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </section>
                <!-- SLIDER -->
            </div>
        </div>
    </div>
    <!-- MAIN CONTENT -->
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>