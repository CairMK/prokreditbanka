<?php $__env->startSection('content'); ?>
    <div class="col-lg-10">
        <div class="row">
            <div class="col-md-12">

                <?php if(Session::has('flash_message')): ?>
                    <div class="alert alert-success">
                        <?php echo e(Session::get('flash_message')); ?>

                    </div>
                <?php endif; ?>
                <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <p><a class="btn btn-labeled " href="/admin/settings/<?php echo e($setting->id); ?>/edit"> <i
                                    class="btn-label fa fa-plus"></i>Подесувања </a></p>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
        <div class="row"> <!-- /# card1 -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Главна адреса</p>
                    </div>
                    <hr/>
                    <div class="media">

                        <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($setting->mainurl); ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>

                </div>

            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Наслов</p>
                    </div>
                    <hr/>
                    <div class="media">
                        <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($setting->title); ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>

                </div>

            </div>
        </div> <!-- /# card1 -->

        <div class="row"> <!-- /# card2 -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Email адреса</p>
                    </div>
                    <hr/>
                    <div class="media">

                        <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($setting->email); ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>

                </div>
                <!-- /# card -->
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Адреса</p>
                    </div>
                    <hr/>
                    <div class="media">
                        <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($setting->address); ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>

                </div>
            </div>
        </div> <!-- /# card2 -->
        <div class="row"> <!-- /# card3 -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Logo</p>
                    </div>
                    <hr/>
                    <div class="media">

                        <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <img src="/assets/img/logo/<?php echo e($setting->logo); ?>" class="img-responsive"/>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                </div>

            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Опис</p>
                    </div>
                    <hr/>
                    <div class="media">
                        <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo $setting->description; ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                </div>

            </div>
        </div> <!-- /# card3 -->
        <div class="row"> <!-- /# card4 -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Телефон</p>
                    </div>
                    <hr/>
                    <div class="media">
                        <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($setting->phone); ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                </div>

            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Facebook</p>
                    </div>
                    <hr/>
                    <div class="media">
                        <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($setting->facebook != NULL): ?>
                                <?php echo e($setting->facebook); ?>


                            <?php else: ?>
                                n/a
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                </div>

            </div>
        </div> <!-- /# card4 -->

        <style>
            #map {
                height: 400px;
                width: 100%;
            }
        </style>

        <div id="map"></div>
        <script>
            <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            function initMap() {
                var uluru = {lat: <?php echo e($setting->lat); ?>, lng: <?php echo e($setting->lng); ?> };
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 4,
                    center: uluru
                });
                var marker = new google.maps.Marker({
                    position: uluru,
                    map: map
                });
            }
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7va0UZbX2kdZmcLseEqlADHNmUf9YOGs&callback=initMap">
        </script>

        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Twitter</p>
                    </div>
                    <hr/>
                    <div class="media">
                        <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($setting->twitter != NULL): ?>
                                <?php echo e($setting->twitter); ?>


                            <?php else: ?>
                                n/a
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                </div>

            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Skype</p>
                    </div>
                    <hr/>
                    <div class="media">
                        <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($setting->skype != NULL): ?>
                                <?php echo e($setting->skype); ?>


                            <?php else: ?>
                                n/a
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>G-plus</p>
                    </div>
                    <hr/>
                    <div class="media">
                        <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($setting->gplus != NULL): ?>
                                <?php echo e($setting->gplus); ?>


                            <?php else: ?>
                                n/a
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                </div>

            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Youtube</p>
                    </div>
                    <hr/>
                    <div class="media">
                        <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($setting->youtube != NULL): ?>
                                <?php echo e($setting->youtube); ?>


                            <?php else: ?>
                                n/a
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Flicker</p>
                    </div>
                    <hr/>
                    <div class="media">
                        <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($setting->flicker != NULL): ?>
                                <?php echo e($setting->flicker); ?>


                            <?php else: ?>
                                n/a
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                </div>

            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-title">
                        <p>Pinterest</p>
                    </div>
                    <hr/>
                    <div class="media">
                        <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($setting->pinterest != NULL): ?>
                                <?php echo e($setting->pinterest); ?>


                            <?php else: ?>
                                n/a
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <p>Последни промени</p>
                    </div>
                    <hr/>
                    <div class="media">
                        <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($setting->updated_at->diffForHumans()); ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                </div>

            </div>

        </div>
    </div>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>