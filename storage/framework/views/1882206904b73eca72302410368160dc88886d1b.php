<?php $__env->startSection('content'); ?>

    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                <?php if(Session::has('flash_message')): ?>
                    <div class="alert alert-success">
                        <?php echo e(Session::get('flash_message')); ?>

                    </div>
                <?php endif; ?>

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Додади слика во слајдер</span>
                    </div>
                    <br/>
                    <div class="widget-body">
                        <div id="horizontal-form">

                            <?php echo e(Form::model('slider', array('route' => array('admin.slider.store'), 'method' => 'POST', 'files'=>true))); ?>

                            <?php echo csrf_field(); ?>



                            <div class="input-group<?php echo e($errors->has('image') ? ' has-error' : ''); ?>">
                         <span class="input-group-btn">
                            <span>
                              Избери слика... <input type="file" name="image">
                          </span>
                      </span>
                            </div>
                            <br/>
                            <?php if($errors->has('image')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('image')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label for="title">Наслов</label>
                                <input type="text" name="title" class="form-control">
                            </div>
                            <?php if($errors->has('title')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('title')); ?></p> <?php endif; ?>


                            <div class="form-group">
                                <label for="link">Link</label>
                                <input type="text" id="link" name="link" class="form-control"></input>
                            </div>
                            <?php if($errors->has('link')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('link')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label for="description">Опис</label>
                                <textarea class="ckeditor" id="elm3" name="description"></textarea>
                            </div>
                            <?php if($errors->has('description')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('description')); ?></p> <?php endif; ?>
                            <div class="form-group">
                                <label for="user">Уредник</label>
                                <select name="user_id" id="user" class="form-control">
                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($user->id); ?>"
                                                <?php if(Auth::user()->id == $user->id): ?> selected <?php endif; ?> ><?php echo e($user->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>
                            </div>


                            <div class="form-group">
                                <p>Workflow: </p>
                                <?php $__currentLoopData = $workflows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $workflow): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="workflow_id"
                                                   value="<?php echo e($workflow->id); ?>" <?php if($workflow->id  == 1): ?> checked <?php endif; ?>>
                                        </label>
                                        <span class="text"> <?php echo e($workflow->name); ?></span>
                                    </div>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>


                            <!-- Hidden inputs -->

                            <input type="hidden" name="creator" value="<?php echo e(Auth::user()->id); ?>">
                            <input type="hidden" id="lat" class="form-control" name="lat">
                            <input type="hidden" id="lng" class="form-control" name="lng">

                            <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                                        class="btn-label fa fa-plus"></i> Create
                            </button>
                            <?php echo Form::close(); ?>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>