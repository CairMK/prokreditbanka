<?php if(\Request::is('news')): ?>
    <div class="sidebar-bottom mt-3">
        <ul class="list-unstyled">
            <li class="sidebar-button location"><a
                        href="/contact"><?php echo e(trans('menu.APPLY_ON_LINE')); ?></a>
            </li>
            <li class="sidebar-button pricelist"><a
                        href=""><?php echo e(trans('menu.price_list_busines')); ?></a>
            </li>
            <li class="sidebar-button pricelist"><a
                        href=""><?php echo e(trans('menu.price_list_private')); ?></a>
            </li>
            
            
            
        </ul>
    </div>
<?php elseif(\Request::is('accounts-and-payments')): ?>
    <div class="sidebar-bottom mt-3">
        <ul class="list-unstyled">
            <li class="sidebar-button location"><a
                        href="/contact"><?php echo e(trans('menu.APPLY_ON_LINE')); ?></a>
            </li>
            <li class="sidebar-button pricelist"><a
                        href=""><?php echo e(trans('menu.price_list_private')); ?></a>
            </li>
            
            
            
            <li class="sidebar-button flat-rate"><a
                        href="/faq"><?php echo e(trans('menu.faq')); ?></a>
            </li>
        </ul>
    </div>
<?php elseif(\Request::is('contactinfo')): ?>
    <div class="sidebar-bottom mt-3">
        <ul class="list-unstyled">
            <li class="sidebar-button location"><a
                        href="/contact"><?php echo e(trans('menu.APPLY_ON_LINE')); ?></a>
            </li>
            <li class="sidebar-button pricelist"><a
                        href=""><?php echo e(trans('menu.price_list_busines')); ?></a>
            </li>
            <li class="sidebar-button pricelist"><a
                        href=""><?php echo e(trans('menu.price_list_private')); ?></a>
            </li>
            
            
            
            <li class="sidebar-button flat-rate"><a
                        href="/faq"><?php echo e(trans('menu.faq')); ?></a>
            </li>
        </ul>
    </div>
<?php else: ?>
    <div class="sidebar-bottom mt-3">
        <ul class="list-unstyled">
            <li class="sidebar-button location"><a
                        href="/contact"><?php echo e(trans('menu.APPLY_ON_LINE')); ?></a>
            </li>
            <li class="sidebar-button pricelist"><a
                        href=""><?php echo e(trans('menu.price_list_busines')); ?></a>
            </li>
            <li class="sidebar-button pricelist"><a
                        href=""><?php echo e(trans('menu.price_list_private')); ?></a>
            </li>
            
            
            
            <li class="sidebar-button flat-rate"><a
                        href="/faq"><?php echo e(trans('menu.faq')); ?></a>
            </li>
        </ul>
    </div>
<?php endif; ?>