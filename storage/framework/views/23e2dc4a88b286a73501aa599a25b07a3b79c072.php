<?php $__env->startSection('content'); ?>

    <div class="col-lg-10">
    <div class="row">
        <div class="col-lg-12">
            <?php if(Session::has('flash_message')): ?>
            <div class="alert alert-success">
                <?php echo e(Session::get('flash_message')); ?>

            </div>
            <?php endif; ?>
            <p><a class="btn btn-labeled " href="/admin/categories/create"> <i
                class="btn-label fa fa-plus"></i>Додади категорија </a></p>
            </div>
            <div class="col-lg-12">
                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-themesecondary">
                        <i class="widget-icon fa fa-tags themesecondary"></i>
                        <span class="widget-caption themesecondary">Категории</span>
                    </div><!--Widget Header-->
                    <div class="tree well">
                        <?php echo $list; ?>

                    </div>
                </div>
            </div>
        </div>

        <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>