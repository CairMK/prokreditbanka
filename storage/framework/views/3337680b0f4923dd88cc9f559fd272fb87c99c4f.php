<?php $__env->startSection('content'); ?>
    <div class="col-lg-10">
    <div class="row">
        <div class="col-lg-12">
            <p>  <a class="btn btn-labeled" href="services/create"> <i class="btn-label fa fa-plus"></i>Додади Услуга </a></p>
        </div>
        <div class="col-lg-12">
          <div class="card">
            <div class="card-title">
              <h4>Products</h4>
          </div>
          <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <tbody>
                    <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td>
                          <div class="round-img">
                            <a href="/admin/services/<?php echo e($service->id); ?>/edit"><img src="/assets/img/services/thumbnails/<?php echo e($service->image); ?>" alt=""></a>
                        </div>
                    </td>
                    <td><a href="/admin/services/<?php echo e($service->id); ?>/edit"><?php echo e($service->title); ?></a></td>
                    <td><span>Original time: <?php echo e($service->created_at); ?></span></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>