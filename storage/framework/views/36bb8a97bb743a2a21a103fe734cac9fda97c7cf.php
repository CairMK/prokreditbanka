<?php $__env->startSection('content'); ?>
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-12">
                <p><a class="btn btn-labeled w" href="/admin/news/create"> <i
                                class="btn-label fa fa-plus"></i>Add Static Page </a></p>
                <div class="card">
                    <div class="card-title">
                        <h4>Статични страни</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $newss): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td>
                                            <div class="round-img">
                                                <a href="/admin/news/<?php echo e($newss->id); ?>/edit"> <?php if($newss->image): ?>
                                                        <img src="/assets/img/news/<?php echo e($newss->imagethumb); ?>"
                                                             alt="">
                                                    <?php endif; ?></a>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="/admin/news/<?php echo e($newss->id); ?>/edit"><?php echo e($newss->title); ?></a>
                                        </td>
                                        <td><span>Original time: <?php echo e($newss->created_at); ?></span></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <?php echo $news->links(); ?>

        </div>
    </div>



<?php $__env->stopSection(); ?>





<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>