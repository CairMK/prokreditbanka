<?php $__env->startSection('menu'); ?>
    <!-- HEADER -->
    <?php if(Request::path() == "/index.php" || Request::path() == "/"): ?>
        <div id="header">
            <div class="container px-0">
                <nav class="navbar navbar-expand-lg">
                    <a class="navbar-brand" href="/">ProCredit</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="aboutUs-dropdown"
                                   data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <?php echo e(trans('menu.aboutus')); ?>

                                </a>
                                <div class="dropdown-menu" aria-labelledby="aboutUs-dropdown">
                                    
                                    
                                    
                                    

                                    <a class="dropdown-item" href="/abou-us#page-tab-1"
                                       data-expand="#page-tab-1"><?php echo e(trans('menu.our_mission')); ?></a>
                                    <a class="dropdown-item" href="/abou-us#page-tab-2"
                                       data-expand="#page-tab-2"><?php echo e(trans('menu.shareholders')); ?></a>
                                    <a class="dropdown-item" href="/abou-us#page-tab-3"
                                       data-expand="#page-tab-3"><?php echo e(trans('menu.management')); ?></a>
                                    <a class="dropdown-item"
                                       href="/abou-us#page-tab-4"
                                       data-expand="#page-tab-4"><?php echo e(trans('menu.group_of_responsible_banks')); ?></a>
                                    <a class="dropdown-item"
                                       href="/abou-us#page-tab-5"
                                       data-expand="#page-tab-5"><?php echo e(trans('menu.business_ethics')); ?></a>
                                    <a class="dropdown-item"
                                       href="/the-banks-capital#page-tab-1"><?php echo e(trans('menu.risk_Management')); ?></a>
                                    <a class="dropdown-item"
                                       href="/the-banks-capital#page-tab-2"><?php echo e(trans('menu.Annual_report')); ?></a>
                                    <a class="dropdown-item"
                                       href="/the-banks-capital#page-tab-3"><?php echo e(trans('menu.Corporate_information_according_to_NBRM')); ?></a>
                                    <a class="dropdown-item"
                                       href="/the-banks-capital#page-tab-4"><?php echo e(trans('menu.the_bank’s_capital')); ?></a>
                                </div>
                            </li>
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            <li class="nav-item">
                                <a class="nav-link"
                                   href="https://hr.procredit-group.com/mk?languagecode=en"><?php echo e(trans('menu.OUR_STAFF')); ?></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="eco-dropdown" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <?php echo e(trans('menu.ECO')); ?>

                                </a>
                                <div class="dropdown-menu" aria-labelledby="eco-dropdown">
                                    <a class="dropdown-item"
                                       href="/our-eco-strategy"><?php echo e(trans('menu.OUR_ECO_STRATEGY')); ?></a>
                                    <a class="dropdown-item" href="/eco"><?php echo e(trans('menu.eco_loans')); ?></a>
                                </div>
                            </li>
                            <li class="nav-item e-banking-button">
                                <a class="nav-link btn btn-primary"
                                   href="https://ebank.pcb.com.mk/User/LogOn"><?php echo e(trans('menu.e-banking')); ?></a>

                            </li>

                        </ul>
                    </div>
                </nav>
                <div class="language-switcher">
                    <ul class="list-unstyled d-flex" style="text-transform: uppercase;">

                        <?php $__currentLoopData = LaravelLocalization::getSupportedLocales(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $localeCode => $properties): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li>
                                <a rel="alternate" hreflang="<?php echo e($localeCode); ?>"
                                   href="<?php echo e(LaravelLocalization::getLocalizedURL($localeCode, null, [], true)); ?>">
                                    <?php echo e($localeCode); ?>

                                </a>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </ul>

                </div>
            </div>
        </div>
        <!-- HEADER -->
    <?php else: ?>
        <div id="header">
            <div class="container px-0">
                <nav class="navbar navbar-expand-lg">
                    <a class="navbar-brand" href="/">ProCredit</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="aboutUs-dropdown"
                                   data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <?php echo e(trans('menu.aboutus')); ?>

                                </a>
                                <div class="dropdown-menu" aria-labelledby="aboutUs-dropdown">
                                    
                                    
                                    
                                    

                                    <a class="dropdown-item" href="/abou-us#page-tab-1"
                                       data-expand="#page-tab-1"><?php echo e(trans('menu.our_mission')); ?></a>
                                    <a class="dropdown-item" href="/abou-us#page-tab-2"
                                       data-expand="#page-tab-2"><?php echo e(trans('menu.shareholders')); ?></a>
                                    <a class="dropdown-item" href="/abou-us#page-tab-3"
                                       data-expand="#page-tab-3"><?php echo e(trans('menu.management')); ?></a>
                                    <a class="dropdown-item"
                                       href="/abou-us#page-tab-4"
                                       data-expand="#page-tab-4"><?php echo e(trans('menu.group_of_responsible_banks')); ?></a>
                                    <a class="dropdown-item"
                                       href="/abou-us#page-tab-5"
                                       data-expand="#page-tab-5"><?php echo e(trans('menu.business_ethics')); ?></a>
                                    <a class="dropdown-item"
                                       href="/the-banks-capital#page-tab-1"><?php echo e(trans('menu.risk_Management')); ?></a>
                                    <a class="dropdown-item"
                                       href="/the-banks-capital#page-tab-2"><?php echo e(trans('menu.Annual_report')); ?></a>
                                    <a class="dropdown-item"
                                       href="/the-banks-capital#page-tab-3"><?php echo e(trans('menu.Corporate_information_according_to_NBRM')); ?></a>
                                    <a class="dropdown-item"
                                       href="/the-banks-capital#page-tab-4"><?php echo e(trans('menu.the_bank’s_capital')); ?></a>
                                </div>
                            </li>
                            <li class="nav-item dynamic-element ">
                                <a class="nav-link" target="_blank"
                                   href="https://www.procreditbank-direct.com/macedonia/mk"><?php echo e(trans('menu.private_clients')); ?></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="aboutUs-dropdown"
                                   data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <?php echo e(trans('menu.business_clients')); ?>

                                </a>
                                <div class="dropdown-menu" aria-labelledby="businessClient-dropdown1">
                                    <a class="dropdown-item"
                                       href="/accounts-and-payments"><?php echo e(trans('menu.accounts')); ?></a>
                                    <a class="dropdown-item" href="/credit-offers"><?php echo e(trans('menu.credit_line')); ?></a>
                                    <a class="dropdown-item"
                                       href="/documentary-operations"><?php echo e(trans('menu.documentary_operations')); ?></a>
                                    <a class="dropdown-item"
                                       href="/term-deposits"><?php echo e(trans('menu.deposits_and_state_bonds')); ?></a>
                                    <a class="dropdown-item"
                                       href="/services-for-non-resident-legal-entities"><?php echo e(trans('menu.services_for_non_resident_legal_entities')); ?></a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"
                                   href="https://hr.procredit-group.com/mk?languagecode=en"><?php echo e(trans('menu.OUR_STAFF')); ?></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="eco-dropdown" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <?php echo e(trans('menu.ECO')); ?>

                                </a>
                                <div class="dropdown-menu" aria-labelledby="eco-dropdown">
                                    <a class="dropdown-item"
                                       href="/our-eco-strategy"><?php echo e(trans('menu.OUR_ECO_STRATEGY')); ?></a>
                                    <a class="dropdown-item" href="/eco"><?php echo e(trans('menu.eco_loans')); ?></a>
                                </div>
                            </li>
                            <li class="nav-item e-banking-button">
                                <a class="nav-link btn btn-primary"
                                   href="https://ebank.pcb.com.mk/User/LogOn"><?php echo e(trans('menu.e-banking')); ?></a>

                            </li>

                        </ul>
                    </div>
                </nav>
                <div class="language-switcher">
                    <ul class="list-unstyled d-flex" style="text-transform: uppercase;">

                        <?php $__currentLoopData = LaravelLocalization::getSupportedLocales(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $localeCode => $properties): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li>
                                <a rel="alternate" hreflang="<?php echo e($localeCode); ?>"
                                   href="<?php echo e(LaravelLocalization::getLocalizedURL($localeCode, null, [], true)); ?>">
                                    <?php echo e($localeCode); ?>

                                </a>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </ul>

                </div>
            </div>
        </div>
        <!-- HEADER -->
    <?php endif; ?>
<?php $__env->stopSection(); ?>