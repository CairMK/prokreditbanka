<?php $__env->startSection('content'); ?>
    <div class="col-lg-10">
    <div class="row">
        <div class="col-lg-12">
            <p><a class="btn btn-labeled" href="/admin/slider/create"> <i
                class="btn-label fa fa-plus"></i>Додади слика во слајдер </a></p>

                <div class="card">
                    <div class="card-title">
                      <h4>Слики</h4>
                  </div>
                  <div class="card-body">
                      <div class="table-responsive">
                        <table class="table">

                          <tbody>
                             <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                             <tr>
                                <td>
                                  <div class="round-img">
                                    <a href="/admin/slider/<?php echo e($slider->id); ?>/edit"><?php if($slider->image): ?><img src="/assets/img/slider/thumbnails/<?php echo e($slider->image); ?>" alt=""><?php endif; ?></a>
                                </div>
                            </td>
                            <td><?php echo e($slider->title); ?></td>

                            <td><span>Original creator: <?php echo e($slider->createdby->name); ?></span></td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>