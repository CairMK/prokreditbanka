<?php $__env->startSection('content'); ?>
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <p><a class="btn btn-labeled" href="/admin/bankomat/create"> <i class="btn-label fa fa-plus"></i>Додади
                        банкомати и експозитури </a></p>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Банкомати и експозитури</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">

                                <tbody>
                                <?php $__currentLoopData = $bankomat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bankomats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td>

                                            <div class="round-img">
                                                <a href="/admin/bankomat/<?php echo e($bankomats->id); ?>/edit"><img
                                                            src="/assets/img/bankomat/thumbnails/<?php echo e($bankomats->image); ?>"
                                                            alt=""></a>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="/admin/bankomat/<?php echo e($bankomats->id); ?>/edit"><?php echo e($bankomats->title); ?></a>
                                        </td>
                                        <td><?php echo e($bankomats->created_at); ?></td>

                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 right">
                <?php echo $bankomat->links(); ?>

            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>