<?php $__env->startSection('scripts'); ?>
    <!-- Google Maps -->
    <script type="text/javascript"
            src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyA75bnzyJ_5j2Ger9Erjo1Q-0XucnZbst4"></script>
    <script>

        $(document).ready(function () {
// Google Maps


            map = new google.maps.Map(document.getElementById('map-canvas'), {
                center: {lat: <?php echo e($bankomat->lat); ?>, lng: <?php echo e($bankomat->lng); ?> },
                zoom: 10
            });

            var marker = new google.maps.Marker({
                position: {lat: <?php echo e($bankomat->lat); ?>, lng: <?php echo e($bankomat->lng); ?> },
                map: map,
                draggable: true
            });

            var input = document.getElementById('searchmap');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(input);

            google.maps.event.addListener(searchBox, 'places_changed', function () {
                var places = searchBox.getPlaces();
                var bounds = new google.maps.LatLngBounds();
                var i, place;
                for (i = 0; place = places[i]; i++) {
                    bounds.extend(place.geometry.location);
                    marker.setPosition(place.geometry.location);
                }
                map.fitBounds(bounds);
                map.setZoom(8);

            });

            google.maps.event.addListener(marker, 'position_changed', function () {
                var lat = marker.getPosition().lat();
                var lng = marker.getPosition().lng();

                $('#lat').val(lat);
                $('#lng').val(lng);
            });


            $("form").bind("keypress", function (e) {
                if (e.keyCode == 13) {
                    $("#searchmap").attr('value');
                    //add more buttons here
                    return false;
                }
            });

        });

    </script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                <?php if(Session::has('flash_message')): ?>
                    <div class="alert alert-success">
                        <?php echo e(Session::get('flash_message')); ?>

                    </div>
                <?php endif; ?>

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Edit bankomat: <?php echo e(strip_tags($bankomat->title)); ?></span>
                    </div>
                    <div class="widget-body">

                        <div class="img-blog">
                            <?php if(!!$bankomat->image): ?>
                                <img class="img-responsive"
                                     src="/assets/img/bankomat/medium/<?php echo e($bankomat->imagemedium); ?>"
                                     alt="<?php echo e($bankomat->title); ?>"/>
                            <?php endif; ?>
                        </div>
                        <br/>


                        <div id="horizontal-form">

                            <?php echo e(Form::model('bankomat', array('route' => array('admin.bankomat.update', $bankomat->id), 'method' => 'PUT','files' => true))); ?>

                            <?php echo csrf_field(); ?>



                            <div class="input-group<?php echo e($errors->has('image') ? ' has-error' : ''); ?>">
                        <span class="input-group-btn">
                            <span>
                                 Browse... <input type="file" name="image">
                            </span>
                        </span>
                            </div>
                            <br/>
                            <?php if($errors->has('image')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('image')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" placeholder="<?php echo e($bankomat->title); ?>"
                                       name="title">
                            </div>
                            <?php if($errors->has('title')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('title')); ?></p> <?php endif; ?>


                            <div class="form-group">
                                <label for="description">bankomat description</label>
                                <textarea class="ckeditor" id="elm3"
                                          name="description"><?php echo e($bankomat->description); ?></textarea>
                            </div>
                            <?php if($errors->has('description')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('description')); ?></p> <?php endif; ?>


                            <br>
                            <style>
                                /* Set the size of the div element that contains the map */
                                #map-canvas {
                                    height: 400px; /* The height is 400 pixels */
                                    width: 100%; /* The width is the width of the web page */
                                }
                            </style>
                            <div class="form-group">
                                <input type="text" id="searchmap" class="form-control">
                                <div id="map-canvas"></div>
                            </div>

                            <div class="form-group">
                                <label for="user">Author</label>
                                <select name="user_id" id="user" class="form-control">
                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($user->id); ?>"
                                                <?php if($bankomat->user_id == $user->id): ?> selected <?php endif; ?> ><?php echo e($user->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>
                            </div>


                            <div class="form-group">
                                <label>Workflow: </label>
                                <?php $__currentLoopData = $workflows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $workflow): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <label>
                                        <input name="workflow_id" type="radio"
                                               class="form-control <?php echo e($workflow->color); ?>" value="<?php echo e($workflow->id); ?>"
                                               <?php if($workflow->id  == 1): ?> checked <?php endif; ?>>
                                        <span class="text"> <?php echo e($workflow->name); ?></span>
                                    </label>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                            <input type="hidden" id="lat" class="form-control" name="lat"
                                   value="<?php echo e($bankomat->lat); ?>">
                            <input type="hidden" id="lng" class="form-control" name="lng"
                                   value="<?php echo e($bankomat->lng); ?>">
                            <input type="hidden" id="id" class="form-control" name="id" value="<?php echo e($bankomat->id); ?>">

                            <button type="submit" class="btn btn-labeled shiny btn-info btn-large"><i
                                        class="btn-label fa fa-plus"></i> Update
                            </button>
                            <?php echo Form::close(); ?>


                            <hr/>

                            <?php echo e(Form::model('bankomat', array('route' => array('admin.bankomat.destroy', $bankomat->id), 'method' => 'DELETE', 'id' => 'deletebankomat'))); ?>

                            <?php echo csrf_field(); ?>


                            <button type="submit" class="btn btn-labeled shiny btn-danger delete"><i
                                        class="btn-label fa fa-trash"></i> Delete
                            </button>
                            <?php echo e(Form::close()); ?>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <!-- Google Maps -->
    <script type="text/javascript"
            src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyA75bnzyJ_5j2Ger9Erjo1Q-0XucnZbst4"></script>
    <script type="text/javascript" src="/assets/js/maps.js"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>