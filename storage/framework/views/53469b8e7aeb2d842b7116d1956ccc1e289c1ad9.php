<!DOCTYPE html>
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<meta charset="utf-8">
<title><?php echo e($settings->title); ?></title>
<meta name="description" content="<?php echo str_limit(strip_tags($settings->description), 150, '...'); ?>">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo e(str_limit(strip_tags($settings->description),170, $end = '...' )); ?>">
    <meta name="author" content="">
    <meta name="keywords"
          content="ПроКредит,мали,средни,претпријатија,банка,кредит,кредити,сметка,сметки,денарска сметка,еко, девизна сметка,закажи средба,контакт инфо, инфо, картички,физички лица, Master,Maestro,Visa,штедење,денарско штедење,девизно штедење,платен промет,гаранции, станбен кредит,готовински кредит, курсна,потрошувачки кредит,кредит,banka,kredit,krediti,smetka,smetki,denarska smetka,devizna smetka, prokredit,karticki,stedenje,denarsko stedenje,devizno stedenje,platen promet,garancii,gotovinski kredit, stamben kredit, appointment,sredba,info, News,Apply Online, Price List, mali, sredni, pretprijatija"/>
    <!-- Bootstrap core CSS -->
    <link href="/assets/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<div class="page-wrapper">

    <?php echo $__env->yieldContent('menu'); ?>

    <?php echo $__env->yieldContent('slider'); ?>

    <div id="page-start"></div>

    <?php echo $__env->yieldContent('content'); ?>

<!-- FOOTER -->
    <div class="row footer-wrapper px-0 mx-0">
        <div class="container">
            <footer>
                <div class="row">
                    <div class="col-lg-3 col-md-7 order-lg-1 order-md-4 order-1 ">
                        <ul class="list-unstyled p-2">
                            <li class="d-flex align-items-center"><i class="swift-icon"></i>
                                <h3 class="frontpage">SWIFT:PRBUMK22XXX</h3></li>
                            <li class="d-flex align-items-center"><i class="envelope-icon"></i>
                                <h3 class="frontpage">info@procreditbank.com.mk</h3>
                            </li>
                            <li class="d-flex align-items-center"><i class="location-icon"></i>
                                <h3 class="frontpage">Manapo br.7 1000 Skopje</h3>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-4 col-6 order-lg-2 order-md-1 order-2 ">
                        <ul class="list-unstyled p-2">
                            <li class="mb-1"><a href="#"><h4 class="frontpage"><?php echo e(trans('menu.news')); ?></h4></a></li>
                            <li class="mb-1"><a href="#"><h4 class="frontpage"><?php echo e(trans('menu.applayonline')); ?></h4></a>
                            </li>
                            <li class="mb-1"><a href="#"><h4 class="frontpage">Ценовник</h4></a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-4 col-6 order-lg-3 order-md-2 order-3 ">
                        <ul class="list-unstyled p-2">
                            <li class="mb-1"><a href="/abou-us#page-tab-1"><h6
                                            class="frontpage"><?php echo e(trans('menu.aboutus')); ?></h6></a></li>
                            <li class="mb-1"><a href="https://hr.procredit-group.com/mk?languagecode=en"><h6
                                            class="frontpage"><?php echo e(trans('menu.OUR_STAFF')); ?></h6></a></li>
                            <li class="mb-1"><a href="/our-eco-strategy"><h6
                                            class="frontpage"><?php echo e(trans('menu.ECO')); ?></h6></a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-4 col-6 order-lg-4 order-md-3 order-4 ">
                        <ul class="list-unstyled p-2">
                            <li class="mb-1"><a href="/terms-and-conditions"><h5
                                            class="frontpage"><?php echo e(trans('menu.terms')); ?></h5></a></li>
                            <li class="mb-1"><a href="#"><h5 class="frontpage">Тендери</h5></a></li>
                            <li class="mb-1"><a href="#"><h5 class="frontpage">Продажба на имот</h5></a></li>
                            <li class="mb-1"><a href="/faq"><h5 class="frontpage"><?php echo e(trans('menu.faq')); ?></h5></a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-5 col-6 order-lg-5 order-md-5 order-5 ">
                        <div class="social-media-footer d-flex align-items-center flex-wrap">
                            <?php if($settings->twitter): ?>
                                <a target="_blank" href="<?php echo e($settings->twitter); ?>"><i
                                            class="fa fa-twitter"></i></a>
                            <?php endif; ?>
                            <?php if($settings->skype): ?>
                                <a target="_blank" href="<?php echo e($settings->skype); ?>"><i
                                            class="fa fa-skype"></i></a>
                            <?php endif; ?>
                            <?php if($settings->linkedin): ?>
                                <a target="_blank" href="<?php echo e($settings->linkedin); ?>"><i
                                            class="fa fa-linkedin"></i></a>
                            <?php endif; ?>
                            <?php if($settings->gplus): ?>
                                <a target="_blank" href="<?php echo e($settings->gplus); ?>"><i
                                            class="fa fa-google-plus"></i></a>
                            <?php endif; ?>
                            <?php if($settings->youtube): ?>
                                <a target="_blank" href="<?php echo e($settings->youtube); ?>"><i
                                            class="fa fa-youtube-play"></i></a>
                            <?php endif; ?>
                            <?php if($settings->flickr): ?>
                                <a target="_blank" href="<?php echo e($settings->flickr); ?>"><i
                                            class="fa fa-flickr"></i></a>
                            <?php endif; ?>
                            <?php if($settings->facebook): ?>
                                <a target="_blank" href="<?php echo e($settings->facebook); ?>"><i
                                            class="fa fa-facebook"></i></a>
                            <?php endif; ?>
                            <?php if($settings->pinterest): ?>
                                <a target="_blank" href="<?php echo e($settings->pinterest); ?>"><i
                                            class="fa fa-pinterest"></i></a>
                            <?php endif; ?>
                            <img class="group-logo" src="/assets/img/pcb-group.png" alt="image">
                        </div>
                        <h2 class="copyright mt-2">&copy; All Rights Reserved.<br>Pro Credit Bank 2018</h2>
                    </div>

                </div>

            </footer>
        </div>
    </div>
    <!-- FOOTER -->
    <!-- Bootstrap core JavaScript -->
    <script src="/assets/vendor/jquery/jquery.min.js"></script>
    <script defer="defer" src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <?php echo $__env->yieldContent('scripts'); ?>
    <script defer="defer" type="text/javascript" src="/assets/vendor/scrolloverflow.js"></script>
    <script defer="defer" type="text/javascript" src="/assets/js/jquery.fullPage.js"></script>
    <script defer="defer" src="/assets/js/main.js"></script>


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

    <script defer="defer" src="/assets/slick/slick.min.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">
        $(window).load(function () {
            $(".lazy").slick({

                infinite: true,
                dots: true,
                prevArrow: false,
                nextArrow: false,
                autoplay: true,
                autoplaySpeed: 2000

            });
        });
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-76143150-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-76143150-2');
    </script>


    </body>
</html>
