<?php $__env->startSection('content'); ?>

    <div class="col-lg-10">
    <div class="row">
        <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

            <?php if(Session::has('flash_message')): ?>
            <div class="alert alert-success">
                <?php echo e(Session::get('flash_message')); ?>

            </div>
            <?php endif; ?>

            <div class="widget">
                <div class="widget-header bordered-bottom bordered-warning">
                    <span class="widget-caption">Додади слика во слајдер</span>
                </div>
                <br>
                <div class="widget-body">
                    <div id="horizontal-form">

                        <?php echo e(Form::model('sliders', array('route' => array('admin.sliders.store'), 'method' => 'POST', 'files'=>true))); ?>

                        <?php echo csrf_field(); ?>



                        <div class="input-group<?php echo e($errors->has('image') ? ' has-error' : ''); ?>">
                           <span>
                            <span class=" btn-file">
                              Избери слика... <input type="file"
                              name="image">
                          </span>
                      </span>
                  </div>
                  <br/>
                  <?php if($errors->has('image')): ?> <p
                  class="alert alert-danger"><?php echo e($errors->first('image')); ?></p> <?php endif; ?>


                  <div class="form-group">
                    <label for="user">Уредник</label>
                    <select name="user_id" id="user" class="form-control">
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($user->id); ?>"
                            <?php if(Auth::user()->id == $user->id): ?> selected <?php endif; ?> ><?php echo e($user->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </select>
                    </div>


                    <div class="form-group">
                        <label>Workflow: </label>
                        <?php $__currentLoopData = $workflows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $workflow): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <label>
                            <input name="workflow_id" type="radio"
                            class="form-control <?php echo e($workflow->color); ?>" value="<?php echo e($workflow->id); ?>"
                            <?php if($workflow->id  == 1): ?> checked <?php endif; ?>>
                            <span class="text"> <?php echo e($workflow->name); ?></span>
                        </label>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                    </div>


                    <!-- Hidden inputs -->

                    <input type="hidden" name="creator" value="<?php echo e(Auth::user()->id); ?>">
                    <input type="hidden" id="product_id" class="form-control" name="product_id"
                    value="<?php echo e($product->id); ?>">



                    <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                        class="btn-label fa fa-plus"></i> Create
                    </button>
                    <?php echo Form::close(); ?>



                </div>
            </div>
        </div>
    </div>
</div>

<?php $__currentLoopData = array_chunk($sliders->all(), 3); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sliders): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="row">
    <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">
        <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php echo e(Form::model('sliders', array('route' => array('admin.sliders.destroy' , $product->id), 'method' => 'POST', 'files'=>true))); ?>

        <div class="col-md-4">
            <input type="hidden" id="id" class="form-control" name="id"
            value="<?php echo e($slider->id); ?>">
            <?php echo e(method_field('DELETE')); ?>


            <img src="/assets/img/sliders/medium/<?php echo e($slider->image); ?>" class="img-responsive"/>

            <button type="submit" class="btn btn-danger">Delete</button>

        </div>
        <?php echo Form::close(); ?>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>