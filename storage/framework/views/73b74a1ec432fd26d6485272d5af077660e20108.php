<?php $__env->startSection('scripts'); ?>
    <!-- Google Maps -->
    <script type="text/javascript"
            src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyA75bnzyJ_5j2Ger9Erjo1Q-0XucnZbst4"></script>

    <script>

        $(document).ready(function () {
// Google Maps


            map = new google.maps.Map(document.getElementById('map-canvas'), {
                center: {lat: <?php echo e($settings->lat); ?>, lng: <?php echo e($settings->lng); ?> },
                zoom: 10
            });

            var marker = new google.maps.Marker({
                position: {lat: <?php echo e($settings->lat); ?>, lng: <?php echo e($settings->lng); ?> },
                map: map,
                draggable: true
            });

            var input = document.getElementById('searchmap');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(input);

            google.maps.event.addListener(searchBox, 'places_changed', function () {
                var places = searchBox.getPlaces();
                var bounds = new google.maps.LatLngBounds();
                var i, place;
                for (i = 0; place = places[i]; i++) {
                    bounds.extend(place.geometry.location);
                    marker.setPosition(place.geometry.location);
                }
                map.fitBounds(bounds);
                map.setZoom(8);

            });

            google.maps.event.addListener(marker, 'position_changed', function () {
                var lat = marker.getPosition().lat();
                var lng = marker.getPosition().lng();

                $('#lat').val(lat);
                $('#lng').val(lng);
            });


            $("form").bind("keypress", function (e) {
                if (e.keyCode == 13) {
                    $("#searchmap").attr('value');
                    //add more buttons here
                    return false;
                }
            });

        });

    </script>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                <?php if(Session::has('flash_message')): ?>
                    <div class="alert alert-success">
                        <?php echo e(Session::get('flash_message')); ?>

                    </div>
                <?php endif; ?>

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Подесувања</span>
                    </div>
                    <div class="widget-body">

                        <div class="img-refferal">
                            <?php if(!!$settings->logo): ?>
                                <img class="img-responsive" src="/assets/img/logo/thumbnails/<?php echo e($settings->logo); ?>"
                                     alt="<?php echo e($settings->title); ?>"/>
                            <?php endif; ?>
                        </div>
                        <br/>


                        <div id="horizontal-form">

                            
                            <?php echo Form::model('settings', ['action' => ['HomeController@update', $settings->id], 'method' => 'PUT']); ?>


                            <?php echo csrf_field(); ?>



                            <div class="input-group<?php echo e($errors->has('image') ? ' has-error' : ''); ?>">
                            <span class="input-group-btn">
                                <span class="btn btn-info shiny btn-file">
                                    <i class="btn-label fa fa-image"> </i> Browse... <input type="file" name="logo">
                                </span>
                            </span>
                                <input type="text" class="form-control" readonly="">
                            </div>
                            <br/>
                            <?php if($errors->has('image')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('image')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label for="title">Наслов</label>
                                <input type="text" name="title" class="form-control" value="<?php echo e($settings->title); ?>">
                            </div>
                            <?php if($errors->has('title')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('title')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Главна адреса: </label>
                                <input name="mainurl" type="text" class="form-control" value="<?php echo e($settings->mainurl); ?>">
                            </div>
                            <?php if($errors->has('mainurl')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('mainurl')); ?></p> <?php endif; ?>


                            <div class="form-group">
                                <label>Email адреса: </label>
                                <input name="email" type="email" class="form-control" value="<?php echo e($settings->email); ?>">
                            </div>
                            <?php if($errors->has('email')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('email')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Адреса: </label>
                                <input name="address" type="text" class="form-control" value="<?php echo e($settings->address); ?>">
                            </div>
                            <?php if($errors->has('address')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('address')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Телефон: </label>
                                <input name="phone" type="text" class="form-control" value="<?php echo e($settings->phone); ?>">
                            </div>
                            <?php if($errors->has('phone')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('phone')); ?></p> <?php endif; ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <p>Каматни стапки за депозити во МКД</p><br>
                                    <div class="form-group">
                                        <label>12 meseci: </label>
                                        <input name="mkd1" type="text" class="form-control"
                                               value="<?php echo e($settings->mkd1); ?>">
                                    </div>
                                    <?php if($errors->has('mkd1')): ?> <p
                                            class="alert alert-danger"><?php echo e($errors->first('mkd1')); ?></p> <?php endif; ?>
                                    <div class="form-group">
                                        <label>18 meseci: </label>
                                        <input name="mkd2" type="text" class="form-control"
                                               value="<?php echo e($settings->mkd2); ?>">
                                    </div>
                                    <?php if($errors->has('mkd2')): ?> <p
                                            class="alert alert-danger"><?php echo e($errors->first('mkd2')); ?></p> <?php endif; ?>
                                    <div class="form-group">
                                        <label>24 meseci: </label>
                                        <input name="mkd3" type="text" class="form-control"
                                               value="<?php echo e($settings->mkd3); ?>">
                                    </div>
                                    <?php if($errors->has('mkd3')): ?> <p
                                            class="alert alert-danger"><?php echo e($errors->first('mkd3')); ?></p> <?php endif; ?>
                                    <div class="form-group">
                                        <label>32 meseci: </label>
                                        <input name="mkd4" type="text" class="form-control"
                                               value="<?php echo e($settings->mkd4); ?>">
                                    </div>
                                    <?php if($errors->has('mkd4')): ?> <p
                                            class="alert alert-danger"><?php echo e($errors->first('mkd4')); ?></p> <?php endif; ?>
                                </div>
                                <div class="col-md-6">
                                    <p>Каматни стапки за депозити во EURO</p><br>
                                    <div class="form-group">
                                        <label>12 meseci: </label>
                                        <input name="euro1" type="text" class="form-control"
                                               value="<?php echo e($settings->euro1); ?>">
                                    </div>
                                    <?php if($errors->has('euro1')): ?> <p
                                            class="alert alert-danger"><?php echo e($errors->first('euro1')); ?></p> <?php endif; ?>
                                    <div class="form-group">
                                        <label>18 meseci: </label>
                                        <input name="euro2" type="text" class="form-control"
                                               value="<?php echo e($settings->euro2); ?>">
                                    </div>
                                    <?php if($errors->has('euro2')): ?> <p
                                            class="alert alert-danger"><?php echo e($errors->first('euro2')); ?></p> <?php endif; ?>
                                    <div class="form-group">
                                        <label>24 meseci: </label>
                                        <input name="euro3" type="text" class="form-control"
                                               value="<?php echo e($settings->euro3); ?>">
                                    </div>
                                    <?php if($errors->has('euro3')): ?> <p
                                            class="alert alert-danger"><?php echo e($errors->first('euro3')); ?></p> <?php endif; ?>
                                    <div class="form-group">
                                        <label>32 meseci: </label>
                                        <input name="euro4" type="text" class="form-control"
                                               value="<?php echo e($settings->euro4); ?>">
                                    </div>
                                    <?php if($errors->has('euro4')): ?> <p
                                            class="alert alert-danger"><?php echo e($errors->first('euro4')); ?></p> <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Twitter: </label>
                                <input name="twitter" type="text" class="form-control" value="<?php echo e($settings->twitter); ?>">
                            </div>
                            <?php if($errors->has('twitter')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('twitter')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Facebook: </label>
                                <input name="facebook" type="text" class="form-control"
                                       value="<?php echo e($settings->facebook); ?>">
                            </div>
                            <?php if($errors->has('facebook')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('facebook')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Skype: </label>
                                <input name="skype" type="text" class="form-control" value="<?php echo e($settings->skype); ?>">
                            </div>
                            <?php if($errors->has('skype')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('skype')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>LinkedIn: </label>
                                <input name="linkedin" type="text" class="form-control"
                                       value="<?php echo e($settings->linkedin); ?>">
                            </div>
                            <?php if($errors->has('linkedin')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('linkedin')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Google Plus: </label>
                                <input name="gplus" type="text" class="form-control" value="<?php echo e($settings->gplus); ?>">
                            </div>
                            <?php if($errors->has('gplus')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('gplus')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Youtube: </label>
                                <input name="youtube" type="text" class="form-control" value="<?php echo e($settings->youtube); ?>">
                            </div>
                            <?php if($errors->has('youtube')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('youtube')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Flickr: </label>
                                <input name="flickr" type="text" class="form-control" value="<?php echo e($settings->flickr); ?>">
                            </div>
                            <?php if($errors->has('flickr')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('flickr')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Pinterest: </label>
                                <input name="pinterest" type="text" class="form-control"
                                       value="<?php echo e($settings->pinterest); ?>">
                            </div>
                            <?php if($errors->has('pinterest')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('pinterest')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <input type="text" id="searchmap" class="form-control">
                                <div id="map-canvas"></div>
                            </div>


                            <div class="form-group">
                                <label for="description">Опис</label>
                                <textarea class="ckeditor" id="elm3"
                                          name="description"><?php echo e($settings->description); ?></textarea>
                            </div>
                            <?php if($errors->has('description')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('description')); ?></p> <?php endif; ?>


                            <div class="form-group">
                                <label for="user">Translator</label>
                                <select name="user_id" id="user" class="form-control">
                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($user->id); ?>"
                                                <?php if($settings->user_id == $user->id): ?> selected <?php endif; ?> ><?php echo e($user->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>
                            </div>


                            <div class="form-group">
                                <p>Workflow: </p>
                                <?php $__currentLoopData = $workflows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $workflow): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="workflow_id"
                                                   value="<?php echo e($workflow->id); ?>" <?php if($workflow->id  == 1): ?> checked <?php endif; ?>>
                                        </label>
                                        <span class="text"> <?php echo e($workflow->name); ?></span>
                                    </div>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>


                            <!-- Hidden inputs -->


                            <input type="hidden" id="lat" class="form-control" name="lat" value="<?php echo e($settings->lat); ?>">
                            <input type="hidden" id="lng" class="form-control" name="lng" value="<?php echo e($settings->lng); ?>">
                            <input type="hidden" id="id" class="form-control" name="id" value="<?php echo e($settings->id); ?>">

                            <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                                        class="btn-label fa fa-plus"></i> Обнови
                            </button>
                            <?php echo Form::close(); ?>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>