<?php $__env->startSection('content'); ?>
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <p><a class="btn btn-labeled" href="/admin/popup/create"> <i class="btn-label fa fa-plus"></i>Add PopUp
                    </a></p>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>PopUp</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <?php $__currentLoopData = $popup; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $popup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td>
                                            <div class="round-img">
                                                <a href="/admin/popup/<?php echo e($popup->id); ?>/edit"><img
                                                            src="/assets/img/popup/thumbnails/<?php echo e($popup->image); ?>"
                                                            alt=""></a>
                                            </div>
                                        </td>
                                        <td><?php echo e($popup->title); ?></td>
                                        <td><span><a href="/admin/sliders/<?php echo e($popup->id); ?>/popup" class="btn btn-info">Додади галерија</a></span>
                                        </td>
                                        <td><span>Слики во слајдер: <?php echo e($popup->images()->count()); ?></span></td>
                                        
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 right">
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>