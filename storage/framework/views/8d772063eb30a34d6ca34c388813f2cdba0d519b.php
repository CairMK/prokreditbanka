<?php $__env->startSection('content'); ?>
    <div class="col-lg-10">
    <div class="row">
        <div class="col-lg-12">
            <p>  <a class="btn btn-labeled" href="services/create"> <i class="btn-label fa fa-plus"></i>Додади Услуга </a></p>
        </div>
        <div class="col-lg-12">
          <div class="card">
            <div class="card-title">
              <h4>Референти</h4>
          </div>
          <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <tbody>
                    <?php $__currentLoopData = $referrals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $refferal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td>
                          <div class="round-img">
                            <a href="/admin/referrals/<?php echo e($refferal->id); ?>/edit"><img src="/assets/img/referrals/thumbnails/<?php echo e($refferal->image); ?>" alt=""></a>
                        </div>
                    </td>
                    <td><a href="/admin/referrals/<?php echo e($refferal->id); ?>/edit"><?php echo e($refferal->title); ?></a></td>
                    <td><span>Original time: <?php echo e($refferal->created_at); ?></span></td>
                    <td><span>Asigned for: <?php echo e($refferal->user->name); ?></span></td>
                    <td><span>Workflow: <?php echo e($refferal->workflow->name); ?></span></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>