<?php

    $levelAmount = 'level';

    if (Auth::User()->level() >= 2) {
        $levelAmount = 'levels';

    }

?>

<div class="card">
    <div class="card-header <?php if (Auth::check() && Auth::user()->hasRole('admin', true)): ?> bg-secondary text-white <?php endif; ?>">

        Welcome <?php echo e(Auth::user()->name); ?>


        <?php if (Auth::check() && Auth::user()->hasRole('admin', true)): ?>
        <span class="pull-right badge badge-primary" style="margin-top:4px">
                Admin Access
            </span>
        <?php else: ?>
            <span class="pull-right badge badge-warning" style="margin-top:4px">
                User Access
            </span>
            <?php endif; ?>

    </div>
    <div class="card-body">
        

    </div>
</div>
