<?php $__env->startSection('content'); ?>

    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                <?php if(Session::has('flash_message')): ?>
                    <div class="alert alert-success">
                        <?php echo e(Session::get('flash_message')); ?>

                    </div>
                <?php endif; ?>

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Edit product: <?php echo e(strip_tags($product->title)); ?></span>
                    </div>
                    <div class="widget-body">

                        <div class="img-blog">
                            <?php if(!!$product->image): ?>
                                <img class="img-responsive"
                                     src="/assets/img/products/medium/<?php echo e($product->imagemedium); ?>"
                                     alt="<?php echo e($product->title); ?>"/>
                            <?php endif; ?>
                        </div>
                        <br/>


                        <div id="horizontal-form">

                            <?php echo e(Form::model('product', array('route' => array('admin.product.update', $product->id), 'method' => 'PUT','files' => true))); ?>

                            <?php echo csrf_field(); ?>



                            <div class="input-group<?php echo e($errors->has('image') ? ' has-error' : ''); ?>">
                            <span class="input-group-btn">
                                <span>
                                    Browse... <input type="file" name="image">
                                </span>
                            </span>
                            </div>
                            <br/>
                            <?php if($errors->has('image')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('image')); ?></p> <?php endif; ?>

                            <div class="form-group row">
                                <label for="title" class="col-2 col-form-label">Title</label>
                                <div class="col-12">
                                    <input class="form-control" type="text" value="<?php echo e($product->title); ?>" name="title"
                                           id="title">
                                </div>
                            </div>
                            <?php if($errors->has('title')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('title')); ?></p> <?php endif; ?>


                            <div class="form-group">
                                <label for="description">Product description</label>
                                <textarea class="ckeditor" id="elm3"
                                          name="description"><?php echo e($product->description); ?></textarea>
                            </div>
                            <?php if($errors->has('description')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('description')); ?></p> <?php endif; ?>


                            <div id="accordion" role="tablist">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h5 class="mb-0">
                                            <a data-toggle="collapse" href="#collapse1" aria-expanded="true"
                                               aria-controls="collapseOne">
                                                Наслов и опис на таб 1
                                            </a>
                                        </h5>
                                    </div>
                                </div>
                                <div id="collapse1" class="collapse show" role="tabpanel"
                                     aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <label for="title" class="col-2 col-form-label">Title</label>
                                        <div class="col-12">
                                            <input class="form-control" type="text" value="<?php echo e($product->title1); ?>"
                                                   name="title1" id="title1">
                                        </div>
                                        <?php if($errors->has('title1')): ?> <p
                                                class="alert alert-danger"><?php echo e($errors->first('title1')); ?></p> <?php endif; ?>
                                        <br>
                                        <div class="form-group">
                                            <label for="description1">Product description</label>
                                            <textarea class="ckeditor" id="elm3"
                                                      name="description1"><?php echo e($product->description1); ?></textarea>
                                        </div>

                                        <?php if($errors->has('description1')): ?> <p
                                                class="alert alert-danger"><?php echo e($errors->first('description1')); ?></p> <?php endif; ?>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingTwo">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse2"
                                               aria-expanded="false" aria-controls="collapse2">
                                                Наслов и опис на таб 2
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse2" class="collapse" role="tabpanel"
                                         aria-labelledby="headingTwo"
                                         data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text" value="<?php echo e($product->title2); ?>"
                                                       name="title2" id="title2">
                                            </div>
                                            <?php if($errors->has('title1')): ?> <p
                                                    class="alert alert-danger"><?php echo e($errors->first('title2')); ?></p> <?php endif; ?>
                                            <br>
                                            <div class="form-group">
                                                <label for="description2">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description2"><?php echo e($product->description2); ?></textarea>
                                            </div>
                                            <?php if($errors->has('description2')): ?> <p
                                                    class="alert alert-danger"><?php echo e($errors->first('description2')); ?></p> <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse3"
                                               aria-expanded="false" aria-controls="collapseThree">
                                                Наслов и опис на таб 3
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse3" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text" value="<?php echo e($product->title3); ?>"
                                                       name="title3" id="title3">
                                            </div>
                                            <?php if($errors->has('title3')): ?> <p
                                                    class="alert alert-danger"><?php echo e($errors->first('title3')); ?></p> <?php endif; ?>
                                            <br>
                                            <div class="form-group">
                                                <label for="description3">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description3"><?php echo e($product->description3); ?></textarea>
                                            </div>
                                            <?php if($errors->has('description3')): ?> <p
                                                    class="alert alert-danger"><?php echo e($errors->first('description3')); ?></p> <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse4"
                                               aria-expanded="false" aria-controls="collapse4">
                                                Наслов и опис на таб 4
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse4" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text" value="<?php echo e($product->title4); ?>"
                                                       name="title4" id="title4">
                                            </div>
                                            <?php if($errors->has('title4')): ?> <p
                                                    class="alert alert-danger"><?php echo e($errors->first('title4')); ?></p> <?php endif; ?>
                                            <br>
                                            <div class="form-group">
                                                <label for="description4">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description4"><?php echo e($product->description4); ?></textarea>
                                            </div>
                                            <?php if($errors->has('description4')): ?> <p
                                                    class="alert alert-danger"><?php echo e($errors->first('description4')); ?></p> <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse5"
                                               aria-expanded="false" aria-controls="collapseThree">
                                                Наслов и опис на таб 5
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse5" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text" value="<?php echo e($product->title5); ?>"
                                                       name="title5" id="title5">
                                            </div>
                                            <?php if($errors->has('title5')): ?> <p
                                                    class="alert alert-danger"><?php echo e($errors->first('title5')); ?></p> <?php endif; ?>
                                            <br>
                                            <div class="form-group">
                                                <label for="description5">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description5"><?php echo e($product->description5); ?></textarea>
                                            </div>
                                            <?php if($errors->has('description5')): ?> <p
                                                    class="alert alert-danger"><?php echo e($errors->first('description5')); ?></p> <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapse6"
                                               aria-expanded="false" aria-controls="collapseThree">
                                                Наслов и опис на таб 6
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse6" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <label for="title" class="col-2 col-form-label">Title</label>
                                            <div class="col-12">
                                                <input class="form-control" type="text" value="<?php echo e($product->title6); ?>"
                                                       name="title6" id="title6">
                                            </div>
                                            <?php if($errors->has('title6')): ?> <p
                                                    class="alert alert-danger"><?php echo e($errors->first('title6')); ?></p> <?php endif; ?>
                                            <br>
                                            <div class="form-group">
                                                <label for="description6">Product description</label>
                                                <textarea class="ckeditor" id="elm3"
                                                          name="description6"><?php echo e($product->description6); ?></textarea>
                                            </div>
                                            <?php if($errors->has('description6')): ?> <p
                                                    class="alert alert-danger"><?php echo e($errors->first('description6')); ?></p> <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="lang">Izberete jazik: </label>
                                <select name="lang" id="lang" class="form-control">
                                    <option value="mk">Makedonski</option>
                                    <option value="en">Angliski</option>
                                    <option value="sq">Albanski</option>
                                </select>
                            </div>
                            <?php if($errors->has('lang')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('lang')); ?></p> <?php endif; ?>
                            <br>
                            <div class="form-group">
                                <label for="user">Creator</label>
                                <select name="user_id" id="user" class="form-control">
                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($user->id); ?>"
                                                <?php if($product->user_id == $user->id): ?> selected <?php endif; ?> ><?php echo e($user->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>
                            </div>

                            <div class="form-group">
                                <p>Featured: </p>
                                <?php $__currentLoopData = $featureds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $featured): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="featured_id"
                                                   value="<?php echo e($featured->id); ?>" <?php if($featured->id  == 1): ?> checked <?php endif; ?>>
                                        </label>
                                        <span class="text"> <?php echo e($featured->name); ?></span>
                                    </div>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                            <div class="form-group">
                                <p>Workflow: </p>
                                <?php $__currentLoopData = $workflows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $workflow): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="workflow_id"
                                                   value="<?php echo e($workflow->id); ?>" <?php if($workflow->id  == 1): ?> checked <?php endif; ?>>
                                        </label>
                                        <span class="text"> <?php echo e($workflow->name); ?></span>
                                    </div>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>

                            <button type="submit" class="btn btn-labeled shiny btn-info btn-large"><i
                                        class="btn-label fa fa-plus"></i> Update
                            </button>
                            <?php echo Form::close(); ?>


                            <hr/>

                            <?php echo e(Form::model('product', array('route' => array('admin.product.destroy', $product->id), 'method' => 'DELETE', 'id' => 'deleteproduct'))); ?>

                            <?php echo csrf_field(); ?>


                            <button type="submit" class="btn btn-labeled shiny btn-danger delete"><i
                                        class="btn-label fa fa-trash"></i> Delete
                            </button>
                            <?php echo e(Form::close()); ?>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>