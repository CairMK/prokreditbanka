<?php $__env->startSection('content'); ?>

    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">
                <?php if(Session::has('flash_message')): ?>
                    <div class="alert alert-success">
                        <?php echo e(Session::get('flash_message')); ?>

                    </div>
                <?php endif; ?>
                <div class="widget-header bordered-bottom bordered-warning">
                    <span class="widget-caption">Add bankomat ans atms</span>
                </div>
                <div class="widget-body">
                    <div id="horizontal-form">
                        <?php echo e(Form::model('bankomat', array('route' => array('admin.bankomat.store'), 'method' => 'POST', 'files'=>true))); ?>

                        <?php echo csrf_field(); ?>

                        <div class="input-group<?php echo e($errors->has('image') ? ' has-error' : ''); ?>">
                     <span class="input-group-btn">
                        <span>
                           Browse... <input type="file" name="image">
                       </span>
                   </span>
                        </div>
                        <br/>
                        <?php if($errors->has('image')): ?> <p
                                class="alert alert-danger"><?php echo e($errors->first('image')); ?></p> <?php endif; ?>
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" placeholder="Title" name="title">
                        </div>

                        <?php if($errors->has('title')): ?> <p
                                class="alert alert-danger"><?php echo e($errors->first('title')); ?></p> <?php endif; ?>
                        <div class="form-group">
                            <label for="category">Category</label>

                            <select name="category" id="category" class="form-control">
                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($category->id); ?>"><?php for($i = 0; $i < $category->depth; $i++): ?>
                                            - <?php endfor; ?> <?php echo e($category->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>

                        <?php if($errors->has('category')): ?> <p
                                class="alert alert-danger"><?php echo e($errors->first('category')); ?></p> <?php endif; ?>

                        <div class="form-group">
                            <label for="country">Cities</label>
                            <select name="cities" id="cities" class="form-control">
                                <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cities): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($cities->id); ?>"><?php echo e($cities->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <?php if($errors->has('cities')): ?> <p>
                            class="alert alert-danger"><?php echo e($errors->first('cities')); ?></p> <?php endif; ?>
                        <div class="form-group">
                            <label for="description">Product description</label>
                            <textarea class="ckeditor" id="elm3" name="description"></textarea>
                        </div>
                        <?php if($errors->has('description')): ?> <p
                                class="alert alert-danger"><?php echo e($errors->first('description')); ?></p> <?php endif; ?>
                        <style>
                            /* Set the size of the div element that contains the map */
                            #map-canvas {
                                height: 400px; /* The height is 400 pixels */
                                width: 100%; /* The width is the width of the web page */
                            }
                        </style>
                        <div class="form-group">
                            <input type="text" id="searchmap" class="form-control">
                            <div id="map-canvas"></div>
                        </div>
                        <label for="lang">Izberete jazik: </label>
                        <select name="lang" id="lang" class="form-control">
                            <option value="mk">Makedonski</option>
                            <option value="en">Angliski</option>
                            <option value="sq">Albanski</option>
                        </select>
                        <div class="form-group">
                            <label for="user">User</label>
                            <select name="user_id" id="user" class="form-control">
                                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($user->id); ?>"
                                            <?php if(Auth::user()->id == $user->id): ?> selected <?php endif; ?> ><?php echo e($user->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </select>
                        </div>
                        <div class="form-group">
                            <label>Workflow: </label>
                            <?php $__currentLoopData = $workflows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $workflow): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <label>
                                    <input name="workflow_id" type="radio" class="form-control <?php echo e($workflow->color); ?>"
                                           value="<?php echo e($workflow->id); ?>" <?php if($workflow->id  == 1): ?> checked <?php endif; ?>>
                                    <span class="text"> <?php echo e($workflow->name); ?></span>
                                </label>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>

                        <!-- Hidden inputs -->

                        <input type="hidden" name="creator" value="<?php echo e(Auth::user()->id); ?>">
                        <input type="hidden" id="lat" class="form-control" name="lat">
                        <input type="hidden" id="lng" class="form-control" name="lng">

                        <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                                    class="btn-label fa fa-plus"></i> Зачувај
                        </button>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div><!--zavrsuva widget -->
        </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <!-- Google Maps -->
    <script type="text/javascript"
            src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyA75bnzyJ_5j2Ger9Erjo1Q-0XucnZbst4"></script>
    <script type="text/javascript" src="/assets/admin/js/maps.js"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>