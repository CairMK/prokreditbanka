<?php echo $__env->make('layouts.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('content'); ?>
    <!-- SUB HEADER -->

    <?php if( empty($staticpage['image'])): ?>
        <div class="container-fluid bg-light">
            <div class="container">
                <div class="row px-0 mx-0 py-3">
                    <h1><?php echo e($staticpage->title); ?></h1>
                </div>
            </div>
        </div>
        <div class="row px-0 mx-0 ">
            <div class="container-fluid bg-primary">
                <div class="container">
                    <div class="sub-header-content d-flex justify-content-end">
                        <!--<ul class="breadcrumbs d-flex list-unstyled">-->
                        <!--    <li><a href="/accounts-and-payments">Сметки</a></li>-->
                        <!--    <li><a href="/credit-offers">Кредитна линија</a></li>-->
                        <!--    <li><a href="/documentary-operations">Гаранции и акредитиви</a></li>-->
                        <!--    <li><a href="/term-deposits">Депозити и хартии од вредност</a></li>-->
                        <!--    <li>-->
                        <!--        <a href="/services-for-non-resident-legal-entities">Услуги за нерезидентни-->
                        <!--            преставништва</a>-->
                        <!--    </li>-->
                        <!--</ul>-->
                        <a href="#" class="chat-now-button"><?php echo e(trans('menu.chat_now')); ?></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="page-content">
                <div class="row mx-0 px-0">
                    <!-- SIDEBAR -->
                    <div class="col-md-12">
                        <div class="tab-content" id="pageTabs">
                            <div class="tab-pane fade show active" id="page-tab-1" role="tabpanel">
                                <div class="col-md-12 border-bottom mb-3">
                                    <h2 class="heading"><?php echo e($staticpage->title); ?></h2>
                                </div>

                                <div class="col-md-12">
                                    <?php echo $staticpage->description; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- CONTENT -->
                </div>
            </div>

            <!-- MAIN CONTENT CONTAINER -->
            <?php else: ?>
                <div class="sub-header-image"
                     style="background-image: url('/assets/img/staticpage/<?php echo e($staticpage->image); ?>');"></div>
                <div class="bg-primary">
                    <div class="row px-0 mx-0 ">
                        <div class="container-fluid bg-primary">
                            <div class="container">
                                <div class="sub-header-content d-flex justify-content-end">
                                    <!--<ul class="breadcrumbs d-flex list-unstyled">-->
                                    <!--    <li><a href="/accounts-and-payments">Сметки</a></li>-->
                                    <!--    <li><a href="/credit-offers">Кредитна линија</a></li>-->
                                    <!--    <li><a href="/documentary-operations">Гаранции и акредитиви</a></li>-->
                                    <!--    <li><a href="/term-deposits">Депозити и хартии од вредност</a></li>-->
                                    <!--    <li>-->
                                    <!--        <a href="/services-for-non-resident-legal-entities">Услуги за нерезидентни-->
                                    <!--            преставништва</a>-->
                                    <!--    </li>-->
                                    <!--</ul>-->
                                    <a href="#" class="chat-now-button"><?php echo e(trans('menu.chat_now')); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SUB HEADER -->
                <!-- main-container start -->
                <!-- ================ -->
                <!-- MAIN CONTENT CONTAINER -->
                <div class="container">
                    <div class="page-content">
                        <div class="row mx-0 px-0">
                            <!-- SIDEBAR -->
                            <div class="col-md-3">
                                <div class="sidebar">
                                    <ul class="list-unstyled nav nav-tabs">
                                        <?php if( ! empty($staticpage['title'])): ?>
                                            <li><a class="nav-link active" data-toggle="tab"
                                                   data-target="#page-tab-1"
                                                   href="#page-tab-1"><?php echo e($staticpage->title); ?></a>
                                            </li>
                                        <?php endif; ?>
                                        <?php if( ! empty($staticpage['title1'])): ?>
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-2" href="
                                   #page-tab-2"><?php echo e($staticpage->title1); ?></a>
                                            </li>
                                        <?php endif; ?>
                                        <?php if( ! empty($staticpage['title2'])): ?>
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-3" href="
                                   #page-tab-3"><?php echo e($staticpage->title2); ?></a>
                                            </li>
                                        <?php endif; ?>
                                        <?php if( ! empty($staticpage['title3'])): ?>
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-4" href="
                                   #page-tab-4"><?php echo e($staticpage->title3); ?></a>
                                            </li>
                                        <?php endif; ?>
                                        <?php if( ! empty($staticpage['title4'])): ?>
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-5" href="
                                   #page-tab-5"><?php echo e($staticpage->title4); ?></a>
                                            </li>
                                        <?php endif; ?>
                                        <?php if( ! empty($staticpage['title5'])): ?>
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-6"
                                                   href="#page-tab-6"><?php echo e($staticpage->title5); ?></a>
                                            </li>
                                        <?php endif; ?>
                                        <?php if( ! empty($staticpage['title6'])): ?>
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-7"
                                                   href="#page-tab-7"><?php echo e($staticpage->title6); ?></a>
                                            </li>
                                        <?php endif; ?>
                                        <?php if( ! empty($staticpage['title7'])): ?>
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-8"
                                                   href="#page-tab-7"><?php echo e($staticpage->title7); ?></a>
                                            </li>
                                        <?php endif; ?>
                                        <?php if( ! empty($staticpage['title8'])): ?>
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-9"
                                                   href="#page-tab-7"><?php echo e($staticpage->title8); ?></a>
                                            </li>
                                        <?php endif; ?>
                                        <?php if( ! empty($staticpage['title9'])): ?>
                                            <li><a class="nav-link" data-toggle="tab" data-target="#page-tab-10"
                                                   href="#page-tab-7"><?php echo e($staticpage->title9); ?></a>
                                            </li>
                                        <?php endif; ?>
                                        <div class="sidebar-bottom mt-3">
                                            <ul class="list-unstyled">
                                                <li class="sidebar-button location"><a href="/network">Експозитури и
                                                        банкомати</a>
                                                </li>
                                                <li class="sidebar-button faq"><a href="/faq">Често поставувани
                                                        прашања</a>
                                                </li>
                                                <li class="sidebar-button pricelist"><a href="">Ценовници за физички
                                                        лица</a></li>
                                                <li class="sidebar-button pricelist"><a href="">Ценовници за правни
                                                        лица</a>
                                                </li>
                                                <li class="sidebar-button flat-rate"><a href="">Курсна листа</a></li>
                                            </ul>
                                    </ul>

                                </div>
                            </div>
                            <!-- SIDEBAR -->
                            <!-- CONTENT -->
                            <div class="col-md-9">
                                <div class="col-md-12">
                                    
                                </div>
                                
                                
                                
                                <div class="tab-content" id="pageTabs">

                                    <div class="tab-pane fade show active" id="page-tab-1" role="tabpanel">
                                        <div class="col-md-12 border-bottom mb-3">
                                            <h2 class="heading"><?php echo e($staticpage->title); ?></h2>
                                        </div>

                                        <div class="col-md-12">
                                            <?php echo $staticpage->description; ?>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-2" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading"><?php echo $staticpage->title1; ?></h2>
                                        </div>
                                        <div class="col-md-12">
                                            <?php echo $staticpage->description1; ?>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-3" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading"><?php echo $staticpage->title2; ?></h2>
                                        </div>
                                        <div class="col-md-12">
                                            <?php echo $staticpage->description2; ?>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-4" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading"><?php echo e($staticpage->title3); ?></h2>
                                        </div>
                                        <div class="col-md-12">
                                            <?php echo $staticpage->description3; ?>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-5" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading"><?php echo e($staticpage->title4); ?></h2>
                                        </div>
                                        <div class="col-md-12">
                                            <?php echo $staticpage->description4; ?>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-6" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading"><?php echo e($staticpage->title5); ?></h2>
                                        </div>
                                        <div class="col-md-12">
                                            <?php echo $staticpage->description5; ?>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-7" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading"><?php echo e($staticpage->title6); ?></h2>
                                        </div>
                                        <div class="col-md-12">
                                            <?php echo $staticpage->description6; ?>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-8" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading"><?php echo e($staticpage->title7); ?></h2>
                                        </div>
                                        <div class="col-md-12">
                                            <?php echo $staticpage->description7; ?>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-9" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading"><?php echo e($staticpage->title8); ?></h2>
                                        </div>
                                        <div class="col-md-12">
                                            <?php echo $staticpage->description8; ?>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="page-tab-10" role="tabpanel">
                                        <div class="col-md-12">
                                            <h2 class="heading"><?php echo e($staticpage->title9); ?></h2>
                                        </div>
                                        <div class="col-md-12">
                                            <?php echo $staticpage->description9; ?>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- CONTENT -->
                    </div>
                </div>
        </div>
    <?php endif; ?>
    <!-- MAIN CONTENT CONTAINER -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>