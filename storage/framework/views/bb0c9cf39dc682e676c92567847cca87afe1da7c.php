<?php $__env->startSection('content'); ?>
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                <?php if(Session::has('flash_message')): ?>
                    <div class="alert alert-success">
                        <?php echo e(Session::get('flash_message')); ?>

                    </div>
                <?php endif; ?>

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Подесувања</span>
                    </div>
                    <div class="widget-body">
                        <div id="horizontal-form">

                            <?php echo e(Form::model('settings', array('route' => array('admin.settings.store'), 'method' => 'POST', 'files'=>true))); ?>


                            <div class="input-group<?php echo e($errors->has('logo') ? ' has-error' : ''); ?>">
                         <span class="input-group-btn"><br>
                            <span>
                               Избери лого... <input type="file"
                                                     name="logo">
                           </span>
                       </span>
                            </div>
                            <br/>
                            <?php if($errors->has('logo')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('logo')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label for="title">Наслов на страната</label>
                                <input type="text" name="title" class="form-control"></input>
                            </div>
                            <?php if($errors->has('title')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('title')); ?></p> <?php endif; ?>


                            <div class="form-group">
                                <label>Главна адреса: </label>
                                <input name="mainurl" type="text" class="form-control">
                            </div>
                            <?php if($errors->has('mainurl')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('mainurl')); ?></p> <?php endif; ?>


                            <div class="form-group">
                                <label>Email адреса: </label>
                                <input name="email" type="email" class="form-control">
                            </div>
                            <?php if($errors->has('email')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('email')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Адреса: </label>
                                <input name="address" type="text" class="form-control">
                            </div>
                            <?php if($errors->has('address')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('address')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Телефон: </label>
                                <input name="phone" type="text" class="form-control">
                            </div>
                            <?php if($errors->has('phone')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('phone')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Twitter: </label>
                                <input name="twitter" type="text" class="form-control">
                            </div>
                            <?php if($errors->has('twitter')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('twitter')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Facebook: </label>
                                <input name="facebook" type="text" class="form-control">
                            </div>
                            <?php if($errors->has('facebook')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('facebook')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Skype: </label>
                                <input name="skype" type="text" class="form-control">
                            </div>
                            <?php if($errors->has('skype')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('skype')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>LinkedIn: </label>
                                <input name="linkedin" type="text" class="form-control">
                            </div>
                            <?php if($errors->has('linkedin')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('linkedin')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Google Plus: </label>
                                <input name="gplus" type="text" class="form-control">
                            </div>
                            <?php if($errors->has('gplus')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('gplus')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Youtube: </label>
                                <input name="youtube" type="text" class="form-control">
                            </div>
                            <?php if($errors->has('youtube')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('youtube')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Flickr: </label>
                                <input name="flickr" type="text" class="form-control">
                            </div>
                            <?php if($errors->has('flickr')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('flickr')); ?></p> <?php endif; ?>

                            <div class="form-group">
                                <label>Pinterest: </label>
                                <input name="pinterest" type="text" class="form-control">
                            </div>
                            <?php if($errors->has('pinterest')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('pinterest')); ?></p> <?php endif; ?>


                            <div class="form-group">
                                <input type="text" id="searchmap" class="form-control">
                                <div id="map-canvas"></div>
                            </div>


                            <div class="form-group">
                                <label for="description">Детален текст</label>
                                <textarea class="ckeditor" id="elm3" name="description"></textarea>
                            </div>
                            <?php if($errors->has('description')): ?> <p
                                    class="alert alert-danger"><?php echo e($errors->first('description')); ?></p> <?php endif; ?>


                            <div class="form-group">
                                <label for="user">Корисник</label>
                                <select name="user_id" id="user" class="form-control">

                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($user->id); ?>"
                                                <?php if(Auth::user()->id == $user->id): ?> selected <?php endif; ?> ><?php echo e($user->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>
                            </div>


                            <div class="form-group">
                                <p>Workflow: </p>
                                <?php $__currentLoopData = $workflows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $workflow): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="workflow_id"
                                                   value="<?php echo e($workflow->id); ?>" <?php if($workflow->id  == 1): ?> checked <?php endif; ?>>
                                        </label>
                                        <span class="text"> <?php echo e($workflow->name); ?></span>
                                    </div>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>


                            <!-- Hidden inputs -->

                            <input type="hidden" name="creator" value="<?php echo e(Auth::user()->id); ?>">
                            <input type="hidden" id="lat" class="form-control" name="lat">
                            <input type="hidden" id="lng" class="form-control" name="lng">

                            <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                                        class="btn-label fa fa-plus"></i> Зачувај
                            </button>
                            <?php echo Form::close(); ?>



                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php $__env->stopSection(); ?>


    <?php $__env->startSection('scripts'); ?>
        <!-- Google Maps -->
            <script type="text/javascript"
                    src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAsNNbnqvbIiRo2hBpr3-lvmYxm3ISPtWI"></script>
            <script type="text/javascript" src="/assets/js/maps.js"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>