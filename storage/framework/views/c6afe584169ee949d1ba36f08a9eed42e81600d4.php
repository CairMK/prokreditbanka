<?php echo $__env->make('layouts.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('content'); ?>
    <body class="page-1">
    <!-- SUB HEADER -->
    <div class="bg-primary-home entry-points-wrapper">
        <div class="row px-0 mx-0">
            <div class="container px-0">
                <ul class="navbar-nav entry-points">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="businessClient-dropdown1"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php echo e(trans('menu.business_clients')); ?>

                        </a>
                        <div class="dropdown-menu" aria-labelledby="businessClient-dropdown1">
                            <a class="dropdown-item" href="/accounts-and-payments"><?php echo e(trans('menu.accounts')); ?></a>
                            <a class="dropdown-item" href="/credit-offers"><?php echo e(trans('menu.credit_line')); ?></a>
                            <a class="dropdown-item"
                               href="/documentary-operations"><?php echo e(trans('menu.documentary_operations')); ?></a>
                            <a class="dropdown-item"
                               href="/term-deposits"><?php echo e(trans('menu.deposits_and_state_bonds')); ?></a>
                            <a class="dropdown-item"
                               href="/services-for-non-resident-legal-entities"><?php echo e(trans('menu.services_for_non_resident_legal_entities')); ?></a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" target="_blank"
                           href="https://www.procreditbank-direct.com/macedonia/mk"><?php echo e(trans('menu.private_clients')); ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- SUB HEADER -->
    <!-- MAIN CONTENT -->
    <div class="container" id="homepage">
        <div class="row px-0 mx-0">
            <div class=" col-md-4  col-lg-3 px-0 order-md-1 order-2">
                <ul class="contact-list">
                    <li><a href="#"><i class="chat-icon"></i>
                            <h1 class="frontpage"><?php echo e(trans('menu.chat_now')); ?></h1></a></li>
                    <li><a href="/contactinfo"><i class="contact-icon"></i>
                            <h1 class="frontpage"><?php echo e(trans('menu.contact_info')); ?></h1></a></li>
                    <li><a href="/contact"><i class="appointment-icon"></i>
                            <h1 class="frontpage"><?php echo e(trans('menu.make_appointment')); ?></h1></a></li>
                </ul>
            </div>
            <div class="col-md-8 col-lg-9 px-0 order-md-2 order-1">
                <!-- SLIDER -->
                <section id="main-slider" class="lazy slider" data-sizes="50vw">
                    <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sliders): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="single-slide">
                            <a href="<?php echo e($sliders->link); ?>"><img class="img-fluid"
                                                                src="/assets/img/slider/<?php echo e($sliders->image); ?>"
                                                                alt="image"></a>
                            <?php if( ! empty($sliders['title'])): ?>
                                <div class="slide-content">
                                    <a href="<?php echo e($sliders->slug); ?>"><h2
                                                class="small-heading"><?php echo e(str_limit(strip_tags($sliders->title),22, $end = '...' )); ?></h2>
                                    </a>
                                    <h2 class="heading"><?php echo e(str_limit(strip_tags($sliders->description),22, $end = '...' )); ?></h2>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </section>
                <!-- SLIDER -->
            </div>
        </div>
    </div>
    <!-- MAIN CONTENT -->
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>