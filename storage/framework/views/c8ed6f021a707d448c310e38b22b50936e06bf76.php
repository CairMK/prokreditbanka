<?php $__env->startSection('content'); ?>
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <p><a class="btn btn-labeled" href="/admin/atm/create"> <i class="btn-label fa fa-plus"></i>Add FAQ </a>
                </p>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>FAQ</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">

                                <tbody>
                                <?php $__currentLoopData = $atms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $atms): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        <td><a href="/admin/atm/<?php echo e($atms->id); ?>/edit"><?php echo e($atms->title); ?></a></td>
                                        <td><?php echo e($atms->address); ?></td>
                                        <td><span>Workflow: <?php echo e($atms->workflow->name); ?></span></td>

                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 right">
                
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>