<!DOCTYPE html>
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<meta charset="utf-8">
<title><?php echo e($settings->title); ?></title>
<meta name="description" content="<?php echo str_limit(strip_tags($settings->description), 150, '...'); ?>">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo e(str_limit(strip_tags($settings->description),170, $end = '...' )); ?>">
    <meta name="author" content="">
    <meta name="keywords"
          content="ПроКредит,мали,средни,претпријатија,банка,кредит,кредити,сметка,сметки,денарска сметка,еко, девизна сметка,закажи средба,контакт инфо, инфо, картички,физички лица, Master,Maestro,Visa,штедење,денарско штедење,девизно штедење,платен промет,гаранции, станбен кредит,готовински кредит, курсна,потрошувачки кредит,кредит,banka,kredit,krediti,smetka,smetki,denarska smetka,devizna smetka, prokredit,karticki,stedenje,denarsko stedenje,devizno stedenje,platen promet,garancii,gotovinski kredit, stamben kredit, appointment,sredba,info, News,Apply Online, Price List, mali, sredni, pretprijatija"/>
    <!-- Bootstrap core CSS -->
    <link href="/assets/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<div class="page-wrapper">

    <?php echo $__env->yieldContent('menu'); ?>

    <?php echo $__env->yieldContent('slider'); ?>

    <div id="page-start"></div>

    <?php echo $__env->yieldContent('content'); ?>

<!-- FOOTER -->
    <div class="row footer-wrapper px-0 mx-0">
        <div class="container">
            <footer>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12 order-lg-1 order-md-2 order-2 news-footer">
                        <div class="news-label">
                            <a href="/news/all"><h5><?php echo e(trans('menu.news')); ?></h5></a>
                        </div>
                        <?php $__currentLoopData = $newshome; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $newshomes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <p>
                                <a href="/news/all"><?php echo str_limit(strip_tags($newshomes->title),55, $end = '...' ); ?></a>
                            </p>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                    <div class="col-lg-3 col-md-6 col-12 order-lg-2 order-md-2 order-2 ">
                        <ul class="list-unstyled p-2">
                            <li class="mb-1  footer-icon-1"><a href="/network"><h4
                                            class="frontpage"><?php echo e(trans('menu.NETWORK')); ?></h4></a>
                            </li>
                            <li class="mb-1 footer-icon-2"><a
                                        href="http://pcb.com.mk/content/pdf/cenovnik-fizicki-lica01.02.pdf.pdf"
                                        target="_blank"><h4
                                            class="frontpage"><?php echo e(trans('menu.price_list_private')); ?></h4>
                                </a>
                            </li>
                            <li class="mb-1" footer-icon-3><a
                                        href="http://pcb.com.mk/content/pdf/cenovnik-pravni-lica.pdf"
                                        target="_blank">
                                    <h4 class="frontpage"><?php echo e(trans('menu.price_list_busines')); ?></h4>
                                </a></li>
                            <li class="mb-1" footer-icon-4><a
                                        href="/calculator1">
                                    <h4 class="frontpage"><?php echo e(trans('menu.calculator_invest')); ?></h4>
                                </a></li>
                            <li class="mb-1" footer-icon-5><a
                                        href="/calculator">
                                    <h4 class="frontpage"><?php echo e(trans('menu.calculator_deposit')); ?></h4>
                                </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12 order-lg-2 order-md-2 order-2 ">
                        <ul class="list-unstyled p-2">
                            <li class="d-flex align-items-center"><i class="swift-icon"></i>
                                <h3 class="frontpage">SWIFT:PRBUMK22XXX</h3></li>
                            <li class="d-flex align-items-center"><i class="envelope-icon"></i>
                                <h3 class="frontpage">info@procreditbank.com.mk</h3>
                            </li>
                            <li class="d-flex align-items-center"><i class="location-icon"></i>
                                <h3 class="frontpage">Manapo br.7 1000 Skopje</h3>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-6 order-lg-4 order-md-4 order-4 ">
                        <ul class="list-unstyled p-2">
                            <!-- Button to Open the Modal -->
                            <li class="mb-1" data-toggle="modal" data-target="#myModal">
                                <?php echo e(trans('menu.exchange_rate')); ?>

                            </li>

                            <!-- The Modal -->
                            <div class="modal" id="myModal">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title"> <?php echo e(trans('menu.exchange_rate')); ?></h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Валута</th>
                                                    <th>Откупен</th>
                                                    <th>Продажен</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>EUR</td>
                                                    <td> <?php $__currentLoopData = $kurs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kurss): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($kurss->euro != NULL): ?>
                                                                <?php echo e($kurss->euro); ?>

                                                            <?php else: ?>
                                                                n/a
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </td>
                                                    <td> <?php $__currentLoopData = $kurs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kurss): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($kurss->euro1 != NULL): ?>
                                                                <?php echo e($kurss->euro1); ?>

                                                            <?php else: ?>
                                                                n/a
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>USD</td>
                                                    <td> <?php $__currentLoopData = $kurs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kurss): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($kurss->usa != NULL): ?>
                                                                <?php echo e($kurss->usa); ?>

                                                            <?php else: ?>
                                                                n/a
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </td>
                                                    <td> <?php $__currentLoopData = $kurs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kurss): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($kurss->usa1 != NULL): ?>
                                                                <?php echo e($kurss->usa1); ?>

                                                            <?php else: ?>
                                                                n/a
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>GBP</td>
                                                    <td> <?php $__currentLoopData = $kurs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kurss): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($kurss->funta != NULL): ?>
                                                                <?php echo e($kurss->funta); ?>

                                                            <?php else: ?>
                                                                n/a
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </td>
                                                    <td> <?php $__currentLoopData = $kurs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kurss): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($kurss->funta1 != NULL): ?>
                                                                <?php echo e($kurss->funta1); ?>

                                                            <?php else: ?>
                                                                n/a
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>CHF</td>
                                                    <td> <?php $__currentLoopData = $kurs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kurss): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($kurss->frank != NULL): ?>
                                                                <?php echo e($kurss->frank); ?>

                                                            <?php else: ?>
                                                                n/a
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </td>
                                                    <td> <?php $__currentLoopData = $kurs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kurss): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($kurss->frank1 != NULL): ?>
                                                                <?php echo e($kurss->frank1); ?>

                                                            <?php else: ?>
                                                                n/a
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <li class="mb-1"><a href="/tenderi"><h5
                                            class="frontpage"><?php echo e(trans('menu.tenders')); ?></h5></a></li>
                            <li class="mb-1"><a href="/prodazba"><h5
                                            class="frontpage"><?php echo e(trans('menu.sales')); ?></h5></a></li>
                            <li class="mb-1"><a href="/terms-and-conditions"><h5
                                            class="frontpage"><?php echo e(trans('menu.terms')); ?></h5></a></li>
                        </ul>
                    </div>
                    <div class="col-lg-12  col-12 order-lg-5 order-md-5 order-5 row mx-0 px-0 border-top pt-2">
                        <div class="col-6">
                            <div class="social-media-footer d-flex align-items-center justify-content-start flex-wrap">
                                <?php if($settings->twitter): ?>
                                    <a target="_blank" href="<?php echo e($settings->twitter); ?>"><i
                                                class="fa fa-twitter"></i></a>
                                <?php endif; ?>
                                <?php if($settings->skype): ?>
                                    <a target="_blank" href="<?php echo e($settings->skype); ?>"><i
                                                class="fa fa-skype"></i></a>
                                <?php endif; ?>
                                <?php if($settings->linkedin): ?>
                                    <a target="_blank" href="<?php echo e($settings->linkedin); ?>"><i
                                                class="fa fa-linkedin"></i></a>
                                <?php endif; ?>
                                <?php if($settings->gplus): ?>
                                    <a target="_blank" href="<?php echo e($settings->gplus); ?>"><i
                                                class="fa fa-google-plus"></i></a>
                                <?php endif; ?>
                                <?php if($settings->youtube): ?>
                                    <a target="_blank" href="<?php echo e($settings->youtube); ?>"><i
                                                class="fa fa-youtube-play"></i></a>
                                <?php endif; ?>
                                <?php if($settings->flickr): ?>
                                    <a target="_blank" href="<?php echo e($settings->flickr); ?>"><i
                                                class="fa fa-flickr"></i></a>
                                <?php endif; ?>
                                <?php if($settings->facebook): ?>
                                    <a target="_blank" href="<?php echo e($settings->facebook); ?>"><i
                                                class="fa fa-facebook"></i></a>
                                <?php endif; ?>
                                <?php if($settings->pinterest): ?>
                                    <a target="_blank" href="<?php echo e($settings->pinterest); ?>"><i
                                                class="fa fa-pinterest"></i></a>
                                <?php endif; ?>

                            </div>
                        </div>
                        <div class="col-6">
                            <h2 class="copyright mt-2 text-right">&copy; All Rights Reserved. Pro Credit Bank 2018
                            </h2>
                        </div>
                    </div>

                </div>

            </footer>
        </div>
    </div>
    <!-- FOOTER -->
    <!-- Bootstrap core JavaScript -->
    <script src="/assets/vendor/jquery/jquery.min.js"></script>
    <script defer="defer" src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <?php echo $__env->yieldContent('scripts'); ?>
    <script defer="defer" type="text/javascript" src="/assets/vendor/scrolloverflow.js"></script>
    <script defer="defer" type="text/javascript" src="/assets/js/jquery.fullPage.js"></script>
    <script defer="defer" src="/assets/js/main.js"></script>


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

    <script defer="defer" src="/assets/slick/slick.min.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">
        $(window).load(function () {
            $(".lazy").slick({

                infinite: true,
                dots: true,
                prevArrow: false,
                nextArrow: false,
                autoplay: true,
                autoplaySpeed: 2000

            });
        });
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-76143150-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-76143150-2');
    </script>


    </body>
</html>
