<?php $__env->startSection('content'); ?>
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-12">
                <p><a class="btn btn-labeled w" href="/admin/staticpage/create"> <i
                                class="btn-label fa fa-plus"></i>Add Static Page </a></p>
                <div class="card">
                    <div class="card-title">
                        <h4>Статични страни</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <?php $__currentLoopData = $staticpages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $staticpage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td>
                                            <div class="round-img">
                                                <a href="/admin/staticpage/<?php echo e($staticpage->id); ?>/edit"> <?php if($staticpage->image): ?>
                                                        <img src="/assets/img/staticpage/<?php echo e($staticpage->imagethumb); ?>"
                                                             alt="">
                                                    <?php endif; ?></a>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="/admin/staticpage/<?php echo e($staticpage->id); ?>/edit"><?php echo e($staticpage->title); ?></a>
                                        </td>
                                        <td><span>Original time: <?php echo e($staticpage->created_at); ?></span></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <?php echo $staticpages->links(); ?>

        </div>
    </div>



<?php $__env->stopSection(); ?>





<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>